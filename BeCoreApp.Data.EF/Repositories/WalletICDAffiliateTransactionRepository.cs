﻿using BeCoreApp.Data.Entities;
using BeCoreApp.Data.IRepositories;
using System;
using System.Collections.Generic;
using System.Text;

namespace BeCoreApp.Data.EF.Repositories
{
    public class WalletICDAffiliateTransactionRepository : EFRepository<WalletICDAffiliateTransaction, int>, IWalletICDAffiliateTransactionRepository
    {
        public WalletICDAffiliateTransactionRepository(AppDbContext context) : base(context)
        {
        }
    }
}
