﻿using BeCoreApp.Data.GameCrashEntities;
using BeCoreApp.Data.IRepositories.CrashGame;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BeCoreApp.Data.EF.Repositories.CrashGame
{
   
    public class GameHashRepository : GameRepository<GameHash, long>, IGameHashRepository
    {
        public GameHashRepository(CrashGameDbContext context) : base(context)
        {
        }


    }
}
