﻿using BeCoreApp.Data.GameCrashEntities;
using BeCoreApp.Data.IRepositories.CrashGame;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BeCoreApp.Data.EF.Repositories.CrashGame
{
   
    public class PlayRepository : GameRepository<Play, long>, IPlayRepository
    {
        CrashGameDbContext _dbContext;
        public PlayRepository(CrashGameDbContext context) : base(context)
        {
            _dbContext = context;
        }

        public int UpdatePlayerBonus(Dictionary<long,double> bonuses)
        {
            try
            {
                var sql = string.Empty;

                foreach (var item in bonuses)
                {
                    sql += $"update play set bonus = {item.Value} where id={item.Key} go ";

                }

                if (!string.IsNullOrEmpty(sql))
                {
                    return _dbContext.Database.ExecuteSqlRaw(sql);
                }

                
            }
            catch (Exception e)
            {

            }
            
            return 0;
        }

    }
}
