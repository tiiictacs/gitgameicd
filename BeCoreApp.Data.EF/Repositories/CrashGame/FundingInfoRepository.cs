﻿using BeCoreApp.Data.GameCrashEntities;
using BeCoreApp.Data.IRepositories.CrashGame;
using Microsoft.EntityFrameworkCore;
using System.Linq;

namespace BeCoreApp.Data.EF.Repositories.CrashGame
{
    public class FundingInfoRepository : GameRepository<FundingInfo, long>, IFundingInfoRepository
    {

        protected readonly CrashGameDbContext DbContext;
        public FundingInfoRepository(CrashGameDbContext context) : base(context)
        {
            DbContext = context;
        }


        public double GetBankRoll()
        {
            try
            {
                string sql = "exec spGetBankRoll";


                var scalarValue = DbContext.ScalarDoubleValue.FromSqlRaw(sql).AsEnumerable().FirstOrDefault();

                return scalarValue.Value;
            }
            catch (System.Exception)
            {

            }
            return 0;


        }

    }
}
