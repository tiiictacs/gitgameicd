﻿using BeCoreApp.Data.GameCrashEntities;
using BeCoreApp.Data.IRepositories.CrashGame;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BeCoreApp.Data.EF.Repositories.CrashGame
{
    public class UserInfoRepository : GameRepository<UserInfo, Guid>, IUserInfoRepository
    {
        CrashGameDbContext _dbContext;
        public UserInfoRepository(CrashGameDbContext context) : base(context)
        {
            _dbContext = context;
        }

        public int SetEndGameBonus(Dictionary<Guid, double> bonuses)
        {
            if (bonuses.Count == 0)
                return 0;

            var sql = string.Empty;



            foreach (var item in bonuses)
            {
                sql += $"update play set Balance += {item.Value} where id='{item.Key}' go ";

            }

            return _dbContext.Database.ExecuteSqlRaw(sql);

            
        }


    }
}
