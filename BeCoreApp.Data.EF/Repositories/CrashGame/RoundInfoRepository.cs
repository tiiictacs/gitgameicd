﻿using BeCoreApp.Data.GameCrashEntities;
using BeCoreApp.Data.IRepositories.CrashGame;
using System;
using System.Collections.Generic;
using System.Text;

namespace BeCoreApp.Data.EF.Repositories.CrashGame
{
    public class RoundInfoRepository : EFRepository<RoundInfo, int>, IRoundInfoRepository
    {
        public RoundInfoRepository(AppDbContext context) : base(context)
        {
        }


    }
}
