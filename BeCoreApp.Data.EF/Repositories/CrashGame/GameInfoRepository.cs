﻿using BeCoreApp.Data.GameCrashEntities;
using BeCoreApp.Data.IRepositories.CrashGame;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BeCoreApp.Data.EF.Repositories.CrashGame
{
   

    public class GameInfoRepository : GameRepository<GameInfo, long>, IGameInfoRepository
    {
        CrashGameDbContext _dbContext;
        public GameInfoRepository(CrashGameDbContext context) : base(context)
        {
            _dbContext = context;
        }

        public List<GameInfoHistory> GetGameInfoHistories()
        {
            var sql = "exec spGetGameInfoHistories";

            var data = _dbContext.GameInfoHistories.FromSqlRaw(sql).ToList();

            return data;
        }

        public int SetEndGame(long gameId)
        {
            var sql = $"exec spEndGame @gameId={gameId}";

            int rows = _dbContext.Database.ExecuteSqlRaw(sql);

            return rows;

            
            
        }

    }
}
