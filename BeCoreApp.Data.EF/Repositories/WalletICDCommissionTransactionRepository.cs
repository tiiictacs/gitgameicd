﻿using BeCoreApp.Data.Entities;
using BeCoreApp.Data.IRepositories;
using System;
using System.Collections.Generic;
using System.Text;

namespace BeCoreApp.Data.EF.Repositories
{
    

    public class WalletICDCommissionTransactionRepository : EFRepository<WalletICDCommissionTransaction, int>, IWalletICDCommissionTransactionRepository
    {
        public WalletICDCommissionTransactionRepository(AppDbContext context) : base(context)
        {

        }
    }
}
