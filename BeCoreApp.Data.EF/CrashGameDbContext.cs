﻿using BeCoreApp.Data.GameCrashEntities;
using BeCoreApp.Data.Interfaces;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;
using System;
using System.IO;
using System.Linq;

namespace BeCoreApp.Data.EF
{
    public class CrashGameDbContext : DbContext
    {
 
        public CrashGameDbContext(DbContextOptions<CrashGameDbContext> options) : base(options) { }

        public DbSet<RoundInfo> RoundInfos { set; get; }

        public DbSet<GameInfo> GameInfos { set; get; }

        public DbSet<GameHash> GameHashs { set; get; }

        public DbSet<UserInfo> UserInfos { set; get; }

        public DbSet<Play> Plays { set; get; }

        public DbSet<ScalarDecimalValue> ScalarDecimalValue { set; get; }

        public DbSet<ScalarDoubleValue> ScalarDoubleValue { set; get; }

        public DbSet<GameInfoHistory> GameInfoHistories { set; get; }

        public override int SaveChanges()
        {
            var modified = ChangeTracker.Entries()
                .Where(e => e.State == EntityState.Modified || e.State == EntityState.Added);

            foreach (EntityEntry item in modified)
            {
                var changedOrAddedItem = item.Entity as IDateTracking;
                if (changedOrAddedItem != null)
                {
                    if (item.State == EntityState.Added)
                        changedOrAddedItem.DateCreated = DateTime.Now;

                    changedOrAddedItem.DateModified = DateTime.Now;
                }
            }

            return base.SaveChanges();
        }


    }
}
