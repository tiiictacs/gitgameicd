﻿using BeCoreApp.Infrastructure.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BeCoreApp.Data.EF
{
    public class GameUnitOfWork : IUnitOfWork
    {
        private readonly CrashGameDbContext _context;
        public GameUnitOfWork(CrashGameDbContext context)
        {
            _context = context;
        }
        public void Commit()
        {
            _context.SaveChanges();
        }

        public void Dispose()
        {
            _context.Dispose();
        }
    }
}
