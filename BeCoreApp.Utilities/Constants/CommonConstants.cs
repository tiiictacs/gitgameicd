﻿using System;
using System.Collections.Generic;
using System.Text;
using Telegram.Bot.Types;

namespace BeCoreApp.Utilities.Constants
{
    public class CommonConstants
    {
        public class MemberSystem
        {
            public string Email { get; set; }
            public decimal Amount { get; set; }
        }

        public static List<MemberSystem> MemberAccessDenied = new List<MemberSystem>
        {
            new MemberSystem { Email = "Invest6868vn@gmail.com", Amount = 70 },
        };

        //Bot Telegram
        public const string BotToken1 = "1953768449:AAEYkd6ZrQPG1Wv-4jt-wpS1URLdVxt8wuU";
        public const string BotToken2 = "1989172401:AAFuWnReLMXUDOjfTYX-dIUrNcxyHUs7IOA";
        public const string BotToken3 = "1961607617:AAGdPP4tYxd5VokvY-Ch2lC3ybY7EO8YfHQ";
        public const string BotToken4 = "1970681615:AAEaDNWxIicqiIH5FezPhtMzBhJxh7ytoT8";
        public const string BotToken5 = "1995360693:AAG_wKpmWiN8pXX9Z6NfkwgljjKliZegG3w";

        public const string BotToken6 = "1984098820:AAEDWW_rkFIDZ-lp8ysNgo5CTpYrrCbma-Q";
        public const string BotToken7 = "1926615802:AAGS-Z7cLgYo_oSx38k0vfM2rw-PB9KULg8";
        public const string BotToken8 = "1978865032:AAGhbrq-x98SFXR8eREkoKe2hKfK9tKFjP4";
        //public const string BotToken9 = "1989172401:AAFuWnReLMXUDOjfTYX-dIUrNcxyHUs7IOA";
        //public const string BotToken10 = "1989172401:AAFuWnReLMXUDOjfTYX-dIUrNcxyHUs7IOA";
        //Telegram icd reporter group 
        public static ChatId WithdrawGroup => new ChatId(-1001537248847);
        public static ChatId DepositGroup => new ChatId(-1001372634263);
        public static ChatId TestGroup => new ChatId(-573514189);

        public const string DefaultFooterId = "DefaultFooterId";
        public const string DefaultContactId = "default";
        public const string CartSession = "CartSession";
        public const string ProductTag = "Product";
        public const string BlogTag = "Blog";

        public class AppRole
        {
            public const string AdminRole = "Admin";
        }

        public class UserClaims
        {
            public const string Roles = "Roles";
        }


        #region BEP20 TestNet
        public const string BEP20Url = "https://data-seed-prebsc-1-s3.binance.org:8545/";

        public const string BEP20ICDContract = "0x217bd5d5cebb548e32ed3abe7a5b9efaa107c876";
        public const int BEP20ICDDP = 18;

        public const string BEP20SYSTEMPuKey = "0xdf17d243ee8a414a398225c3f669642d94b9cc39";

        public const string BEP20PAYAFFILIATEPuKey = "0xdf17d243ee8a414a398225c3f669642d94b9cc39";
        public const string BEP20PAYAFFILIATEPrKey = "e4408808448fe482b64e8988186a6370f5cd374204ee2ee064f9bb91f24d3deb";

        public const string BEP20AIRDROPPuKey = "0xdf17d243ee8a414a398225c3f669642d94b9cc39";
        public const string BEP20AIRDROPPrKey = "e4408808448fe482b64e8988186a6370f5cd374204ee2ee064f9bb91f24d3deb";

        public const string BEP20EXCHANGEPuKey = "0xdf17d243ee8a414a398225c3f669642d94b9cc39";
        public const string BEP20EXCHANGEPrKey = "e4408808448fe482b64e8988186a6370f5cd374204ee2ee064f9bb91f24d3deb";
        #endregion

        #region ERC20
        public const string ERC20Url = "*****";
        public const string ERC20VITONGPrivateKey = "*****";
        public const string ERC20VITONGPublishKey = "*****";
        public const string ERC20TGContract = "*****";
        public const int ERC20TGDP = 18;
        public const string ERC20USDTContract = "*****";
        public const int ERC20USDTDP = 6;
        #endregion

        #region TRC20
        public const string TRC20Url = "*****";
        public const string TRC20VITONGPrivateKey = "*****";
        public const string TRC20VITONGPublishKey = "*****";
        public const string TRC20USDTContract = "*****";
        public const string TRC20TGContract = "*****";
        public const string TRONApiKey = "*****";
        #endregion

        public static string[] WhiteListMembers = new string[]
        {
            "0x28c2d6fb566Fb63C957F8714De5Db1De857353eB",
            "0x0fa5bf51F2db49ea7d9442463D5718C8542c345a"
        };
    }
}
