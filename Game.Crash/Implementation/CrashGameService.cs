﻿using BeCoreApp.Infrastructure.Interfaces;
using Game.Crash.Interfaces;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;

namespace Game.Crash.Implementation
{
    public class CrashGameService : ICrashGameService
    {
        private readonly IUnitOfWork _unitOfWork;

        public CrashGameService(
            IUnitOfWork unitOfWork
            )
        {
            _unitOfWork = unitOfWork;
        }
    }
}