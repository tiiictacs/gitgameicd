﻿using Microsoft.AspNetCore.SignalR.Client;
using System;
using System.Threading.Tasks;

namespace ClientSocket
{
    class Program
    {
        static async Task Main(string[] args)
        {
            try
            {
                var connection = new HubConnectionBuilder()
                .WithUrl("https://localhost:44396/hubs/game").Build();

                //Console.WriteLine("Hello World!");

                //connection.On<object>("game_starting", (msg) => OnGame_Starting(msg));

                //connection.On<string>("pong", (msg) => OnPong(msg));

                await connection.StartAsync();

                await connection.InvokeAsync("OnPlaceBetAsync", new{ betAmount=200, AutoCashOut=20 });
            }
            catch (Exception e)
            {

            }
            
        }

        public static void OnGame_Starting(object msg)
        {
            Console.WriteLine($"{msg}");
        }

        public static void OnPong(string msg)
        {
            Console.WriteLine($"{msg}");
        }
    }
}
