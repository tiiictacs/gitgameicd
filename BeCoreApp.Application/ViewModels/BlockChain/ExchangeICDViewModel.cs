﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BeCoreApp.Application.ViewModels.BlockChain
{
    public class ExchangeICDViewModel
    {
        public decimal OrderBNB { get; set; }

        public decimal AmountICD { get; set; }
    }
}
