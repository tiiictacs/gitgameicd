﻿using BeCoreApp.Data.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace BeCoreApp.Application.ViewModels.Staking
{
    public class BuyStakingViewModel
    {
        public int Type { get; set; }
        public StakingTimeLine TimeLine { get; set; }
        public StakingPackage Package { get; set; }
        public decimal AmountPayment { get; set; }
    }
}
