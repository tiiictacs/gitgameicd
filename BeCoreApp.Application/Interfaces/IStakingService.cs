﻿using BeCoreApp.Application.ViewModels.System;
using BeCoreApp.Data.Entities;
using BeCoreApp.Data.Enums;
using BeCoreApp.Utilities.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;

namespace BeCoreApp.Application.Interfaces
{
    public interface IStakingService
    {
        PagedResult<StakingViewModel> GetAllPaging(string keyword, string appUserId, int pageIndex, int pageSize);

        decimal GetTotalPackage(Guid userId, StakingType? type);

        decimal GetMaxPackage(Guid userId);

        StakingViewModel GetById(int id);
        void Update(StakingViewModel model);
        void Add(StakingViewModel Model);

        void Save();
    }
}
