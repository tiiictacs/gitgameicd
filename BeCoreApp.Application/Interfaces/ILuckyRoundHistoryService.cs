﻿using BeCoreApp.Application.ViewModels.Game;
using BeCoreApp.Application.ViewModels.System;
using BeCoreApp.Data.Enums;
using BeCoreApp.Utilities.Dtos;
using System.Threading.Tasks;

namespace BeCoreApp.Application.Interfaces
{
    public interface ILuckyRoundHistoryService
    {
        PagedResult<LuckyRoundHistoryViewModel> GetAllPaging(string keyword, string appUserId, int pageIndex, int pageSize);
        
        int CountByType(LuckyRoundHistoryType type, int luckyRoundId);

        void Add(LuckyRoundHistoryViewModel model);

        void Save();
    }
}
