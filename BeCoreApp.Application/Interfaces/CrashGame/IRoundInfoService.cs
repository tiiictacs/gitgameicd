﻿using BeCoreApp.Application.GameViewModels.CrashGame;
using BeCoreApp.Utilities.Dtos;
using System;
using System.Collections.Generic;
using System.Text;

namespace BeCoreApp.Application.Interfaces.CrashGame
{
    public interface IRoundInfoService
    {
        PagedResult<RoundInfoViewModel> GetAllPaging(string startDate, string endDate, string keyword, int blogCategoryId, int pageIndex, int pageSize);

        RoundInfoViewModel Add(RoundInfoViewModel blog);

        void Update(RoundInfoViewModel blog);

        void Delete(int id);
    }
}
