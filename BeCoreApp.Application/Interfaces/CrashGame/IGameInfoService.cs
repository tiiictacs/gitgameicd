﻿using BeCoreApp.Application.GameViewModels.GameInfo;
using BeCoreApp.Application.GameViewModels.UserInfo;
using BeCoreApp.Data.GameCrashEntities;
using System;
using System.Collections.Generic;

namespace BeCoreApp.Application.Interfaces.CrashGame
{
    public interface IGameInfoService
    {
        public void SeedGameHash(int gameCount);

        public GameInfoViewModel CreateGame(long hashId);

        public GameInfoViewModel GetLastGameInfo();

        public long PlaceBet(Guid userGuid, double amount,int AutoCashOut,long gameId);

        public List<GameHistoryInfo> GetGameInfoHistories();

        void CashOut(Guid userId, long playId, double amount);

        double GetBankRoll();

        void EndGame(long gameId, List<PlayerBonusCalcModel> bonuses);

        UserInfo GetUserInfo(Guid userId);


    }
}
