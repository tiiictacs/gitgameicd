﻿using BeCoreApp.Application.ViewModels.System;
using BeCoreApp.Utilities.Dtos;
using System;
using System.Collections.Generic;
using System.Text;

namespace BeCoreApp.Application.Interfaces
{
    public interface IWalletICDCommissionTransactionService
    {
        PagedResult<WalletICDCommissionTransactionViewModel> GetAllPaging(string keyword, string appUserId, int pageIndex, int pageSize);

        void Add(WalletICDCommissionTransactionViewModel Model);

        void Save();
    }
}
