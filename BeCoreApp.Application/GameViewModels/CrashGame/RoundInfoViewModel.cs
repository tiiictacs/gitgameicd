﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BeCoreApp.Application.GameViewModels.CrashGame
{
    public class RoundInfoViewModel
    {
        
        public string RoundCode { get; set; }

        public DateTime CreatedOn { get; set; }

        public decimal TotalBet { get; set; }

        public decimal TotalWin { get; set; }

        public string ResultValue { get; set; }

        public int ResultFrom { get; set; }


        public int ActionLog { get; set; }
    }
}
