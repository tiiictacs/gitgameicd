﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BeCoreApp.Application.GameViewModels.CrashGame
{
    public class EndGameInfoResult
    {
        public bool Forced { get;set;}

        public long Elapsed { get;set;}

        [JsonProperty("game_crash")]
        public long GameCrash { get;set;}
        [JsonProperty("hash")]
        public string Hash { get;set;}
    }
}
