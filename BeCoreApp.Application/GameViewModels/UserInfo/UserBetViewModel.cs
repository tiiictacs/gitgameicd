﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BeCoreApp.Application.GameViewModels.UserInfo
{
    public class UserBetViewModel
    {
        public string UserName { get;set;}

        public Guid UserId { get;set;}

        public double Amount { get;set;}
    }
}
