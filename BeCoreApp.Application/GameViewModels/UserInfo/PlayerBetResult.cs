﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BeCoreApp.Application.GameViewModels.UserInfo
{
    public class PlayerBetResult
    {
        [JsonProperty("userName")]
        public string UserName { get;set;}

        [JsonProperty("index")]
        public  int Index { get;set;}
    }
}
