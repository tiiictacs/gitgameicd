﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BeCoreApp.Application.GameViewModels.UserInfo
{
    public class UserInfoViewModel
    {
        public Guid UserId { get;set;}

        public double Balance { get;set;}

        public double Bet { get;set;}

        public string UserName { get;set;}

        public int AutoCashOut { get;set;}

        public long PlayId { get;set;}

        public string Status { get;set;}

        public double StoppedAt { get;set;}
    }
}
