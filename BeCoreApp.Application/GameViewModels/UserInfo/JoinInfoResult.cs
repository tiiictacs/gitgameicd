﻿using BeCoreApp.Application.GameViewModels.GameInfo;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BeCoreApp.Application.GameViewModels.UserInfo
{
    public class JoinInfoResult
    {
        [JsonProperty("username")]
        public string UserName { get;set;}

        [JsonProperty("isConnected ")]
        public bool IsConnected { get; set; }

        [JsonProperty("gameState")]
        public string GameState { get; set; }

        [JsonProperty("game_id")]
        public long GameId { get; set; }

        [JsonProperty("max_win")]
        public double MaxWin { get; set; }

        [JsonProperty("last_hash")]
        public string LastHash { get; set; }

        [JsonProperty("startTime")]
        public DateTime StartTime { get; set; }

        [JsonProperty("player_info")]
        public List<UserInfoViewModel> PlayerInfo { get;set;}

        [JsonProperty("table_history")]
        public List<GameHistoryInfo> TableHistory { get;set;}

        [JsonProperty("elapsed")]
        public double Elapsed { get;set;}




    }
}
