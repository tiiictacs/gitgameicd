﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BeCoreApp.Application.GameViewModels.UserInfo
{
    public class PlayerBonusCalcModel
    {
        public UserInfoViewModel User { get;set;}

        public long PlayId { get;set;}

        public double Amount { get;set;}
    }
}
