﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BeCoreApp.Application.GameViewModels.UserInfo
{
    public class CashedOutResultViewModel
    {
        public string UserName { get;set;}

        public double StoppedAt { get;set;}
    }
}
