﻿using BeCoreApp.Application.GameViewModels.UserInfo;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BeCoreApp.Application.GameViewModels.GameInfo
{
    public class GameInfoSummary
    {
        public string State { get;set;}

        public List<UserInfoViewModel> PlayerInfo { get; set; } = new List<UserInfoViewModel>();

        [JsonProperty("game_id")]
        public long GameId { get;set;}
        [JsonProperty("last_hash")]
        public string LastHash { get;set;}
        [JsonProperty("max_win")]
        public double MaxWin { get;set;}

        public double Elapsed { get;set; }

        public DateTime CreatedOn { get;set;}

        public List<string> Joined { get;set;}
        [JsonProperty("crash_at")]
        public long CrashAt { get;set;}
    }
}
