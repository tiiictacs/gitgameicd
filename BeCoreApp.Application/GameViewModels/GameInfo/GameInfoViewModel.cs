﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BeCoreApp.Application.GameViewModels.GameInfo
{
    public class GameInfoViewModel: BaseViewModel
    {
        public long GameCrash { get; set; }

        public DateTime CreatedOn { get; set; }

        public bool IsEnded { get; set; }

        public string Hash { get;set;}

        public long HashId { get;set;}

    }
}
