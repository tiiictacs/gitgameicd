﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BeCoreApp.Application.GameViewModels.GameInfo
{
    public class GameProcessResult
    {
        [JsonProperty("game_id")]
        public long GameId { get;set;}
        [JsonProperty("max_win")]
        public double MaxWin { get;set;}

        public int TimeTillStart { get;set;}
    }
}
