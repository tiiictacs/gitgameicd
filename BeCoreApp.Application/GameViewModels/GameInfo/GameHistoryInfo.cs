﻿using BeCoreApp.Application.GameViewModels.UserInfo;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BeCoreApp.Application.GameViewModels.GameInfo
{
    public class GameHistoryInfo
    {
        [JsonProperty("game_id")]
        public long GameId { get;set;}

        [JsonProperty("game_crash")]
        public long GameCrash { get; set; }

        [JsonProperty("created")]
        public DateTime Created { get; set; }
        [JsonProperty("UserInfoViewModel")]
        public List<UserInfoViewModel> PlayerInfo { get; set; } = new List<UserInfoViewModel>();
        [JsonProperty("hash")]
        public string Hash { get;set;}
    }
}
