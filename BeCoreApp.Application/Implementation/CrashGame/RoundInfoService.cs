﻿using BeCoreApp.Application.GameViewModels.CrashGame;
using BeCoreApp.Application.Interfaces.CrashGame;
using BeCoreApp.Data.GameCrashEntities;
using BeCoreApp.Data.IRepositories.CrashGame;
using BeCoreApp.Infrastructure.Interfaces;
using BeCoreApp.Utilities.Dtos;
using System;
using System.Collections.Generic;
using System.Text;

namespace BeCoreApp.Application.Implementation.CrashGame
{
    public class RoundInfoService : IRoundInfoService
    {
        private readonly IRoundInfoRepository _roundInfoRepository;
        private readonly IUnitOfWork _unitOfWork;

        public RoundInfoService(IRoundInfoRepository roundInfoRepository,
            IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
            _roundInfoRepository = roundInfoRepository;
        }

        public RoundInfoViewModel Add(RoundInfoViewModel roundInfoModel)
        {
            RoundInfo roundInfo = new RoundInfo
            {
                ActionLog = roundInfoModel.ActionLog,
                CreatedOn =DateTime.Now,
                ResultFrom = roundInfoModel.ResultFrom,
                ResultValue = roundInfoModel.ResultValue,
                RoundCode = roundInfoModel.RoundCode,
                TotalBet = roundInfoModel.TotalBet,
                TotalWin = roundInfoModel.TotalWin

            };

            _roundInfoRepository.Add(roundInfo);
            Save();
            return roundInfoModel;
        }

        public void Delete(int id) => _roundInfoRepository.Remove(id);

        public void Save() => _unitOfWork.Commit();

        public PagedResult<RoundInfoViewModel> GetAllPaging(string startDate, string endDate, string keyword, int blogCategoryId, int pageIndex, int pageSize)
        {
            throw new NotImplementedException();
        }

        public void Update(RoundInfoViewModel blog)
        {
            throw new NotImplementedException();
        }
    }
}
