﻿using BeCoreApp.Application.GameViewModels.GameInfo;
using BeCoreApp.Application.GameViewModels.UserInfo;
using BeCoreApp.Application.Interfaces.CrashGame;
using BeCoreApp.Data.GameCrashEntities;
using BeCoreApp.Data.IRepositories.CrashGame;
using BeCoreApp.Infrastructure.Interfaces;
using BeCoreApp.Utilities.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;

namespace BeCoreApp.Application.Implementation.CrashGame
{
    public class GameInfoService : IGameInfoService
    {

        private string ServerSeed = "1qa2ws3ed4rf";

        private const string GameHashSeed = "6aff6e963620bf58d2fa7143265245b5ffe606ed3972b182825a71e50e5b2067";

        private readonly IFundingInfoRepository _fundingInfoRepository;
        private readonly IGameHashRepository _gameHashRepository;
        private readonly IGameInfoRepository _gameInfoRepository;
        private readonly IUserInfoRepository _userInfoRepository;
        private readonly IPlayRepository _playRepository;
        private readonly IUnitOfWork _unitOfWork;

        public GameInfoService(IGameHashRepository gameHashRepository,
            IUnitOfWork unitOfWork,
            IGameInfoRepository gameInfoRepository,
            IUserInfoRepository userInfoRepository,
            IPlayRepository playRepository,
            IFundingInfoRepository fundingInfoRepository)
        {
            _fundingInfoRepository = fundingInfoRepository;
            _playRepository = playRepository;
            _unitOfWork = unitOfWork;
            _gameHashRepository = gameHashRepository;
            _gameInfoRepository = gameInfoRepository;
            _userInfoRepository = userInfoRepository;
        }

        public void SeedGameHash(int gameCount = 1000)
        {
            int count = 0;
            for (int i = 0; i < gameCount; i++)
            {
                ServerSeed = GameLib.GenerateHash(ServerSeed);

                _gameHashRepository.Add(new GameHash
                {
                    Hash = ServerSeed
                });
                count++;
                
            }
            if (count%50==0)
            {
                _unitOfWork.Commit();
            }
        }

        public GameInfoViewModel CreateGame(long hashId)
        {
            var gameHash = _gameHashRepository.FindById(hashId);

            if (gameHash==null)
                return null;

            GameInfoViewModel model = new();

            model.Hash = gameHash.Hash;
            model.HashId = hashId;

            model.GameCrash = GameLib.GetCrashPointFromHash(gameHash.Hash);

            var game = new GameInfo
            {
                CreatedOn = DateTime.UtcNow,
                GameCrash = model.GameCrash,
                HashId = hashId
            };

            _gameInfoRepository.Add(game);

            _unitOfWork.Commit();

            model.Id = game.Id;

            return model;
        }

        public GameInfoViewModel GetLastGameInfo()
        {
            var query = _gameInfoRepository.FindAll();

            query = query.OrderByDescending(x=>x.Id);

            var latest = query.FirstOrDefault();

            var model = new GameInfoViewModel
            {
                Hash = GameHashSeed,
                HashId = 2,
                Id = 1
            };

            if (latest==null)
            {
                return model;
            }


            model.Id = latest.Id;
            model.CreatedOn = latest.CreatedOn;
            model.IsEnded = latest.IsEnded;

            var gameHash = _gameHashRepository.FindById(latest.HashId);

            if (gameHash != null)
            {
                model.Hash = gameHash.Hash;
                model.HashId = latest.HashId;
            }
            
                

            return model;
        }

        public long PlaceBet(Guid userGuid , double amount, int AutoCashOut, long gameId)
        {
            var userInfo = _userInfoRepository.FindById(userGuid);
            if (userInfo==null)
                return -1;

            userInfo.Balance -= amount;
            _userInfoRepository.Update(userInfo);
            _unitOfWork.Commit();

            var play = new Play
            {
                AutoCashOut=AutoCashOut,
                Bet=amount,
                CashOut=0,
                CreatedOn=DateTime.UtcNow,
                GameId=gameId,
                UserId= userGuid
            };

            _playRepository.Add(play);
            _unitOfWork.Commit();
            return play.Id;
        }

        public double GetBankRoll()
        {
            return _fundingInfoRepository.GetBankRoll();
        }

        public void CashOut(Guid userId, long playId, double amount)
        {
            AddBalance(userId,amount);
            var playInfo = _playRepository.FindById(playId);
            if (playInfo!=null)
            {
                if (playInfo.CashOut==null)
                {
                    playInfo.CashOut = amount;
                    _playRepository.Update(playInfo);
                    _unitOfWork.Commit();
                }
                
            }
        }

        public void AddBalance(Guid userGuid, double amount)
        {
            var user = _userInfoRepository.FindById(userGuid);
            if (user!=null)
            {
                user.Balance += amount;
                _userInfoRepository.Update(user);
                _unitOfWork.Commit();
            }
        }


        public void EndGame(long gameId,List<PlayerBonusCalcModel> bonuses)
        {
            

            var count = _gameInfoRepository.SetEndGame(gameId);
            if (count==0)
            {
                return;
            }

            Dictionary<long,double> playInfoDict = new();
            Dictionary<Guid, double> userInfoDict = new();
            foreach (var player in bonuses)
            {
                playInfoDict.Add(player.PlayId,player.Amount);
                userInfoDict.Add(player.User.UserId,player.Amount);
            }

            _playRepository.UpdatePlayerBonus(playInfoDict);

            _userInfoRepository.SetEndGameBonus(userInfoDict);

        }

        public List<GameHistoryInfo> GetGameInfoHistories()
        {
            var raw = _gameInfoRepository.GetGameInfoHistories();

            List<GameHistoryInfo> resp = new();

            foreach (var item in raw)
            {
                resp.Add(new GameHistoryInfo
                {
                    Created = item.CreatedOn,
                    GameCrash = item.GameCrash,
                    PlayerInfo = new List<UserInfoViewModel>(),
                    GameId = item.Id
                });
            }

            return resp;
        }

        public UserInfo GetUserInfo(Guid userId)
        {
            try
            {
                var user = _userInfoRepository.FindById(userId);

                return user;
            }
            catch (Exception e)
            {

                
            }
            return null;
        }


        
    }
}
