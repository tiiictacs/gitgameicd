﻿using BeCoreApp.Application.Interfaces;
using BeCoreApp.Application.ViewModels.System;
using BeCoreApp.Data.Entities;
using BeCoreApp.Data.Enums;
using BeCoreApp.Data.IRepositories;
using BeCoreApp.Infrastructure.Interfaces;
using BeCoreApp.Utilities.Dtos;
using BeCoreApp.Utilities.Extensions;
using System;
using System.Linq;

namespace BeCoreApp.Application.Implementation
{
    public class StakingRewardService : IStakingRewardService
    {
        private readonly IStakingRewardRepository _stakingRewardRepository;
        private readonly IUnitOfWork _unitOfWork;

        public StakingRewardService(
            IStakingRewardRepository stakingRewardRepository,
            IUnitOfWork unitOfWork)
        {
            _stakingRewardRepository = stakingRewardRepository;
            _unitOfWork = unitOfWork;
        }

        public PagedResult<StakingRewardViewModel> GetAllPaging(string keyword, string appUserId, int pageIndex, int pageSize)
        {
            var query = _stakingRewardRepository.FindAll(x => x.AppUser);

            if (!string.IsNullOrEmpty(keyword))
                query = query.Where(x => x.AppUser.Email.Contains(keyword)
                || x.AppUser.Sponsor.Value.ToString().Contains(keyword));

            if (!string.IsNullOrWhiteSpace(appUserId))
                query = query.Where(x => x.AppUserId.ToString() == appUserId);

            var totalRow = query.Count();
            var data = query.OrderByDescending(x => x.Id)
                .Skip((pageIndex - 1) * pageSize).Take(pageSize)
                .Select(x => new StakingRewardViewModel()
                {
                    Id = x.Id,
                    Amount = x.Amount,
                    PackageInterestRate = x.PackageInterestRate,
                    RealInterestRate = x.RealInterestRate,
                    SuddenInterestRate = x.SuddenInterestRate,
                    StakingId = x.StakingId,
                    AppUserId = x.AppUserId,
                    AppUserName = x.AppUser.UserName,
                    Sponsor = $"ICD{ x.AppUser.Sponsor}",
                    DateCreated = x.DateCreated
                }).ToList();

            return new PagedResult<StakingRewardViewModel>()
            {
                CurrentPage = pageIndex,
                PageSize = pageSize,
                Results = data,
                RowCount = totalRow
            };
        }

        public StakingReward Add(StakingRewardViewModel model)
        {
            var transaction = new StakingReward()
            {
                Amount = model.Amount,
                SuddenInterestRate = model.SuddenInterestRate,
                PackageInterestRate = model.PackageInterestRate,
                RealInterestRate = model.RealInterestRate,
                StakingId = model.StakingId,
                AppUserId = model.AppUserId,
                DateCreated = model.DateCreated,
            };

            _stakingRewardRepository.Add(transaction);

            return transaction;
        }

        public void Save()
        {
            _unitOfWork.Commit();
        }
    }
}
