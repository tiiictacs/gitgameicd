﻿using BeCoreApp.Application.Interfaces;
using BeCoreApp.Application.ViewModels.System;
using BeCoreApp.Data.Entities;
using BeCoreApp.Data.Enums;
using BeCoreApp.Data.IRepositories;
using BeCoreApp.Infrastructure.Interfaces;
using BeCoreApp.Utilities.Dtos;
using BeCoreApp.Utilities.Extensions;
using System;
using System.Linq;

namespace BeCoreApp.Application.Implementation
{
    public class StakingAffiliateService : IStakingAffiliateService
    {
        private readonly IStakingAffiliateRepository _stakingAffiliateRepository;
        private readonly IUnitOfWork _unitOfWork;

        public StakingAffiliateService(
            IStakingAffiliateRepository stakingAffiliateRepository,
            IUnitOfWork unitOfWork)
        {
            _stakingAffiliateRepository = stakingAffiliateRepository;
            _unitOfWork = unitOfWork;
        }

        public PagedResult<StakingAffiliateViewModel> GetAllPaging(string keyword, string appUserId, int pageIndex, int pageSize)
        {
            var query = _stakingAffiliateRepository.FindAll(x => x.AppUser);

            if (!string.IsNullOrEmpty(keyword))
                query = query.Where(x => x.AppUser.Email.Contains(keyword)
                || x.AppUser.Sponsor.Value.ToString().Contains(keyword));

            if (!string.IsNullOrWhiteSpace(appUserId))
                query = query.Where(x => x.AppUserId.ToString() == appUserId);

            var totalRow = query.Count();
            var data = query.OrderByDescending(x => x.Id)
                .Skip((pageIndex - 1) * pageSize).Take(pageSize)
                .Select(x => new StakingAffiliateViewModel()
                {
                    Id = x.Id,
                    Amount = x.Amount,
                    StakingRewardId = x.StakingRewardId,
                    AppUserId = x.AppUserId,
                    AppUserName = x.AppUser.UserName,
                    Sponsor = $"ICD{ x.AppUser.Sponsor}",
                    DateCreated = x.DateCreated
                }).ToList();

            return new PagedResult<StakingAffiliateViewModel>()
            {
                CurrentPage = pageIndex,
                PageSize = pageSize,
                Results = data,
                RowCount = totalRow
            };
        }

        public void Add(StakingAffiliateViewModel model)
        {
            var transaction = new StakingAffiliate()
            {
                Amount = model.Amount,
                StakingRewardId = model.StakingRewardId,
                AppUserId = model.AppUserId,
                DateCreated = model.DateCreated,
            };

            _stakingAffiliateRepository.Add(transaction);
        }

        public void Save()
        {
            _unitOfWork.Commit();
        }
    }
}
