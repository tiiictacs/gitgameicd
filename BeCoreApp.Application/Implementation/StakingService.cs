﻿using BeCoreApp.Application.Interfaces;
using BeCoreApp.Application.ViewModels.System;
using BeCoreApp.Data.Entities;
using BeCoreApp.Data.Enums;
using BeCoreApp.Data.IRepositories;
using BeCoreApp.Infrastructure.Interfaces;
using BeCoreApp.Utilities.Dtos;
using BeCoreApp.Utilities.Extensions;
using System;
using System.Linq;

namespace BeCoreApp.Application.Implementation
{
    public class StakingService
        : IStakingService
    {
        private readonly IStakingRepository _stakingRepository;
        private readonly IUnitOfWork _unitOfWork;

        public StakingService(
            IStakingRepository stakingRepository,
            IUnitOfWork unitOfWork)
        {
            _stakingRepository = stakingRepository;
            _unitOfWork = unitOfWork;
        }

        public PagedResult<StakingViewModel> GetAllPaging(string keyword, string appUserId, int pageIndex, int pageSize)
        {
            var query = _stakingRepository.FindAll(x => x.AppUser);

            if (!string.IsNullOrEmpty(keyword))
                query = query.Where(x => x.AppUser.Email.Contains(keyword)
                || x.AppUser.Sponsor.Value.ToString().Contains(keyword));

            if (!string.IsNullOrWhiteSpace(appUserId))
                query = query.Where(x => x.AppUserId.ToString() == appUserId);

            var totalRow = query.Count();
            var data = query.OrderByDescending(x => x.Id)
                .Skip((pageIndex - 1) * pageSize).Take(pageSize)
                .Select(x => new StakingViewModel()
                {
                    Id = x.Id,
                    IsGetedCommission = x.ReceiveLatest.Date == DateTime.Now.Date,
                    Package = x.Package,
                    PackageName = x.Package.GetDescription(),
                    InterestRate = x.InterestRate,
                    ReceiveAmount = x.ReceiveAmount,
                    ReceiveLatest = x.ReceiveLatest,
                    ReceiveTimes = x.ReceiveTimes,
                    StakingAmount = x.StakingAmount,
                    StakingTimes = x.StakingTimes,
                    TimeLine = x.TimeLine,
                    TimeLineName = x.TimeLine.GetDescription(),
                    AppUserId = x.AppUserId,
                    AppUserName = x.AppUser.UserName,
                    Sponsor = $"ICD{ x.AppUser.Sponsor}",
                    DateCreated = x.DateCreated,
                    Type = x.Type,
                    TypeName = x.Type.GetDescription()
                }).ToList();

            return new PagedResult<StakingViewModel>()
            {
                CurrentPage = pageIndex,
                PageSize = pageSize,
                Results = data,
                RowCount = totalRow
            };
        }
        public decimal GetTotalPackage(Guid userId, StakingType? type)
        {
            decimal totalStaking = 0;
            var staking = _stakingRepository.FindAll(x => x.AppUserId == userId);

            if (type != null)
                staking = staking.Where(x => x.Type == type);

            totalStaking = staking.Sum(x => x.StakingAmount);

            return totalStaking;
        }

        public decimal GetMaxPackage(Guid userId)
        {
            decimal maxPackage = _stakingRepository.FindAll(x => x.AppUserId == userId)
                .Max(x => x.StakingAmount);

            return maxPackage;
        }

        public StakingViewModel GetById(int id)
        {
            var model = _stakingRepository.FindById(id);
            if (model == null)
                return null;

            var staking = new StakingViewModel()
            {
                Id = model.Id,
                Package = model.Package,
                PackageName = model.Package.GetDescription(),
                InterestRate = model.InterestRate,
                ReceiveAmount = model.ReceiveAmount,
                ReceiveLatest = model.ReceiveLatest,
                ReceiveTimes = model.ReceiveTimes,
                StakingAmount = model.StakingAmount,
                StakingTimes = model.StakingTimes,
                TimeLine = model.TimeLine,
                TimeLineName = model.TimeLine.GetDescription(),
                AppUserId = model.AppUserId,
                AppUserName = model.AppUser.UserName,
                Sponsor = $"ICD{ model.AppUser.Sponsor}",
                DateCreated = model.DateCreated,
                Type = model.Type,
                TypeName = model.Type.GetDescription()
            };

            return staking;
        }

        public void Update(StakingViewModel model)
        {
            var staking = _stakingRepository.FindById(model.Id);

            staking.Package = model.Package;
            staking.InterestRate = model.InterestRate;
            staking.TimeLine = model.TimeLine;
            staking.StakingTimes = model.StakingTimes;
            staking.StakingAmount = model.StakingAmount;
            staking.ReceiveTimes = model.ReceiveTimes;
            staking.ReceiveAmount = model.ReceiveAmount;
            staking.ReceiveLatest = model.ReceiveLatest;
            staking.AppUserId = model.AppUserId;
            staking.DateCreated = model.DateCreated;
            staking.Type = model.Type;

            _stakingRepository.Update(staking);
        }

        public void Add(StakingViewModel model)
        {
            var transaction = new Staking()
            {

                Package = model.Package,
                InterestRate = model.InterestRate,
                TimeLine = model.TimeLine,
                StakingTimes = model.StakingTimes,
                StakingAmount = model.StakingAmount,
                ReceiveTimes = model.ReceiveTimes,
                ReceiveAmount = model.ReceiveAmount,
                ReceiveLatest = model.ReceiveLatest,
                AppUserId = model.AppUserId,
                DateCreated = model.DateCreated,
                Type = model.Type
            };

            _stakingRepository.Add(transaction);
        }

        public void Save()
        {
            _unitOfWork.Commit();
        }
    }
}
