﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace BeCoreApp.Data.Enums
{
    public enum TransactionUnit
    {
        [Description("ICD")]
        ICD = 1,
        [Description("BNB")]
        BNB = 2
    }
}
