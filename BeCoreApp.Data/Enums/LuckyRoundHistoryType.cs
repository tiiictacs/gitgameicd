﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace BeCoreApp.Data.Enums
{
    public enum LuckyRoundHistoryType
    {
        [Description("Pay Lucky Round")]
        PayLuckyRound = 1,
        [Description("Win 50 ICD")]
        Win50ICD = 2,
        [Description("Win 500 ICD")]
        Win500ICD = 3,
        [Description("Win 1000 ICD")]
        Win1000ICD = 4,
        [Description("Win 3000 ICD")]
        Win3000ICD = 5,
        [Description("Good Luck")]
        GoodLuck = 6,
        [Description("Win 5000 ICD")]
        Win5000ICD = 7,
        [Description("Win Ticket")]
        WinTicket = 8,
        [Description("Win 0.5 BNB")]
        Win05BNB = 9
    }
}
