﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace BeCoreApp.Data.Enums
{
    public enum TransactionType
    {
        [Description("Airdrop")]
        Airdrop = 0,
        [Description("Buy ICD")]
        BuyICD = 1,
        [Description("Sell ICD")]
        SellICD = 2,
        [Description("Affiliate Buy ICD")]
        AffiliateBuyICD = 3,
        [Description("Pay Private Sale")]
        PayPrivateSale = 4,
        [Description("Pay PreSale")]
        PayPreSale = 5
    }
}
