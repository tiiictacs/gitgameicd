﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace BeCoreApp.Data.Enums
{
    public enum StakingPackage
    {
        [Description("Package 100 ICD")]
        Package100 = 100,
        [Description("Package 500 ICD")]
        Package500 = 500,
        [Description("Package 1,000 ICD")]
        Package1000 = 1000,
        [Description("Package 2,500 ICD")]
        Package2500 = 2500,
        [Description("Package 5,000 ICD")]
        Package5000 = 5000,
        [Description("Package 10,000 ICD")]
        Package10000 = 10000,
        [Description("Package 25,000 ICD")]
        Package25000 = 25000,
        [Description("Package 50,000 ICD")]
        Package50000 = 50000,
        [Description("Package 100,000 ICD")]
        Package100000 = 100000
    }
}