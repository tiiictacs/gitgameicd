﻿using BeCoreApp.Data.GameCrashEntities;
using BeCoreApp.Infrastructure.Interfaces;
using System.Collections.Generic;

namespace BeCoreApp.Data.IRepositories.CrashGame
{
    public interface IPlayRepository : IRepository<Play, long>
    {
        int UpdatePlayerBonus(Dictionary<long, double> bonuses);
    }
}
