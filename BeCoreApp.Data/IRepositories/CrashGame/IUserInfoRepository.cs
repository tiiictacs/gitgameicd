﻿using BeCoreApp.Data.GameCrashEntities;
using BeCoreApp.Infrastructure.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BeCoreApp.Data.IRepositories.CrashGame
{
    public interface IUserInfoRepository : IRepository<UserInfo, Guid>
    {
        int SetEndGameBonus(Dictionary<Guid, double> bonuses);
    }
}
