﻿using BeCoreApp.Data.GameCrashEntities;
using BeCoreApp.Infrastructure.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace BeCoreApp.Data.IRepositories.CrashGame
{
    
    public interface IRoundInfoRepository : IRepository<RoundInfo, int>
    {
    }
}
