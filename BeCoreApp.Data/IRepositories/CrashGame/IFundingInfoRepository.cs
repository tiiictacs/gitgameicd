﻿using BeCoreApp.Data.GameCrashEntities;
using BeCoreApp.Infrastructure.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BeCoreApp.Data.IRepositories.CrashGame
{
    public interface IFundingInfoRepository : IRepository<FundingInfo, long>
    {
        double GetBankRoll();
    }
}
