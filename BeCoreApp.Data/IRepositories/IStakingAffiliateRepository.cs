﻿using BeCoreApp.Data.Entities;
using BeCoreApp.Infrastructure.Interfaces;

namespace BeCoreApp.Data.IRepositories
{
    public interface IStakingAffiliateRepository : IRepository<StakingAffiliate, int>
    {

    }
}