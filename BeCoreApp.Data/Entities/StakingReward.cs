﻿using BeCoreApp.Data.Enums;
using BeCoreApp.Data.Interfaces;
using BeCoreApp.Infrastructure.SharedKernel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace BeCoreApp.Data.Entities
{
    [Table("StakingRewards")]
    public class StakingReward : DomainEntity<int>
    {
        [Required]
        public decimal PackageInterestRate { get; set; }

        [Required]
        public decimal SuddenInterestRate { get; set; }

        [Required]
        public decimal RealInterestRate { get; set; }

        [Required]
        public decimal Amount { get; set; }

        [Required]
        public DateTime DateCreated { get; set; }

        [Required]
        public int StakingId { get; set; }

        [ForeignKey("StakingId")]
        public virtual Staking Staking { set; get; }

        [Required]
        public Guid AppUserId { get; set; }

        [ForeignKey("AppUserId")]
        public virtual AppUser AppUser { set; get; }

        public virtual ICollection<StakingAffiliate> StakingAffiliates { get; set; }
    }
}
