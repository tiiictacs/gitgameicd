﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BeCoreApp.Data.GameCrashEntities
{
    public class GameInfoHistory
    {
        public long Id { get; set; }

        public long GameCrash { get; set; }

        public DateTime CreatedOn { get; set; }

        public string PlayerInfo { get; set; }
    }
}
