﻿using BeCoreApp.Infrastructure.SharedKernel;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace BeCoreApp.Data.GameCrashEntities
{

    [Table("RoundInfo")]
    public class RoundInfo : DomainEntity<int>
    {
        [Required]
        [StringLength(128)]
        public string RoundCode { get; set; }

        public DateTime CreatedOn { get;set;}

        public decimal TotalBet { get;set;}

        public decimal TotalWin { get;set;}

        public string ResultValue { get;set;}

        public int ResultFrom { get;set;}

        public int ActionLog { get;set;}
    }
}
