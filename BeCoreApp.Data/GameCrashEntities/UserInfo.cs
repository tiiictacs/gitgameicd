﻿using BeCoreApp.Infrastructure.SharedKernel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BeCoreApp.Data.GameCrashEntities
{
    [Table("UserInfo")]
    public class UserInfo : DomainEntity<Guid>
    {
        public double Balance { get;set;}

        public int GamePlays { get;set;}
    }
}
