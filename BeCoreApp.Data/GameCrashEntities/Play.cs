﻿using BeCoreApp.Infrastructure.SharedKernel;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace BeCoreApp.Data.GameCrashEntities
{
    [Table("Play")]
    public class Play : DomainEntity<long>
    {
        public Guid UserId { get;set;}

        public double? CashOut { get;set;}

        public int AutoCashOut { get;set;}

        public long GameId { get;set;}

        public DateTime CreatedOn { get;set;}

        public double Bet { get;set;}

        public double? Bonus { get;set;}
    }
}
