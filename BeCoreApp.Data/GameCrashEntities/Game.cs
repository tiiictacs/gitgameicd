﻿using BeCoreApp.Infrastructure.SharedKernel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BeCoreApp.Data.GameCrashEntities
{
    [Table("GameInfo")]
    public class GameInfo : DomainEntity<long>
    {
        public long GameCrash { get;set;}

        public DateTime CreatedOn { get;set;}

        public bool IsEnded { get;set;}

        public long HashId { get;set;}
    }
}
