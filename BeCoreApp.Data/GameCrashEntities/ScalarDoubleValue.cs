﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BeCoreApp.Data.GameCrashEntities
{
    
    [Keyless]
    public class ScalarDoubleValue
    {
        public double Value { get; set; }

        public override string ToString()
        {
            return Value.ToString();
        }
    }
}
