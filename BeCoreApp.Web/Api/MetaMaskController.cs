﻿using BeCoreApp.Application.Interfaces;
using BeCoreApp.Application.ViewModels.System;
using BeCoreApp.Data.Entities;
using BeCoreApp.Data.Enums;
using BeCoreApp.Data.IRepositories;
using BeCoreApp.Extensions;
using BeCoreApp.Infrastructure.Interfaces;
using BeCoreApp.Utilities.Constants;
using BeCoreApp.Utilities.Dtos;
using BeCoreApp.Web.Models.Reponses;
using BeCoreApp.Web.Models.RequestParams;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Nethereum.RPC.Eth.DTOs;
using Nethereum.Util;
using Nethereum.Web3;
using Polly;
using Polly.Retry;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BeCoreApp.Web.Api
{
    [Produces("application/json")]
    [Route("[Controller]/[action]")]
    [ApiController]
    public class MetaMaskController : Controller
    {
        private readonly IBotTelegramService _botTelegramService;
        private readonly IMetamaskTransactionRepository _metamaskTransaction;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IBlockChainService _blockChain;
        private readonly ILogger<MetaMaskController> _logger;
        private readonly AddressUtil _addressUtil = new AddressUtil();
        static AsyncRetryPolicy _policy = Policy
           .Handle<Exception>()
           .WaitAndRetryAsync(new[] {
                TimeSpan.FromSeconds(1),
                TimeSpan.FromSeconds(2),
                TimeSpan.FromSeconds(4),
                TimeSpan.FromSeconds(8),
                TimeSpan.FromSeconds(15)
               });

        public MetaMaskController(IBotTelegramService botTelegramService,
                                  IMetamaskTransactionRepository metamaskTransaction,
                                  IUnitOfWork unitOfWork,
                                  IBlockChainService blockChain,
                                  ILogger<MetaMaskController> logger)
        {
            _botTelegramService = botTelegramService;
            _metamaskTransaction = metamaskTransaction;
            _unitOfWork = unitOfWork;
            _blockChain = blockChain;
            _logger = logger;
        }

        [HttpPost]
        public async Task<IActionResult> InitializeTransactionMetaMask([FromBody] BuyWithMetamaskParams model)
        {
            try
            {
                _logger.LogInformation($"Start call InitializeTransactionMetaMask with param: ", model);

                //check whitelist
                var address = model.Address;
                //if (!CommonConstants.WhiteListMembers.Any(x => x.ToLower() == address.ToLower()))
                //{
                //    return BadRequest(new GenericResult(false,
                //        "Sorry, you are not the first 200 lucky user can buy this round, please wait for the next time."));
                //}

                //limit min
                if (model.BNBAmount < 0.1m)
                {
                    return BadRequest(new GenericResult(false, "Minimum buy 0.1 BNB"));
                }

                if (model.BNBAmount > 6)
                {
                    return BadRequest(new GenericResult(false, "Maximum buy 6 BNB"));
                }

                var result = _metamaskTransaction
                    .FindAll(x => x.AddressFrom == model.Address && x.MetaMaskState == MetaMaskState.Confirmed)
                    .Sum(x => x.BNBAmount);

                //limit amount per wallet
                if ((result + model.BNBAmount) > 6)
                {
                    return BadRequest(new GenericResult(false, "Maximum buy 6 BNB"));
                }

                decimal priceBNBBep20 = _blockChain.GetCurrentPrice("BNB", "USD");
                if (priceBNBBep20 == 0)
                    return BadRequest(new GenericResult(false, "There is a problem loading the currency value!"));

                var icdAmount = ConculateICDAmount(model.BNBAmount, priceBNBBep20);

                var transaction = new MetamaskTransaction()
                {
                    Id = Guid.NewGuid(),
                    BNBAmount = model.BNBAmount,
                    ICDAmount = icdAmount,
                    AddressFrom = model.Address,
                    AddressTo = CommonConstants.BEP20SYSTEMPuKey,
                    DateCreated = DateTime.Now,
                    MetaMaskState = MetaMaskState.Requested,
                    Type = MetaMaskTransactionType.Presale,
                    IsDevice = model.IsDevice,
                    WalletRequest = model.WalletRequest
                };

                _metamaskTransaction.Add(transaction);
                _unitOfWork.Commit();

                var convertor = new Nethereum.Hex.HexConvertors.HexUTF8StringConvertor();

                var message = new MetamaskMessage
                {
                    TransactionHex = convertor.ConvertToHex(transaction.Id.ToString()),
                    From = model.Address,
                    To = CommonConstants.BEP20SYSTEMPuKey,
                    Value = model.BNBAmount,
                    Gas = "0x55f0",//22000
                    GasPrice = "0x2540be400"
                };

                _logger.LogInformation($"Finish call InitializeTransactionMetaMask");
                return Ok(new GenericResult(true, message));
            }
            catch (Exception e)
            {
                return BadRequest(new GenericResult(false, e.Message));
            }
        }

        [HttpPost]
        public async Task<IActionResult> VerifyMetaMaskRequest([FromBody] string transactionHash)
        {
            try
            {
                _logger.LogInformation($"Start call VerifyMetaMaskRequest with transaction hash: {transactionHash}");
                //ensure transaction was completed successful
                var transactionReceipt = await _policy.ExecuteAsync<TransactionReceipt>(async () =>
                {
                    var result = await _blockChain.GetTransactionReceiptByTransactionID(transactionHash, CommonConstants.BEP20Url);
                    if (result == null)
                    {
                        throw new ArgumentNullException();
                    }

                    return result;
                });

                var transaction = await _blockChain.GetTransactionByTransactionID(transactionHash, CommonConstants.BEP20Url);

                var uft8Convertor = new Nethereum.Hex.HexConvertors.HexUTF8StringConvertor();
                var transactionId = uft8Convertor.ConvertFromHex(transaction.Input);

                var metamaskTransaction = _metamaskTransaction.FindById(Guid.Parse(transactionId));

                var balance = Web3.Convert.FromWei(transaction.Value);

                //send chat bot
                var depositMessage = _botTelegramService.BuildReportDepositPresaleMessage(
                    new Application.ViewModels.BotTelegram.PresaleMessageParam
                    {
                        AmountBNB = balance,
                        DepositAt = DateTime.Now,
                        AmountICD = metamaskTransaction.ICDAmount,
                        UserWallet = metamaskTransaction.AddressFrom,
                        SystemWallet = metamaskTransaction.AddressTo
                    });

                await _botTelegramService.SendMessageAsyncWithSendingBalance(Application.Implementation.ActionType.Deposit, depositMessage, CommonConstants.DepositGroup);

                if (transactionReceipt.Status.Value != 1)
                {
                    metamaskTransaction.DateUpdated = DateTime.Now;
                    metamaskTransaction.MetaMaskState = MetaMaskState.Failed;
                    metamaskTransaction.BNBTransactionHash = transactionHash;

                    _unitOfWork.Commit();
                    _logger.LogError($"VerifyMetaMaskRequest: TransactionReceipt's status was failed: {transactionReceipt.Status.Value}");

                    return BadRequest(new GenericResult(false, "Your transction was invalid. Please Contact administrator for support!"));
                }

                //compare transaction with blockchain transaction
                if (metamaskTransaction.MetaMaskState != MetaMaskState.Requested)
                {
                    metamaskTransaction.DateUpdated = DateTime.Now;
                    metamaskTransaction.MetaMaskState = MetaMaskState.Failed;
                    metamaskTransaction.BNBTransactionHash = transactionHash;

                    _unitOfWork.Commit();
                    _logger.LogError($"VerifyMetaMaskRequest: MetaMaskState was not matched: {metamaskTransaction.MetaMaskState}");

                    return BadRequest(new GenericResult(false, "Your transction was invalid. Please Contact administrator for support!"));
                }

                //compare transaction with blockchain transaction
                if (metamaskTransaction.AddressFrom.ToLower() != transaction.From.ToLower() || metamaskTransaction.AddressTo.ToLower() != transaction.To.ToLower())
                {
                    metamaskTransaction.DateUpdated = DateTime.Now;
                    metamaskTransaction.MetaMaskState = MetaMaskState.Failed;
                    metamaskTransaction.BNBTransactionHash = transactionHash;

                    _unitOfWork.Commit();
                    _logger.LogError($"VerifyMetaMaskRequest: Transaction's infor was not matched: ", transaction);

                    return BadRequest(new GenericResult(false, "Your transction was invalid. Please Contact administrator for support!"));
                }

                //compare amount
                if (balance != metamaskTransaction.BNBAmount)
                {
                    metamaskTransaction.DateUpdated = DateTime.Now;
                    metamaskTransaction.MetaMaskState = MetaMaskState.Failed;
                    metamaskTransaction.BNBTransactionHash = transactionHash;

                    _unitOfWork.Commit();
                    _logger.LogError($"VerifyMetaMaskRequest: Transaction's balance was not matched: {balance}");

                    return BadRequest(new GenericResult(false, "Your transction was invalid. Please Contact administrator for support!"));
                }

                metamaskTransaction.MetaMaskState = MetaMaskState.Confirmed;
                metamaskTransaction.BNBTransactionHash = transactionHash;
                metamaskTransaction.DateUpdated = DateTime.Now;
                _unitOfWork.Commit();

                //send icd
                var icdTransactionHash = await _blockChain
                           .SendERC20Async(CommonConstants.BEP20EXCHANGEPrKey,
                           metamaskTransaction.AddressFrom,
                           CommonConstants.BEP20ICDContract, metamaskTransaction.ICDAmount,
                           CommonConstants.BEP20ICDDP, CommonConstants.BEP20Url);

                if (!string.IsNullOrWhiteSpace(icdTransactionHash))
                {
                    metamaskTransaction.ICDAmount = metamaskTransaction.ICDAmount;
                    metamaskTransaction.ICDTransactionHash = icdTransactionHash;
                    _unitOfWork.Commit();

                    var receivedMessage = _botTelegramService.BuildReportReceivePresaleMessage(new Application.ViewModels.BotTelegram.PresaleMessageParam
                    {
                        AmountBNB = balance,
                        ReceivedAt = DateTime.Now,
                        AmountICD = metamaskTransaction.ICDAmount,
                        UserWallet = metamaskTransaction.AddressFrom,
                        SystemWallet = CommonConstants.BEP20EXCHANGEPuKey
                    });

                    await _botTelegramService.SendMessageAsyncWithSendingBalance(Application.Implementation.ActionType.Deposit, receivedMessage, CommonConstants.WithdrawGroup);
                }

                _logger.LogInformation($"End call VerifyMetaMaskRequest with transaction hash: {icdTransactionHash}");

                return Ok(new GenericResult(true, "Successed to Buy ICD"));
            }
            catch (Exception e)
            {
                _logger.LogError("Internal Error", e);

                try
                {
                    var transaction = await _blockChain.GetTransactionByTransactionID(transactionHash, CommonConstants.BEP20Url);

                    var uft8Convertor = new Nethereum.Hex.HexConvertors.HexUTF8StringConvertor();
                    var transactionId = uft8Convertor.ConvertFromHex(transaction.Input);

                    var metamaskTransaction = _metamaskTransaction.FindById(Guid.Parse(transactionId));

                    metamaskTransaction.BNBTransactionHash = transactionHash;
                    metamaskTransaction.MetaMaskState = MetaMaskState.Failed;

                    _unitOfWork.Commit();
                }
                catch 
                {
                }

                return BadRequest(new GenericResult(false, "Somethings went wrong! Please Contact administrator for support."));
            }
        }

        [HttpPost]
        public async Task<IActionResult> UpdateErrorMetaMask([FromBody] MetaMaskErrorParams model)
        {
            var convertor = new Nethereum.Hex.HexConvertors.HexUTF8StringConvertor();

            var transactionId = convertor.ConvertFromHex(model.TransactionHex);

            var result = _metamaskTransaction.FindById(Guid.Parse(transactionId));

            switch (model.ErrorCode)
            {
                case "4001":
                    result.MetaMaskState = MetaMaskState.Rejected;
                    break;
                case "-32603":
                    result.MetaMaskState = MetaMaskState.Failed;
                    break;
                default:
                    result.MetaMaskState = MetaMaskState.Failed;
                    break;
            }

            _unitOfWork.Commit();
            return Ok(new GenericResult(true, "Successed to update."));
        }

        [HttpGet("{amount}")]
        public async Task<IActionResult> GetAmountICDPerBNB(decimal amount)
        {
            try
            {
                decimal priceBNBBep20 = _blockChain.GetCurrentPrice("BNB", "USD");
                if (priceBNBBep20 == 0)
                    return new OkObjectResult(new GenericResult(false, "There is a problem loading the currency value!"));

                decimal amountICD = ConculateICDAmount(amount, priceBNBBep20);

                return Ok(new GenericResult(true, amountICD));
            }
            catch (Exception e)
            {
                return BadRequest(new GenericResult(false, "Invalid amount"));
            }

        }

        private decimal ConculateICDAmount(decimal bnbAmount, decimal priceBNBBep20)
        {
            decimal priceICD = 0.12M;
            var amountUSD = Math.Round(bnbAmount * priceBNBBep20, 2);
            return Math.Round(amountUSD / priceICD, 2);
        }
    }
}
