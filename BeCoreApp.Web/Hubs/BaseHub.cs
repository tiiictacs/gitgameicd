﻿using BeCoreApp.Extensions;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.SignalR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BeCoreApp.Web.Hubs
{
    [Authorize]
    public class BaseHub : Hub
    {
        public string CurrentUserName
        {
            get
            {
                if (Context.User.Identity.IsAuthenticated)
                {
                    var userNameClaim = Context.User.GetSpecificClaim("UserName");
                    return userNameClaim;
                }
                return "guess";
            }
        }

        public Guid CurrentUserId
        {
            get
            {
                if (Context.User.Identity.IsAuthenticated)
                {
                    var userIdClaim = Context.User.GetSpecificClaim("UserId");
                    if (!string.IsNullOrEmpty(userIdClaim))
                    {
                        return Guid.Parse(userIdClaim);
                    }
                }

                return Guid.Empty;
            }
        }

        public async Task SendErrorAsync(string description)
        {
            Console.WriteLine($"Warning: Send Client {description}");
            await Clients.Client(Context.ConnectionId).SendAsync("err", $"{description}");
        }

        public async Task SendAllAsync(string method, object data)
        {
            Console.WriteLine($"Warning: Send All Client {method}");
            await Clients.All.SendAsync(method, data);
        }

        public async Task SendClient(string method, object data)
        {
            await Clients.Client(Context.ConnectionId).SendAsync(method,data);
        }

        public async Task SendInternalErrorAsync(string description,string exception)
        {
            Console.WriteLine($"Warning: Send All {description}");
            await Clients.All.SendAsync("err", $"{description} - {exception}");
        }

        

        public async Task AddGroupAsync(string roomName)
        {
            await Groups.AddToGroupAsync(Context.ConnectionId, roomName);
        }

        public async Task RemoveFromGroupAsync(string roomName)
        {
            await Groups.RemoveFromGroupAsync(Context.ConnectionId, roomName);

            
        }
    }
}
