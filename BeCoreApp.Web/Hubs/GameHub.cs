﻿using BeCoreApp.Application.GameViewModels.CrashGame;
using BeCoreApp.Application.GameViewModels.GameInfo;
using BeCoreApp.Application.GameViewModels.UserInfo;
using BeCoreApp.Application.Interfaces;
using BeCoreApp.Application.Interfaces.CrashGame;
using BeCoreApp.Data.Entities;
using BeCoreApp.Data.GameCrashEntities;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.SignalR;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading;
using System.Threading.Tasks;

namespace BeCoreApp.Web.Hubs
{
    public class GameHub<TUser, TRole> : BaseHub
        where TUser : IdentityUser
        where TRole : IdentityRole
    {

        private readonly IUserService _userService;
        private readonly RoleManager<AppRole> _roleManager;
        private readonly UserManager<AppUser> _userManager;
        private readonly IGameInfoService _gameInfoService;

        private static int clientCount = 0;
        private static bool isInit = false;


        private const int TickRate = 150;// ping the client every X miliseconds

        private const long AfterCrashTime = 3000;// how long from game_crash -> game_starting

        private const int RestartTime = 5000;// How long from  game_starting -> game_started

        #region Config Game

        private static double BankRoll = 0;

        private static double MaxWin = 0;

        private static bool GameShuttingDown = false;

        private static DateTime StartTime = DateTime.Now;// time game started. If before game started, is an estimate...

        private static long CrashPoint = 0;// when the game crashes, 0 means instant crash

        private static int GameDuration = 0;// how long till the game will crash..

        private static double ForcePoint = 0; // The point we force terminate the game

        private static string STATE = "ENDED";// 'STARTING' | 'BLOCKING' | 'IN_PROGRESS' |  'ENDED'

        private static int PendingCount = 0;

        private static readonly List<Guid> Pending = new();

        private static Dictionary<Guid, UserInfoViewModel> Players;

        private static SortedList<Guid, UserInfoViewModel> Joined;// A list of joins, before the game is in progress

        private static List<GameHistoryInfo> GameHistory;


        private static long GameId = 0;


        private static string LastHash = "";

        private static string Hash = "";

        private static long LastHashId = 0;

        #endregion

        public GameHub(IUserService userService,
            RoleManager<AppRole> roleManager,
            UserManager<AppUser> userManager,
            IGameInfoService gameInfoService)
        {
            _gameInfoService = gameInfoService;
            _roleManager = roleManager;
            _userManager = userManager;
            _userService = userService;


        }



        public async Task InitGameInfo()
        {
            if (isInit)
                return;

            isInit = true;

            Players = new Dictionary<Guid, UserInfoViewModel>();

            Joined = new SortedList<Guid, UserInfoViewModel>();

            var lastGameInfo = _gameInfoService.GetLastGameInfo();

            GameId = lastGameInfo.Id;

            var gameHistories = _gameInfoService.GetGameInfoHistories();

            GameHistory = gameHistories;

            LastHash = lastGameInfo.Hash;

            LastHashId = lastGameInfo.HashId;

            BankRoll = _gameInfoService.GetBankRoll();

            await RunGame();

        }



        public async Task RunGame()
        {
            LastHashId++;
            var info = _gameInfoService.CreateGame(LastHashId);

            STATE = "STARTING";
            CrashPoint = info.GameCrash;
            Hash = info.Hash;
            // LastHashId++;
            StartTime = DateTime.Now.AddMilliseconds(RestartTime);
            GameDuration = (int)Math.Ceiling(InverseGrowth(CrashPoint + 1));
            MaxWin = Math.Round(BankRoll * 0.03);

            GameProcessResult gameInfo = new()
            {
                GameId = info.Id,
                MaxWin = MaxWin,
                TimeTillStart = RestartTime
            };

            await SendAllAsync("game_starting", gameInfo);

            Thread.Sleep(RestartTime);

            await BlockGame();
        }

        public async Task StartGame()
        {
            STATE = "IN_PROGRESS";
            StartTime = DateTime.Now;
            PendingCount = 0;

            var bets = new List<UserBetViewModel>();

            foreach (var user in Joined.Values)
            {
                var uInfo = new UserBetViewModel
                {
                    Amount = user.Bet,
                    UserId = user.UserId,
                    UserName = user.UserName
                };
                bets.Add(uInfo);

                if (Players.ContainsKey(user.UserId))
                    Players.Remove(user.UserId);

                Players.Add(user.UserId, user);
            }

            Joined.Clear();

            await SendAllAsync("game_started", bets);

            SetForcePoint();

            await CallTick(0);
        }

        public async Task BlockGame()
        {
            STATE = "BLOCKING";// we're waiting for pending bets..

            if (PendingCount > 0)
            {
                Console.WriteLine($"Delaying game by 100ms for {PendingCount} joins");
                Thread.Sleep(100);
            }
            await StartGame();
        }

        public async Task CallTick(double elapsed)
        {
            var left = GameDuration - elapsed;
            var nextTick = Math.Max(0, Math.Min(left, TickRate));

            Thread.Sleep(Convert.ToInt32(nextTick));

            await RunTick();

        }

        async Task CashOutAll(double at)
        {
            if (STATE != "IN_PROGRESS")
                return;

            await RunCashOuts(at);

            if (at > CrashPoint)
                return; // game already crashed, sorry guys

            var tasks = new List<UserInfoViewModel>();

            foreach (var play in Players.Values)
            {
                if (play.Status == "PLAYING")
                {
                    tasks.Add(play);

                    await DoCashOut(play, at);
                }
            }

            Console.WriteLine($"Needing to force cash out: {tasks.Count} players");
        }

        async Task RunTick()
        {
            var elapsed = (DateTime.Now - StartTime).TotalMilliseconds;
            var at = GrowthFunc(elapsed);

            await RunCashOuts(at);

            if (ForcePoint <= at && ForcePoint <= CrashPoint)
            {

                await CashOutAll(ForcePoint);
                await EndGame(true);
                //self.cashOutAll(self.forcePoint, function(err) {
                //    console.log('Just forced cashed out everyone at: ', self.forcePoint, ' got err: ', err);

                //    endGame(true);
                //});
                return;
            }

            // and run the next

            if (at > CrashPoint)
            {
                await EndGame(false);
            }
            else
                await Tick(elapsed);
        }

        async Task Tick(double elapsed)
        {
            await SendAllAsync("game_tick", elapsed);
            await CallTick(elapsed);
        }

        async Task EndGame(bool force)
        {

            var crashTime = DateTime.Now;

            var bonuses = new List<PlayerBonusCalcModel>();

            if (CrashPoint != 0)
            {
                bonuses = CalcBonuses();

                double givenOut = 0;
                foreach (var player in Players.Values)
                {
                    givenOut += player.Bet * 0.01;

                    if (player.Status == "CASHED_OUT")
                    {
                        double given = player.StoppedAt * (player.Bet / 100);
                        givenOut += given;
                    }
                }

                BankRoll -= givenOut;
            }

            LastHash = Hash;

            var playerInfo = GetInfo().PlayerInfo;



            await SendAllAsync("game_crash", new EndGameInfoResult
            {
                GameCrash = CrashPoint,
                Elapsed = GameDuration,
                Forced = force,
                Hash = LastHash
            });

            GameHistory.Add(new GameHistoryInfo
            {
                Created = StartTime,
                GameCrash = CrashPoint,
                GameId = GameId,
                Hash = LastHash,
                PlayerInfo = playerInfo

            });

            dbTimeout();

            _gameInfoService.EndGame(GameId, bonuses);

            if (GameShuttingDown)
                await SendAllAsync("shutdown", true);
            else
            {
                int wait = Convert.ToInt32((crashTime.AddMilliseconds(AfterCrashTime) - DateTime.Now).TotalMilliseconds);

                Thread.Sleep(wait);
                await RunGame();
            }

            STATE = "ENDED";
        }

        void dbTimeout()
        {
            Thread.Sleep(1000);
        }


        List<PlayerBonusCalcModel> CalcBonuses()
        {
            var sorted = Players.Values.OrderByDescending(d => d.StoppedAt).ToList();

            var results = new List<PlayerBonusCalcModel>();

            if (sorted.Count == 0)
                return results;


            double bonusPool = 0;
            double largestBet = 0;

            foreach (var player in sorted)
            {
                var bet = player.Bet;

                bonusPool += bet / 100;

                largestBet = Math.Max(largestBet, bet);


            }

            var maxWinRatio = bonusPool / largestBet;

            var i = 0;
            while (i < sorted.Count)
            {
                var tmp = new List<UserInfoViewModel>();
                double betAmount = 0;
                var sa = sorted[i].StoppedAt;
                for (; i < sorted.Count && sorted[i].StoppedAt == sa; ++i)
                {
                    betAmount += sorted[i].Bet;
                    tmp.Add(sorted[i]);
                }

                SlideSameStoppedAt(results, tmp, betAmount, maxWinRatio, bonusPool);
            }

            return results;
            //List<UserInfoViewModel> result = new ();
        }

        void SlideSameStoppedAt(
            List<PlayerBonusCalcModel> results,
            List<UserInfoViewModel> listOfRecords,
            double totalBetAmount,
            double maxWinRatio,
            double bonusPool)
        {
            if (bonusPool <= 0)
                return;

            var toAllocAll = Math.Min(totalBetAmount * maxWinRatio, bonusPool);

            for (var i = 0; i < listOfRecords.Count; ++i)
            {
                var toAlloc = Math.Round((listOfRecords[i].Bet / totalBetAmount) * toAllocAll);

                if (toAlloc <= 0)
                    continue;

                bonusPool -= toAlloc;

                var playId = listOfRecords[i].PlayId;

                var user = listOfRecords[i];


                results.Add(new PlayerBonusCalcModel
                {
                    PlayId = playId,
                    Amount = toAlloc,
                    User = user
                });
            }
        }


        async Task RunCashOuts(double at)
        {
            bool update = false;
            // Check for auto cashouts

            foreach (var play in Players.Values)
            {
                if (play.Status == "CASHED_OUT")
                    continue;

                if (play.AutoCashOut <= at && play.AutoCashOut <= CrashPoint && play.AutoCashOut <= ForcePoint)
                {
                    await DoCashOut(play, play.AutoCashOut);
                    //self.doCashOut(play, play.AutoCashOut, function(err) {
                    //    if (err)
                    //        console.log('[INTERNAL_ERROR] could not auto cashout ', playerUserName, ' at ', play.autoCashOut);
                    //});
                    update = true;
                }
            }

            if (update)
            {
                SetForcePoint();
            }
        }

        void SetForcePoint()
        {
            double totalBet = 0; // how much satoshis is still in action
            double totalCashedOut = 0; // how much satoshis has been lost

            foreach (var play in Players.Values)
            {
                if (play.Status == "CASHED_OUT")
                {
                    var amount = play.Bet * (play.StoppedAt - 100) / 100;
                    totalCashedOut += amount;
                }
                else if (play.Status == "PLAYING")
                {
                    totalBet += play.Bet;
                }
            }

            if (totalBet == 0)
            {
                ForcePoint = int.MaxValue; // the game can go until it crashes, there's no end.
            }
            else
            {
                var left = MaxWin - totalCashedOut - (totalBet * 0.01);

                var ratio = (left + totalBet) / totalBet;

                // in percent
                ForcePoint = Math.Max(Math.Floor(ratio * 100), 101);
            }
        }



        async Task DoCashOut(UserInfoViewModel play, double at)
        {
            var player = Players[play.UserId];
            if (player.Status == "PLAYING")
            {
                player.Status = "CASHED_OUT";
                player.StoppedAt = at;
            }

            var won = Convert.ToDouble(player.Bet) / 100 * at;

            await SendAllAsync("cashed_out", new CashedOutResultViewModel
            {
                StoppedAt = at,
                UserName = play.UserName
            });

            _gameInfoService.CashOut(play.UserId, play.PlayId, won);
        }


        public override async Task OnConnectedAsync()
        {
            await InitGameInfo();
            //await Clients.All.SendAsync("pong", "connection");

            if (isInit)
            {
                var gameInfo = GetJoinGameInfo();

                await SendClient("join", JsonConvert.SerializeObject(gameInfo));
            }

            await base.OnConnectedAsync();

        }

        JoinInfoResult GetJoinGameInfo()
        {

            if (!Players.ContainsKey(CurrentUserId))
            {
                var userInfo = _gameInfoService.GetUserInfo(CurrentUserId);
                if (userInfo != null)
                {
                    Players.Add(CurrentUserId, new UserInfoViewModel
                    {
                        UserId = CurrentUserId,
                        UserName = CurrentUserName,
                        Balance = userInfo.Balance
                    });
                }

            }


            var gameInfo = GetInfo();


            return new JoinInfoResult
            {
                GameId = gameInfo.GameId,
                GameState = gameInfo.State,
                IsConnected = true,
                LastHash = gameInfo.LastHash,
                MaxWin = gameInfo.MaxWin,
                PlayerInfo = gameInfo.PlayerInfo,
                StartTime = gameInfo.CreatedOn,
                UserName = CurrentUserName,
                TableHistory = GameHistory,
                Elapsed = gameInfo.Elapsed
            };

        }

        public async Task Ping(string msg)
        {
            await Clients.All.SendAsync("pong", msg);
        }



        public async Task OnCashedOutAsync()
        {
            if (STATE != "IN_PROGRESS")
            {
                await SendErrorAsync("GAME_NOT_IN_PROGRESS");
                return;
            }

            var elapsed = (DateTime.Now - StartTime).TotalMilliseconds;

            var at = GrowthFunc(elapsed);
            if (!Players.ContainsKey(CurrentUserId))
            {
                await SendErrorAsync("NO_BET_PLACED");
                return;
            }

            var play = Players[CurrentUserId];

            if (play.AutoCashOut <= at)
                at = play.AutoCashOut;

            if (ForcePoint <= at)
                at = ForcePoint;


            if (at > CrashPoint)
            {

                await SendErrorAsync("GAME_ALREADY_CRASHED");
                return;
            }


            if (play.Status == "CASHED_OUT")
            {
                await SendErrorAsync("ALREADY_CASHED_OUT");
                return;
            }

            await DoCashOut(play, at);
            SetForcePoint();
        }


        public GameInfoSummary GetInfo()
        {
            List<UserInfoViewModel> playerInfo = new();

            foreach (var player in Players.Values)
            {
                var user = new UserInfoViewModel
                {
                    Bet = player.Bet
                };

                if (player.Status == "CASHED_OUT")
                {
                    user.StoppedAt = player.StoppedAt;
                }

                playerInfo.Add(user);
            }


            var res = new GameInfoSummary
            {
                State = STATE,
                GameId = GameId,
                LastHash = LastHash,
                MaxWin = MaxWin,
                Elapsed = (DateTime.Now - StartTime).TotalMilliseconds,
                PlayerInfo = playerInfo,
                Joined = Joined.Values.Select(d => d.UserName).ToList(),
                CreatedOn = StartTime,

            };

            if (STATE == "ENDED")
                res.CrashAt = CrashPoint;

            return res;
        }


        public async Task OnPlaceBetAsync(double betAmount, int AutoCashOut)
        {
            if (STATE != "STARTING")
            {
                await SendErrorAsync("GAME_IN_PROGRESS");
                return;
            }

            if (Pending.Any(d => d == CurrentUserId))
            {
                await SendErrorAsync("ALREADY_PLACED_BET");
                return;
            }

            Pending.Add(CurrentUserId);

            PendingCount++;

            var playId = _gameInfoService.PlaceBet(CurrentUserId, betAmount, AutoCashOut, GameId);

            PendingCount--;

            if (playId <= 0)
            {
                await SendErrorAsync("Cant place bet");
                return;
            }

            BankRoll += Convert.ToDouble(betAmount);

            Joined.Add(CurrentUserId, new UserInfoViewModel
            {
                Bet = betAmount,
                UserName = CurrentUserName,
                AutoCashOut = AutoCashOut,
                PlayId = playId,
                Status = "PLAYING"
            });

            var index = Joined.IndexOfKey(CurrentUserId);

            var result = new PlayerBetResult
            {
                UserName = CurrentUserName,
                Index = index
            };


            await SendAllAsync("player_bet", JsonConvert.SerializeObject(result));

        }

        public override async Task OnDisconnectedAsync(Exception exception)
        {
            Console.WriteLine($"Client disconnect, left {clientCount}");

            //DisconnectPlayer();

            await base.OnDisconnectedAsync(exception);
        }

        void DisconnectPlayer()
        {
            if (Players.ContainsKey(CurrentUserId))
                Players.Remove(CurrentUserId);
        }

        public async Task OnShutDown()
        {
            GameShuttingDown = true;
            await SendAllAsync("shuttingdown", true);

            if (STATE == "ENDED")
            {
                await SendAllAsync("shuttingdown", true);
            }
        }



        double GrowthFunc(double ms)
        {
            var r = 0.00006;
            return Math.Floor(100 * Math.Pow(Math.E, r * ms));
        }

        double InverseGrowth(long crashPoint)
        {
            var c = 16666.666667;
            return c * Math.Log(0.01 * crashPoint);
        }

    }
}
