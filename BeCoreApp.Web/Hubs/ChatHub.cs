﻿using BeCoreApp.Application.GameViewModels.GameInfo;
using BeCoreApp.Application.GameViewModels.UserInfo;
using BeCoreApp.Application.Interfaces;
using BeCoreApp.Application.Interfaces.CrashGame;
using BeCoreApp.Extensions;
using Microsoft.AspNetCore.SignalR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BeCoreApp.Web.Hubs
{
    public class ChatHub : Hub
    {
        public async Task Send(string name, string message)
        {
            // Call the broadcastMessage method to update clients.
            await Clients.All.SendAsync("broadcastMessage", name, message);
        }
    }
}
