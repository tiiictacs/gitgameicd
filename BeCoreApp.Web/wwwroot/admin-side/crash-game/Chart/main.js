
var Clib = {
    MAX_BET: 100000,
    STOP_PREDICTING_LAPSE: 300,
    formatSatoshis: function (n, decimals) {
        return formatDecimals(n / 100, decimals);
    },

    formatDecimals: function (n, decimals) {
        if (typeof decimals === 'undefined') {
            if (n % 100 === 0)
                decimals = 0;
            else
                decimals = 2;
        }
        return n.toFixed(decimals).toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,");
    },

    payout: function (betSize, ms) {
        return betSize * Math.pow(Math.E, (0.00006 * ms));
    },

    payoutTime: function (betSize, payout) {
        return Math.log(payout / betSize) / 0.00006;
    },

    seed: function (newSeed) {
        rng = Math.random(); (newSeed);
    },

    parseBet: function (betString) {
        betString = String(betString);

        if (!/^\d+k*$/.test(betString))
            return new Error('Bet may only contain digits, and k (to mean 1000)');

        var bet = parseInt(betString.replace(/k/g, '000'));

        if (bet < 1)
            return new Error('The bet should be at least 1 bit');

        if (bet * 100 > MAX_BET)
            return new Error('The bet must be less no more than ' + formatSatoshis(MAX_BET) + ' bits');

        if (_.isNaN(bet) || Math.floor(bet) !== bet)
            return new Error('The bet should be an integer greater than or equal to one');

        return bet;
    },

    parseAutoCash: function (autoCashString) {
        var co = autoCashString;

        if (!/^\d+(\.\d{1,2})?$/.test(co))
            return new Error('Invalid auto cash out amount');

        co = parseFloat(co);
        console.assert(!_.isNaN(co));

        if (co < 1)
            return new Error('The auto cash out amount should be bigger than 1');

        return co;
    },

    winProb: function (amount, cashOut) {

        // The cash out factor that we need to get the cashOut with our wager.
        var factor = Math.ceil(100 * cashOut / amount);

        /* The probability that the second phase of the RNG chooses a lower
         crash point. This is derived in the following way:

         Let r be a random variable uniformly distributed on [0,1) and let
         p be 1/(1-r). Then we need to calculate the probability of
         floor(100 * (p - (p - 1)*0.01)) < factor

         Using the fact that factor is integral and solving for r yields
         r < 1 - 99 / (f-1)

         Because r is uniformly distributed the probability is identical
         to the right hand side.

         Combining with the first phase of the RNG we get the probability
         of losing
         1/101 + 100/101 * (1 - 99 / (factor-1))
         and the win probability
         1 - 1/101 - 100/101 * (1 - 99 / (factor-1)).
         = 100/101 - 100/101 * (1 - 99 / (factor-1))
         = 100/101 * (1 - (1 - 99 / (factor-1))
         = 100/101 * (99 / (factor-1))
         = 100/101 * (99 / (factor-1))
         = 9900 / (101*(factor-1))
         */
        return 9900 / (101 * (factor - 1));
    },

    profit: function (amount, cashOut) {

        // The factor that we need to get the cash out with our wager.
        var factor = Math.ceil(100 * cashOut / amount);

        // We calculate the profit with the factor instead of using the
        // difference between cash out and wager amount.
        return amount * (factor - 100) / 100;
    },

    houseExpectedReturn: function (amount, cashOut) {

        var p1, p2, p3;
        var v1, v2, v3;

        // Instant crash.
        p1 = 1 / 101;
        v1 = amount;

        // Player win.
        p2 = this.winProb(amount, cashOut);
        v2 = - 0.01 * amount - this.profit(amount, cashOut);

        // Player loss.
        p3 = 1 - p1 - p2;
        v3 = 0.99 * amount;

        // Expected value.
        return p1 * v1 + p2 * v2 + p3 * v3;
    },

    capitaliseFirstLetter: function (string) {
        return string.charAt(0).toUpperCase() + string.slice(1);
    },

    isInteger: function (nVal) {
        return typeof nVal === "number" && isFinite(nVal) && nVal > -9007199254740992 && nVal < 9007199254740992 && Math.floor(nVal) === nVal;
    },

    isNumber: function (nVal) {
        return typeof nVal === "number" && isFinite(nVal) && nVal > -9007199254740992 && nVal < 9007199254740992;
    },

    //Returns plural or singular, for a given amount of bits.
    grammarBits: function (bits) {
        return bits <= 100 ? 'bit' : 'bits';
    },

    //Calculate the payout based on the time
    growthFunc: function (ms) {
        var r = 0.00006;
        return Math.floor(100 * Math.pow(Math.E, r * ms)) / 100;
    },

    //A better name
    calcGamePayout: function (ms) {
        var gamePayout = this.growthFunc(ms);
        return gamePayout;
    },

    //Returns the current payout and stops when lag, use this time to calc game payout with lag
    getElapsedTimeWithLag: function (engine) {
        if (engine.gameState == 'IN_PROGRESS') {
            var elapsed;
            if (engine.lag)
                elapsed = engine.lastGameTick - engine.startTime + this.STOP_PREDICTING_LAPSE; //+ STOP_PREDICTING_LAPSE because it looks better
            else
                elapsed = this.getElapsedTime(engine.startTime);

            return elapsed;
        } else {
            return 0;
        }
    },

    //Just calculates the elapsed time
    getElapsedTime: function (startTime) {
        return Date.now() - startTime;
    },

    isMobileOrSmall: function () {
        return window.getComputedStyle(document.getElementById('handheld-detection'), null).display == 'none';
    },

    loadCss: function (url, id) {
        var link = document.createElement("link");
        link.type = "text/css";
        link.rel = "stylesheet";
        link.href = url;
        link.id = id;
        document.getElementsByTagName("head")[0].appendChild(link);
    },

    removeCss: function (id) {
        var el = document.getElementById(id);
        if (el && el.parentNode) {
            el.parentNode.removeChild(el);
        }
    },

    localOrDef: function (name, defaultValue) {
        return localStorage[name] ? localStorage[name] : defaultValue;
    },

    isInvalidUsername: function (username) {
        if (typeof username !== 'string') return 'NOT_STRING';
        if (username.length === 0) return 'NOT_PROVIDED';
        if (username.length < 3) return 'TOO_SHORT';
        if (username.length > 50) return 'TOO_LONG';
        if (!/^[a-z0-9_\-]*$/i.test(username)) return 'INVALID_CHARS';
        if (username === '__proto__') return 'INVALID_CHARS';
        return false;
    }
}

var ChartGraphics = {
    loadWaiting: false,
    usepopupnotifications: false,
    topcartselector: '',
    topwishlistselector: '',
    flyoutcartselector: '',
    getParentNodeFunc: '',
    rendering: false,
    Result: 3.9,
    Engine: {
        lastGameTick: 1635780423652,

        Animations: {
            NYAN_CAT_TRIGGER_MS: 200 //115129ms ~ 1000x // 11552ms ~ 2x
        },

        /** The engine is connected to the server, if not connected, all fields are unreadable */
        isConnected: false,

        /** The username or null if is not logged in */
        username: null,

        /** The balance of the user */
        balanceSatoshis: null,

        /** Max bet will be sent by the server in the future, for now is a constant **/
        maxBet: 100000000,

        /** Array containing chat history */
        //chat: [];

        /** Object containing the game history */
        tableHistory: [],

        /** Array of the user names of the current players who joined the game, while the game is STARTING
         * sorted by bet by the server but the client doesn't know the bet amount
         * its empty in the other states */
        joined: [],

        /** Object containing the current game players and their status, this is saved in game history every game crash
         * cleared in game_starting.
         * e.g: { user1: { bet: satoshis, stopped_at: 200 }, user2: { bet: satoshis } }
         */
        playerInfo: [],

        /**
         * The state of the game
         * Possible states: IN_PROGRESS, ENDED, STARTING
         */
        gameState: null,

        /** Creation time of current game. This is the server time, not clients.. **/
        created: null,

        /** The game id of the current game */
        gameId: null,

        /** How much can be won this game */
        maxWin: null,

        /**
         * Client side times:
         * if the game is pending, startTime is how long till it starts
         * if the game is running, startTime is how long its running for
         * if the game is ended, startTime is how long since the game started
         */
        startTime: null,

        /** time from the game_starting event to game_started event **/
        timeTillStart: null,

        /** If you are currently placing a bet
         * True if the bet is queued (nextBetAmount)
         * True if the bet was sent to the server but the server has not responded yet
         *
         * Cleared in game_started, its possible to receive this event before receiving the response of
         */
        placingBet: false,

        /** True if cashing out.. */
        cashingOut: false,

        /**
         * If a number, how much to bet next round
         * Saves the queued bet if the game is not 'game_starting', cleared in 'bet_placed' by us and 'game_started' and 'cancel bet'
         */
        nextBetAmount: null,

        /** Complements nextBetAmount queued bet with the queued autoCashOut */
        nextAutoCashout: null,

        /** Store the id of the timer to check for lag **/
        tickTimer: null,

        /** Tell if the game is lagging but only  when the game is in progress **/
        lag: false,

        /** The hash of the last game **/
        lastHash: null,

        /** Animation Events triggers**/
        nyan: false,
    },
    startRendering: function () {
        if (this.rendering === false) {
            this.rendering = true;
            const canvasNode = document.getElementById('canvas');
            if (!canvasNode.getContext)
                return console.error('No canvas');

            this.ctx = canvasNode.getContext('2d');
            var parentNode = document.getElementById('chart-inner-container');
            this.canvasWidth = parentNode.clientWidth;
            this.canvasHeight = parentNode.clientHeight;
            this.canvas = canvasNode;
            this.configPlotSettings();
            this.animRequest = window.requestAnimationFrame(this.render());

            window.addEventListener('resize', this.onWindowResizeBinded);
        }
    },
    stopRendering: function () {
        this.rendering = false;
        window.removeEventListener('resize', this.onWindowResizeBinded);
    },

    onChange: function () {
        this.configPlotSettings();
    },

    render: function () {
        if (!this.rendering)
            return;
        this.calcGameData();
        this.calculatePlotValues();
        this.clean();
        this.drawGraph();
        this.drawAxes();
        this.drawGameData();
        this.animRequest = window.requestAnimationFrame(this.render.bind(this));
    },

    /** On windows resize adjust the canvas size to the canvas parent size */
    onWindowResize: function () {
        var parentNode = document.getElementById('chart-inner-container');
        this.canvasWidth = parentNode.clientWidth;
        this.canvasHeight = parentNode.clientHeight;
        this.configPlotSettings();
    },

    configPlotSettings: function () {
        this.canvas.width = this.canvasWidth;
        this.canvas.height = this.canvasHeight;
        this.themeWhite = (this.theme === 'white');
        this.plotWidth = this.canvasWidth - 30;
        this.plotHeight = this.canvasHeight - 20; //280
        this.xStart = this.canvasWidth - this.plotWidth;
        this.yStart = this.canvasHeight - this.plotHeight;
        this.XAxisPlotMinValue = 10000;    //10 Seconds
        this.YAxisSizeMultiplier = 2;    //YAxis is x times
        this.YAxisInitialPlotValue = "zero"; //"zero", "betSize" //TODO: ???
    },

    calcGameData: function () { //TODO: Use getGamePayout from engine.
        this.currentTime = Clib.getElapsedTimeWithLag(this.Engine);
        this.currentGamePayout = Clib.calcGamePayout(this.currentTime);
    },

    calculatePlotValues: function () {

        //Plot variables
        this.YAxisPlotMinValue = this.YAxisSizeMultiplier;
        this.YAxisPlotValue = this.YAxisPlotMinValue;

        this.XAxisPlotValue = this.XAxisPlotMinValue;

        //Adjust X Plot's Axis
        if (this.currentTime > this.XAxisPlotMinValue)
            this.XAxisPlotValue = this.currentTime;

        //Adjust Y Plot's Axis
        if (this.currentGamePayout > this.YAxisPlotMinValue)
            this.YAxisPlotValue = this.currentGamePayout;

        //We start counting from cero to plot
        this.YAxisPlotValue -= 1;

        //Graph values
        this.widthIncrement = this.plotWidth / this.XAxisPlotValue;
        this.heightIncrement = this.plotHeight / (this.YAxisPlotValue);
        this.currentX = this.currentTime * this.widthIncrement;
    },

    clean: function () {
        this.ctx.clearRect(0, 0, this.canvas.width, this.canvas.height);
    },

    drawGraph: function () {

        /* Style the line depending on the game states */
        this.ctx.strokeStyle = (this.themeWhite ? "Black" : "#b0b3c1");

        //Playing and not cashed out
        if (true) {
            this.ctx.lineWidth = 6;
            this.ctx.strokeStyle = '#7cba00';

            //Cashing out
        } else if (this.Engine.cashingOut) {
            this.ctx.lineWidth = 6;
            //this.ctx.strokeStyle = "Grey";

        } else {
            this.ctx.lineWidth = 4;
        }

        this.ctx.beginPath();
        Clib.seed(1);

        /* Draw the graph */
        for (var t = 0, i = 0; t <= this.currentTime; t += 100, i++) {

            /* Graph */
            var payout = Clib.calcGamePayout(t) - 1; //We start counting from one x
            var y = this.plotHeight - (payout * this.heightIncrement);
            var x = t * this.widthIncrement;
            this.ctx.lineTo(x + this.xStart, y);

            /* Draw green line if last game won */ //TODO: Avoid doing the code above this if it will do this
            /*var realPayout = Clib.payout(this.betSize, t);
             if(this.lastGameWon && (Clib.payout(this.betSize, t) > this.lastWinnings) && !greenSetted) {
             var tempStroke = this.ctx.strokeStyle;
             this.ctx.strokeStyle = '#7cba00';
             this.ctx.stroke();

             this.ctx.beginPath();
             this.ctx.lineWidth=3;
             this.ctx.moveTo(x + this.xStart, y);
             this.ctx.strokeStyle = tempStroke;
             greenSetted = true;
             }*/

            /* Avoid crashing the explorer if the cycle is infinite */
            if (i > 5000) { console.log("For 1 too long!"); break; }
        }
        this.ctx.stroke();
    },

    drawAxes: function () {

        //Function to calculate the plotting values of the Axes
        function stepValues(x) {
            var c = .4;
            var r = .1;
            while (true) {

                if (x < c) return r;

                c *= 5;
                r *= 2;

                if (x < c) return r;
                c *= 2;
                r *= 5;
            }
        }

        //Calculate Y Axis
        this.YAxisPlotMaxValue = this.YAxisPlotMinValue;
        this.payoutSeparation = stepValues(!this.currentGamePayout ? 1 : this.currentGamePayout);

        this.ctx.lineWidth = 1;
        this.ctx.strokeStyle = (this.themeWhite ? "Black" : "#b0b3c1");
        this.ctx.font = "10px Verdana";
        this.ctx.fillStyle = (this.themeWhite ? 'black' : "#b0b3c1");
        this.ctx.textAlign = "center";

        //Draw Y Axis Values
        var heightIncrement = this.plotHeight / (this.YAxisPlotValue);
        for (var payout = this.payoutSeparation, i = 0; payout < this.YAxisPlotValue; payout += this.payoutSeparation, i++) {
            var y = this.plotHeight - (payout * heightIncrement);
            this.ctx.fillText((payout + 1) + 'x', 10, y);

            this.ctx.beginPath();
            this.ctx.moveTo(this.xStart, y);
            this.ctx.lineTo(this.xStart + 5, y);
            this.ctx.stroke();

            if (i > 100) { console.log("For 3 too long"); break; }
        }

        //Calculate X Axis
        this.milisecondsSeparation = stepValues(this.XAxisPlotValue);
        this.XAxisValuesSeparation = this.plotWidth / (this.XAxisPlotValue / this.milisecondsSeparation);

        //Draw X Axis Values
        for (var miliseconds = 0, counter = 0, i = 0; miliseconds < this.XAxisPlotValue; miliseconds += this.milisecondsSeparation, counter++, i++) {
            var seconds = miliseconds / 1000;
            var textWidth = this.ctx.measureText(seconds).width;
            var x = (counter * this.XAxisValuesSeparation) + this.xStart;
            this.ctx.fillText(seconds, x - textWidth / 2, this.plotHeight + 11);

            if (i > 100) { console.log("For 4 too long"); break; }
        }

        //Draw background Axis
        this.ctx.lineWidth = 1;
        this.ctx.beginPath();
        this.ctx.moveTo(this.xStart, 0);
        this.ctx.lineTo(this.xStart, this.canvasHeight - this.yStart);
        this.ctx.lineTo(this.canvasWidth, this.canvasHeight - this.yStart);
        this.ctx.stroke();
    },


    drawGameData: function () {
        //One percent of canvas width
        var onePercent = this.canvasWidth / 100;
        //Multiply it x times
        function fontSizeNum(times) {
            return onePercent * times;
        }
        //Return the font size in pixels of one percent of the width canvas by x times
        function fontSizePx(times) {
            var fontSize = fontSizeNum(times);
            return fontSize.toFixed(2) + 'px';
        }

        this.ctx.textAlign = "center";
        this.ctx.textBaseline = 'middle';
        if (this.Engine.gameState === 'IN_PROGRESS') {
            if (StateLib.currentlyPlaying(this.Engine))
                this.ctx.fillStyle = '#7cba00';
            else
                this.ctx.fillStyle = (this.themeWhite ? "black" : "#b0b3c1");

            this.ctx.font = fontSizePx(20) + " Verdana";
            this.ctx.fillText(parseFloat(this.currentGamePayout).toFixed(2) + 'x', this.canvasWidth / 2, this.canvasHeight / 2);
        }

        //If the engine enters in the room @ ENDED it doesn't have the crash value, so we don't display it
        if (this.Engine.gameState === 'ENDED') {

            this.ctx.font = fontSizePx(15) + " Verdana";
            this.ctx.fillStyle = "red";
            this.ctx.fillText('Busted', this.canvasWidth / 2, this.canvasHeight / 2 - fontSizeNum(15) / 2);
            this.ctx.fillText('@ ' + Clib.formatDecimals(this.Engine.tableHistory[0].game_crash / 100, 2) + 'x', this.canvasWidth / 2, this.canvasHeight / 2 + fontSizeNum(15) / 2);
        }

        if (this.Engine.gameState === 'STARTING') {
            this.ctx.font = fontSizePx(5) + " Verdana";
            this.ctx.fillStyle = "grey";

            var timeLeft = ((this.Engine.startTime - Date.now()) / 1000).toFixed(1);

            this.ctx.fillText('Next round in ' + timeLeft + 's', this.canvasWidth / 2, this.canvasHeight / 2);
        }


        //if(this.lag) {
        //    this.ctx.fillStyle = "black";
        //    this.ctx.font="20px Verdana";
        //    this.ctx.fillText('Network Lag', 250, 250);
        //}
    },

    checkForLag: function () {

        this.Engine.lag = true;
    },
}
