﻿
var Hub = {
    connection: null,
    appCrashGame: angular.element($("#appCrashGame")).scope(),
    init2: function () {
        console.log(appCrashGame.Engine);

    },
    init: function () {
        connection = new signalR.HubConnectionBuilder()
            .withUrl("/hubs/game")
            .configureLogging(signalR.LogLevel.Information)
            .build();

        async function start() {
            try {
                await connection.start();
                console.log("SignalR Connected.");
            } catch (err) {
                console.log(err);
                setTimeout(start, 5000);
            }
        };

        connection.onclose(async () => {
            await start();
        });

        connection.on("game_starting", (data, message) => {

            ChartGraphics.Engine.playerInfo = {};
            ChartGraphics.Engine.joined = [];

            ChartGraphics.Engine.gameState = 'STARTING';
            ChartGraphics.Engine.gameId = data.game_id;
            ChartGraphics.Engine.timeTillStart = data.timeTillStart;
            const currentTime = new Date();
            ChartGraphics.Engine.startTime = new Date(currentTime.getTime() + data.timeTillStart);
            ChartGraphics.Engine.maxWin = data.max_win;

            // Every time the game starts checks if there is a queue bet and send it
            if (ChartGraphics.Engine.nextBetAmount) {
                this.doBet(ChartGraphics.Engine.nextBetAmount, ChartGraphics.Engine.nextAutoCashout, function (err) {
                    if (err)
                        console.log('Response from placing a bet: ', err);
                });
            }
            ChartGraphics.startRendering();
        });

        connection.on("game_started", (bets, message) => {
            ChartGraphics.Engine.joined = [];

            ChartGraphics.Engine.gameState = 'IN_PROGRESS';
            ChartGraphics.Engine.startTime = Date.now();
            ChartGraphics.Engine.lastGameTick = ChartGraphics.Engine.startTime;
            ChartGraphics.Engine.placingBet = false;
            ChartGraphics.Engine.timeTillStart = null;

            ChartGraphics.Engine.nextBetAmount = null;
            ChartGraphics.Engine.nextAutoCashout = null;

            //Create the player info object with bet and username
            //If you are in the bets rest your bet from your balance
            Object.keys(bets).forEach(function (username) {
                if (ChartGraphics.Engine.username === username)
                    ChartGraphics.Engine.balanceSatoshis -= bets[username];

                ChartGraphics.Engine.playerInfo[username] = { bet: bets[username], username: username };
            });

            // this.calcBonuses();

            ChartGraphics.startRendering();

        });
        connection.on('game_crash', function (data) {
            if (ChartGraphics.Engine.tickTimer)
                clearTimeout(ChartGraphics.Engine.tickTimer);

            //If the game crashed at zero x remove bonuses projections by setting them to zero.
            if (data.game_crash == 0)
                ChartGraphics.Engine.setBonusesToZero();

            //Update your balance if you won a bonus, use this one because its the bonus rounded by the server
            for (var user in data.bonuses) {
                console.assert(ChartGraphics.Engine.playerInfo[user]);
                ChartGraphics.Engine.playerInfo[user].bonus = data.bonuses[user]; //TODO: Deprecate sending bonuses to the client?
                if (ChartGraphics.Engine.username === user) {
                    ChartGraphics.Engine.balanceSatoshis += data.bonuses[user];
                }
            }

            ChartGraphics.Engine.lastHash = data.hash;

            var gameInfo = {
                created: ChartGraphics.Engine.created,
                ended: true,
                game_crash: data.gameCrash,
                game_id: ChartGraphics.Engine.gameId,
                hash: data.hash,
                player_info: ChartGraphics.Engine.playerInfo
            };

            //Add the current game info to the game history and if the game history is larger than 40 remove one element
            if (ChartGraphics.Engine.tableHistory.length >= 40)
                ChartGraphics.Engine.tableHistory.pop();
            ChartGraphics.Engine.tableHistory.unshift(gameInfo);

            //Clear current game properties
            ChartGraphics.Engine.gameState = 'ENDED';
            ChartGraphics.Engine.cashingOut = false;
            ChartGraphics.Engine.lag = false;

            //Clear Animation trigger flags
            ChartGraphics.Engine.nyan = false;

            ChartGraphics.startRendering();
        });

        connection.on("join", (result, message) => {
            const data = JSON.parse(result);
            ChartGraphics.Engine.balanceSatoshis = data.balance_satoshis;
            //self.chat = resp.chat;

            /** If username is a falsey value the user is not logged in */
            ChartGraphics.Engine.username = data.username;

            /** Variable to check if we are connected to the server */
            ChartGraphics.Engine.isConnected = true;
            ChartGraphics.Engine.gameState = data.gameState;
            ChartGraphics.Engine.playerInfo = data.player_info;

            // set current game properties
            ChartGraphics.Engine.gameId = data.game_id;
            ChartGraphics.Engine.maxWin = data.max_win;
            ChartGraphics.Engine.lastHash = data.last_hash;
            ChartGraphics.Engine.created = data.created;
            const currentTime = new Date();
            ChartGraphics.Engine.startTime = new Date(currentTime.getTime() - data.elapsed);
            ChartGraphics.Engine.joined = data.joined;
            ChartGraphics.Engine.tableHistory = data.table_history;
            if (ChartGraphics.Engine.gameState === 'IN_PROGRESS') {
                ChartGraphics.Engine.lastGameTick = Date.now();
            }

            //Attach username to each user for sorting proposes 
            for (var user in ChartGraphics.Engine.playerInfo) {
                ChartGraphics.Engine.playerInfo[user].username = user;
            }

            //Calculate the bonuses of the current game if necessary
            if (ChartGraphics.Engine.gameState === 'IN_PROGRESS' || data.gameState === 'ENDED') {
                // this.calcBonuses();
            }
            ChartGraphics.startRendering();

        });


        connection.on('disconnect', function (data) {
            ChartGraphics.Engine.isConnected = false;
        });

        connection.on("cashed_out", (data, message) => {
            //Add the cashout percentage of each user at cash out
            if (!ChartGraphics.Engine.playerInfo[data.username])
                return console.warn('Username not found in playerInfo at cashed_out: ', data.username);

            ChartGraphics.Engine.playerInfo[resp.username].stopped_at = data.stopped_at;

            if (ChartGraphics.Engine.username === data.username) {
                ChartGraphics.Engine.cashingOut = false;
                ChartGraphics.Engine.balanceSatoshis += ChartGraphics.Engine.playerInfo[resp.username].bet * data.stopped_at / 100;
            }
            ChartGraphics.Engine.calcBonuses();
        });
        connection.on("game_tick", (data, message) => {
            /** Time of the last tick received */
            ChartGraphics.Engine.lastGameTick = Date.now();
            if (ChartGraphics.Engine.lag === true) {
                ChartGraphics.Engine.lag = false;
            }

            /** Correct the time of startTime every gameTick **/
            var currentLatencyStartTime = ChartGraphics.Engine.lastGameTick - ChartGraphics.Engine.elapsed;
            if (ChartGraphics.Engine.startTime > currentLatencyStartTime)
                ChartGraphics.Engine.startTime = currentLatencyStartTime;

            if (ChartGraphics.Engine.tickTimer)
                clearTimeout(ChartGraphics.Engine.tickTimer);

            ChartGraphics.Engine.tickTimer = setTimeout(ChartGraphics.checkForLag(), Clib.STOP_PREDICTING_LAPSE);

            //Check for animation triggers
            if (ChartGraphics.Engine.elapsed > ChartGraphics.Engine.Animations.NYAN_CAT_TRIGGER_MS && !ChartGraphics.Engine.nyan) {
                ChartGraphics.Engine.nyan = true;
            }
        });


        connection.on('player_bet', function (data) {
            console.log('player_bet');

            if (ChartGraphics.Engine.username === data.username) {
                ChartGraphics.Engine.placingBet = false;
                ChartGraphics.Engine.nextBetAmount = null;
                ChartGraphics.Engine.nextAutoCashout = null;
            }
            ChartGraphics.Engine.joined.splice(data.index, 0, data.username);
            console.log(ChartGraphics.Engine);
        });



        connection.on('disconnect', function (data) {
            console.log('disconnect');

            ChartGraphics.Engine.isConnected = false;

            console.log('Client disconnected |', data, '|', typeof data);
            ChartGraphics.Engine.trigger('disconnected');
        });

        connection.on('msg', function (data) {

            console.log('msg');
            //The chat only renders if the Arr length is diff, remove blocks of the array
            const MAX_LENGTH = 500;
            if (ChartGraphics.Engine.chat.length > MAX_LENGTH)
                ChartGraphics.Engine.chat.splice(0, 400);

            // Match @username until end of string or invalid username char
            var r = new RegExp('@' + ChartGraphics.Engine.username + '(?:$|[^a-z0-9_\-])', 'i');
            if (data.type === 'say' && data.username !== ChartGraphics.Engine.username && r.test(data.message)) {
                new Audio('/sounds/gong.mp3').play();
            }
            ChartGraphics.Engine.chat.push(data);

            ChartGraphics.Engine.trigger('msg', data);
        });

        /** Triggered by the server to let users the have to reload the page */
        connection.on('update', function () {

            console.log('update');
            alert('Please refresh your browser! We just pushed a new update to the server!');
        });

        connection.on('error', function (x) {
            console.log('error');

            console.log('on error: ', x);
            self.trigger('error', x);
        });

        /** Server Errors */
        connection.on('err', function (err) {
            console.log('err');
            console.error('Server sent us the error: ', err);
        });

        // Start the connection.
        start();
    },

    doBet: function (amount, autoCashOut, callback) {
        try {
            console.log(amount, autoCashOut);
            connection.invoke('OnPlaceBetAsync', amount, autoCashOut);
        } catch (e) {
            console.log('doBet', e);

        }
    },

    cashed_out: function () {
        try {
            ChartGraphics.Engine.cashingOut = true;
            connection.emit('cash_out', function (error) {
                if (error) {
                    self.cashingOut = false;
                    console.warn('Cashing out error: ', error);
                    self.trigger('cashing_out_error'); //TODO: This is not listened anywhere, check if a component needs to listen to it.
                }
            });
        } catch (e) {
            console.log('cashed_out', e);
        }
    },

    calcBonuses: function () {
        //Slides across the array and apply the function to equally stopped_at parts of the array
        function slideSameStoppedAt(arr, fn) {
            var i = 0;
            while (i < arr.length) {
                var tmp = [];
                var betAmount = 0;
                var sa = arr[i].stopped_at;
                for (; i < arr.length && arr[i].stopped_at === sa; ++i) {
                    betAmount += arr[i].bet;
                    tmp.push(i);
                }
                fn(arr, tmp, sa, betAmount);
            }
        }

        //Transform the player info object in an array of references to the user objects
        //{ user1: { bet: satoshis, stopped_at: 200 }, user2: { bet: satoshis } } -> [ user1: { bet: satoshis, stopped_at: 200 } ... ]
        var playersArr = _.map(ChartGraphics.Engine.playerInfo, function (player, username) {
            return player;
        });

        //Sort the list of players based on the cashed position, the rest doesn't matter because the losers get the same ratio of bonus
        var playersArrSorted = _.sortBy(playersArr, function (player) {
            return player.stopped_at ? -player.stopped_at : null;
        });

        var bonusPool = 0;
        var largestBet = 0;

        //Get max bet and bonus pool
        for (var i = 0, length = playersArrSorted.length; i < length; ++i) {
            var bet = playersArrSorted[i].bet;
            bonusPool += bet / 100;
            largestBet = Math.max(largestBet, bet);
        }

        //The ratio bits per bit bet
        var maxWinRatio = bonusPool / largestBet;

        slideSameStoppedAt(playersArrSorted,
            function (array, listOfRecordsPositions, cashOutAmount, totalBetAmount) {

                //If the bonus pool is empty fill the bonus with 0's
                if (bonusPool <= 0) {
                    for (var i = 0, length = listOfRecordsPositions.length; i < length; i++) {
                        array[listOfRecordsPositions[i]].bonus = 0;
                    }
                    return;
                }

                //If the bonusPool is less than what this user/group could get just give the remaining of the bonus pool
                var toAllocAll = Math.min(totalBetAmount * maxWinRatio, bonusPool);

                //Alloc the bonuses
                for (var i = 0, length = listOfRecordsPositions.length; i < length; i++) {

                    //The alloc qty of this user, if its one it will get all
                    var toAlloc = (array[listOfRecordsPositions[i]].bet / totalBetAmount) * toAllocAll;

                    bonusPool -= toAlloc;

                    array[listOfRecordsPositions[i]].bonus = toAlloc;
                }
            }
        );
    },
}