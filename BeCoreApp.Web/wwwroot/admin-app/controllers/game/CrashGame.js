﻿var tooltipSlider = document.querySelector("#kt_slider_tooltip");

noUiSlider.create(tooltipSlider, {
    start: [0, 80, 200],
    tooltips: [false, wNumb({ decimals: 1 }), true],
    range: {
        "min": 0,
        "max": 200
    }
});

var ctx = document.getElementById('kt_chartjs_3');

$(document).ready(function () {
    var appCrashGame = angular.element($("#appCrashGame")).scope();
});


var appCrashGame = angular.module("appCrashGame", []);

appCrashGame.factory("dataService", ["$http", "$q", function ($http, $q) {
    var _isInit = false;

    var _isReady = function () {
        return _isInit;
    };
    return {
        isReady: _isReady
    };
}]);

appCrashGame.controller("ctrlCrashGame", ["$scope", "$http", "dataService", "$location", "$window", "$timeout", "$compile", "$filter", function ($scope, $http, dataService, $location, $window, $timeout, $compile, $filter) {

    var window = angular.element($window);
    $scope.loadWaiting = false;
    $scope.usepopupnotifications = false;
    $scope.topcartselector = '';
    $scope.topwishlistselector = '';
    $scope.flyoutcartselector = '';
    $scope.getParentNodeFunc = '';
    $scope.rendering = false;
    $scope.connection = null;
    $scope.onePercent = 0;
    $scope.Engine = {
        lastGameTick: 1635780423652,
        Animations: {
            NYAN_CAT_TRIGGER_MS: 200 //115129ms ~ 1000x // 11552ms ~ 2x
        },
        isConnected: false,
        username: null,
        balanceSatoshis: null,
        maxBet: 100000000,
        //chat: [];
        tableHistory: [],
        joined: [],
        /** Object containing the current game players and their status, this is saved in game history every game crash
         * cleared in game_starting.
         * e.g: { user1: { bet: satoshis, stopped_at: 200 }, user2: { bet: satoshis } }
         */
        playerInfo: [],
        /**
         * The state of the game
         * Possible states: IN_PROGRESS, ENDED, STARTING
         */
        gameState: null,
        created: null,
        gameId: null,
        maxWin: null,
        startTime: null,
        timeTillStart: null,
        placingBet: false,
        cashingOut: false,
        nextBetAmount: null,
        nextAutoCashout: null,
        tickTimer: null,
        lag: false,
        lastHash: null,
        nyan: false,
    };
    $scope.placeBet_Amount = 0;
    $scope.placeBet_CashOut = 0;
    $scope.initPage = function () {
        $scope.initSocket();
    };
    $scope.requestAnimationFrame = function (callback) {
        setTimeout(callback, 0);
    };
    $scope.initSocket = function () {
        $scope.connection = new signalR.HubConnectionBuilder()
            .withUrl("/hubs/game")
            .configureLogging(signalR.LogLevel.Information)
            .build();

        async function start() {
            try {
                await $scope.connection.start();
                console.log("SignalR Connected.");
            } catch (err) {
                console.log(err);
                setTimeout(start, 5000);
            }
        };

        $scope.connection.onclose(async () => {
            await start();
        });

        $scope.connection.on("game_starting", (data, message) => {

            $scope.Engine.playerInfo = {};
            $scope.Engine.joined = [];

            $scope.Engine.gameState = 'STARTING';
            $scope.Engine.gameId = data.game_id;
            $scope.Engine.timeTillStart = data.timeTillStart;
            const currentTime = new Date();
            $scope.Engine.startTime = new Date(currentTime.getTime() + data.timeTillStart);
            $scope.Engine.maxWin = data.max_win;

            // Every time the game starts checks if there is a queue bet and send it
            if ($scope.Engine.nextBetAmount) {
                this.doBet($scope.Engine.nextBetAmount, $scope.Engine.nextAutoCashout, function (err) {
                    if (err)
                        console.log('Response from placing a bet: ', err);
                });
            }
            $scope.startRendering();
        });

        $scope.connection.on("game_started", (bets, message) => {
            console.log('game_started ', bets);
            $scope.Engine.joined = [];

            $scope.Engine.gameState = 'IN_PROGRESS';
            $scope.Engine.startTime = Date.now();
            $scope.Engine.lastGameTick = $scope.Engine.startTime;
            $scope.Engine.placingBet = false;
            $scope.Engine.timeTillStart = null;

            $scope.Engine.nextBetAmount = null;
            $scope.Engine.nextAutoCashout = null;

            //Create the player info object with bet and username
            //If you are in the bets rest your bet from your balance
            Object.keys(bets).forEach(function (username) {
                if ($scope.Engine.username === username)
                    $scope.Engine.balanceSatoshis -= bets[username];

                $scope.Engine.playerInfo[username] = { bet: bets[username], username: username };
            });

            $scope.calcBonuses();

            $scope.startRendering();

        });
        $scope.connection.on('game_crash', function (data) {
            if ($scope.Engine.tickTimer)
                clearTimeout($scope.Engine.tickTimer);

            //If the game crashed at zero x remove bonuses projections by setting them to zero.
            if (data.game_crash == 0)
                $scope.Engine.setBonusesToZero();

            //Update your balance if you won a bonus, use this one because its the bonus rounded by the server
            for (var user in data.bonuses) {
                console.assert($scope.Engine.playerInfo[user]);
                $scope.Engine.playerInfo[user].bonus = data.bonuses[user]; //TODO: Deprecate sending bonuses to the client?
                if ($scope.Engine.username === user) {
                    $scope.Engine.balanceSatoshis += data.bonuses[user];
                }
            }

            $scope.Engine.lastHash = data.hash;

            var gameInfo = {
                created: $scope.Engine.created,
                ended: true,
                game_crash: data.gameCrash,
                game_id: $scope.Engine.gameId,
                hash: data.hash,
                player_info: $scope.Engine.playerInfo
            };

            //Add the current game info to the game history and if the game history is larger than 40 remove one element
            if ($scope.Engine.tableHistory.length >= 40)
                $scope.Engine.tableHistory.pop();
            $scope.Engine.tableHistory.unshift(gameInfo);

            //Clear current game properties
            $scope.Engine.gameState = 'ENDED';
            $scope.Engine.cashingOut = false;
            $scope.Engine.lag = false;

            //Clear Animation trigger flags
            $scope.Engine.nyan = false;

            $scope.startRendering();
        });

        $scope.connection.on("join", (result, message) => {
            const data = JSON.parse(result);
            $scope.Engine.balanceSatoshis = data.balance_satoshis;
            //self.chat = resp.chat;

            /** If username is a falsey value the user is not logged in */
            $scope.Engine.username = data.username;

            /** Variable to check if we are connected to the server */
            $scope.Engine.isConnected = true;
            $scope.Engine.gameState = data.gameState;
            $scope.Engine.playerInfo = data.player_info;

            // set current game properties
            $scope.Engine.gameId = data.game_id;
            $scope.Engine.maxWin = data.max_win;
            $scope.Engine.lastHash = data.last_hash;
            $scope.Engine.created = data.created;
            const currentTime = new Date();
            $scope.Engine.startTime = new Date(currentTime.getTime() - data.elapsed);
            $scope.Engine.joined = data.joined;
            $scope.Engine.tableHistory = data.table_history;
            if ($scope.Engine.gameState === 'IN_PROGRESS') {
                $scope.Engine.lastGameTick = Date.now();
            }

            //Attach username to each user for sorting proposes 
            for (var user in $scope.Engine.playerInfo) {
                $scope.Engine.playerInfo[user].username = user;
            }

            //Calculate the bonuses of the current game if necessary
            if ($scope.Engine.gameState === 'IN_PROGRESS' || data.gameState === 'ENDED') {
                // this.calcBonuses();
            }
            $scope.startRendering();

        });


        $scope.connection.on('disconnect', function (data) {
            $scope.Engine.isConnected = false;
        });

        $scope.connection.on("cashed_out", (data, message) => {
            //Add the cashout percentage of each user at cash out
            if (!$scope.Engine.playerInfo[data.username])
                return console.warn('Username not found in playerInfo at cashed_out: ', data.username);

            $scope.Engine.playerInfo[resp.username].stopped_at = data.stopped_at;

            if ($scope.Engine.username === data.username) {
                $scope.Engine.cashingOut = false;
                $scope.Engine.balanceSatoshis += $scope.Engine.playerInfo[resp.username].bet * data.stopped_at / 100;
            }
            $scope.calcBonuses();
        });
        $scope.connection.on("game_tick", (data, message) => {
            /** Time of the last tick received */
            $scope.Engine.lastGameTick = Date.now();
            if ($scope.Engine.lag === true) {
                $scope.Engine.lag = false;
            }

            /** Correct the time of startTime every gameTick **/
            var currentLatencyStartTime = $scope.Engine.lastGameTick - $scope.Engine.elapsed;
            if ($scope.Engine.startTime > currentLatencyStartTime)
                $scope.Engine.startTime = currentLatencyStartTime;

            if ($scope.Engine.tickTimer)
                clearTimeout($scope.Engine.tickTimer);

            $scope.Engine.tickTimer = setTimeout($scope.checkForLag(), Clib.STOP_PREDICTING_LAPSE);

            //Check for animation triggers
            if ($scope.Engine.elapsed > $scope.Engine.Animations.NYAN_CAT_TRIGGER_MS && !$scope.Engine.nyan) {
                $scope.Engine.nyan = true;
            }
        });


        $scope.connection.on('player_bet', function (data) {
            console.log('player_bet', data);

            if ($scope.Engine.username === data.username) {
                $scope.Engine.placingBet = false;
                $scope.Engine.nextBetAmount = null;
                $scope.Engine.nextAutoCashout = null;
            }
            $scope.Engine.joined.push({
                index: data.index,
                userName: data.userName,
                cashOut: 0,
            });
            console.log($scope.Engine.joined);
        });



        $scope.connection.on('disconnect', function (data) {
            console.log('disconnect');

            $scope.Engine.isConnected = false;

            console.log('Client disconnected |', data, '|', typeof data);
            $scope.Engine.trigger('disconnected');
        });

        $scope.connection.on('msg', function (data) {

            console.log('msg');
            //The chat only renders if the Arr length is diff, remove blocks of the array
            const MAX_LENGTH = 500;
            if ($scope.Engine.chat.length > MAX_LENGTH)
                $scope.Engine.chat.splice(0, 400);

            // Match @username until end of string or invalid username char
            var r = new RegExp('@' + $scope.Engine.username + '(?:$|[^a-z0-9_\-])', 'i');
            if (data.type === 'say' && data.username !== $scope.Engine.username && r.test(data.message)) {
                new Audio('/sounds/gong.mp3').play();
            }
            $scope.Engine.chat.push(data);
        });

        /** Triggered by the server to let users the have to reload the page */
        $scope.connection.on('update', function () {

            console.log('update');
            alert('Please refresh your browser! We just pushed a new update to the server!');
        });

        $scope.connection.on('error', function (x) {
            console.log('error');

            console.log('on error: ', x);
        });

        /** Server Errors */
        $scope.connection.on('err', function (err) {
            console.log('err');
            console.error('Server sent us the error: ', err);
        });

        // Start the connection.
        start();
    };
    $scope.doBet = function (amount, autoCashOut, callback) {
        try {
            console.log(amount, autoCashOut);
            $scope.connection.invoke('OnPlaceBetAsync', amount, autoCashOut);
        } catch (e) {
            console.log('doBet', e);
        }
    };

    $scope.cashed_out = function () {
        try {
            $scope.Engine.cashingOut = true;
            $scope.connection.emit('cash_out', function (error) {
                if (error) {
                    self.cashingOut = false;
                    console.warn('Cashing out error: ', error);
                    self.trigger('cashing_out_error'); //TODO: This is not listened anywhere, check if a component needs to listen to it.
                }
            });
        } catch (e) {
            console.log('cashed_out', e);
        }
    };

    $scope.calcBonuses = function () {
        //Slides across the array and apply the function to equally stopped_at parts of the array
        function slideSameStoppedAt(arr, fn) {
            var i = 0;
            while (i < arr.length) {
                var tmp = [];
                var betAmount = 0;
                var sa = arr[i].stopped_at;
                for (; i < arr.length && arr[i].stopped_at === sa; ++i) {
                    betAmount += arr[i].bet;
                    tmp.push(i);
                }
                fn(arr, tmp, sa, betAmount);
            }
        }

        //Transform the player info object in an array of references to the user objects
        //{ user1: { bet: satoshis, stopped_at: 200 }, user2: { bet: satoshis } } -> [ user1: { bet: satoshis, stopped_at: 200 } ... ]
        var playersArr = _.map($scope.Engine.playerInfo, function (player, username) {
            return player;
        });

        //Sort the list of players based on the cashed position, the rest doesn't matter because the losers get the same ratio of bonus
        var playersArrSorted = _.sortBy(playersArr, function (player) {
            return player.stopped_at ? -player.stopped_at : null;
        });

        var bonusPool = 0;
        var largestBet = 0;

        //Get max bet and bonus pool
        for (var i = 0, length = playersArrSorted.length; i < length; ++i) {
            var bet = playersArrSorted[i].bet;
            bonusPool += bet / 100;
            largestBet = Math.max(largestBet, bet);
        }

        //The ratio bits per bit bet
        var maxWinRatio = bonusPool / largestBet;

        slideSameStoppedAt(playersArrSorted,
            function (array, listOfRecordsPositions, cashOutAmount, totalBetAmount) {

                //If the bonus pool is empty fill the bonus with 0's
                if (bonusPool <= 0) {
                    for (var i = 0, length = listOfRecordsPositions.length; i < length; i++) {
                        array[listOfRecordsPositions[i]].bonus = 0;
                    }
                    return;
                }

                //If the bonusPool is less than what this user/group could get just give the remaining of the bonus pool
                var toAllocAll = Math.min(totalBetAmount * maxWinRatio, bonusPool);

                //Alloc the bonuses
                for (var i = 0, length = listOfRecordsPositions.length; i < length; i++) {

                    //The alloc qty of this user, if its one it will get all
                    var toAlloc = (array[listOfRecordsPositions[i]].bet / totalBetAmount) * toAllocAll;

                    bonusPool -= toAlloc;

                    array[listOfRecordsPositions[i]].bonus = toAlloc;
                }
            }
        );
    };

    $scope.startRendering = function () {
        if ($scope.rendering === false) {
            $scope.rendering = true;
            const canvasNode = document.getElementById('canvas');
            if (!canvasNode.getContext)
                return console.error('No canvas');

            $scope.ctx = canvasNode.getContext('2d');
            var parentNode = document.getElementById('chart-inner-container');
            $scope.canvasWidth = parentNode.clientWidth;
            $scope.canvasHeight = parentNode.clientHeight;
            $scope.canvas = canvasNode;
            $scope.configPlotSettings();
            $scope.animRequest = $scope.requestAnimationFrame($scope.render());

            window.addEventListener('resize', $scope.onWindowResizeBinded);
        }
    };
    $scope.stopRendering = function () {
        $scope.rendering = false;
        window.removeEventListener('resize', $scope.onWindowResizeBinded);
    };

    $scope.onChange = function () {
        $scope.configPlotSettings();
    };
    $scope.render = function () {
        if (!$scope.rendering)
            return;
        $scope.calcGameData();
        $scope.calculatePlotValues();
        $scope.clean();
        $scope.drawGraph();
        $scope.drawAxes();
        $scope.drawGameData();
        $scope.animRequest = $scope.requestAnimationFrame($scope.render.bind(this));
    };

    /** On windows resize adjust the canvas size to the canvas parent size */
    $scope.onWindowResize = function () {
        var parentNode = document.getElementById('chart-inner-container');
        $scope.canvasWidth = parentNode.clientWidth;
        $scope.canvasHeight = parentNode.clientHeight;
        $scope.configPlotSettings();
    };

    $scope.configPlotSettings = function () {
        $scope.canvas.width = $scope.canvasWidth;
        $scope.canvas.height = $scope.canvasHeight;
        $scope.themeWhite = ($scope.theme === 'white');
        $scope.plotWidth = $scope.canvasWidth - 30;
        $scope.plotHeight = $scope.canvasHeight - 20; //280
        $scope.xStart = $scope.canvasWidth - $scope.plotWidth;
        $scope.yStart = $scope.canvasHeight - $scope.plotHeight;
        $scope.XAxisPlotMinValue = 10000;    //10 Seconds
        $scope.YAxisSizeMultiplier = 2;    //YAxis is x times
        $scope.YAxisInitialPlotValue = "zero"; //"zero", "betSize" //TODO: ???
    };
    $scope.calcGameData = function () { //TODO: Use getGamePayout from engine.
        $scope.currentTime = Clib.getElapsedTimeWithLag($scope.Engine);
        $scope.currentGamePayout = Clib.calcGamePayout($scope.currentTime);
    };
    $scope.calculatePlotValues = function () {

        //Plot variables
        $scope.YAxisPlotMinValue = $scope.YAxisSizeMultiplier;
        $scope.YAxisPlotValue = $scope.YAxisPlotMinValue;

        $scope.XAxisPlotValue = $scope.XAxisPlotMinValue;

        //Adjust X Plot's Axis
        if ($scope.currentTime > $scope.XAxisPlotMinValue)
            $scope.XAxisPlotValue = $scope.currentTime;

        //Adjust Y Plot's Axis
        if ($scope.currentGamePayout > $scope.YAxisPlotMinValue)
            $scope.YAxisPlotValue = $scope.currentGamePayout;

        //We start counting from cero to plot
        $scope.YAxisPlotValue -= 1;

        //Graph values
        $scope.widthIncrement = $scope.plotWidth / $scope.XAxisPlotValue;
        $scope.heightIncrement = $scope.plotHeight / ($scope.YAxisPlotValue);
        $scope.currentX = $scope.currentTime * $scope.widthIncrement;
    };

    $scope.clean = function () {
        $scope.ctx.clearRect(0, 0, $scope.canvas.width, $scope.canvas.height);
    };
    $scope.drawGraph = function () {

        /* Style the line depending on the game states */
        $scope.ctx.strokeStyle = ($scope.themeWhite ? "Black" : "#b0b3c1");

        //Playing and not cashed out
        if (true) {
            $scope.ctx.lineWidth = 6;
            $scope.ctx.strokeStyle = '#7cba00';

            //Cashing out
        } else if ($scope.Engine.cashingOut) {
            $scope.ctx.lineWidth = 6;
            //$scope.ctx.strokeStyle = "Grey";

        } else {
            $scope.ctx.lineWidth = 4;
        }

        $scope.ctx.beginPath();
        Clib.seed(1);

        /* Draw the graph */
        for (var t = 0, i = 0; t <= $scope.currentTime; t += 100, i++) {

            /* Graph */
            var payout = Clib.calcGamePayout(t) - 1; //We start counting from one x
            var y = $scope.plotHeight - (payout * $scope.heightIncrement);
            var x = t * $scope.widthIncrement;
            $scope.ctx.lineTo(x + $scope.xStart, y);

            /* Draw green line if last game won */ //TODO: Avoid doing the code above this if it will do this
            /*var realPayout = Clib.payout($scope.betSize, t);
             if($scope.lastGameWon && (Clib.payout($scope.betSize, t) > $scope.lastWinnings) && !greenSetted) {
             var tempStroke = $scope.ctx.strokeStyle;
             $scope.ctx.strokeStyle = '#7cba00';
             $scope.ctx.stroke();

             $scope.ctx.beginPath();
             $scope.ctx.lineWidth=3;
             $scope.ctx.moveTo(x + $scope.xStart, y);
             $scope.ctx.strokeStyle = tempStroke;
             greenSetted = true;
             }*/

            /* Avoid crashing the explorer if the cycle is infinite */
            if (i > 5000) { console.log("For 1 too long!"); break; }
        }
        $scope.ctx.stroke();
    };

    $scope.fontSizeNum = function (times) {
        return $scope.onePercent * times;
    };
    //Return the font size in pixels of one percent of the width canvas by x times
    $scope.fontSizePx = function (times) {
        var fontSize = $scope.fontSizeNum(times);
        return fontSize.toFixed(2) + 'px';
    };
    $scope.drawAxes = function () {

        //Function to calculate the plotting values of the Axes
        function stepValues(x) {
            var c = .4;
            var r = .1;
            while (true) {

                if (x < c) return r;

                c *= 5;
                r *= 2;

                if (x < c) return r;
                c *= 2;
                r *= 5;
            }
        }

        //Calculate Y Axis
        $scope.YAxisPlotMaxValue = $scope.YAxisPlotMinValue;
        $scope.payoutSeparation = stepValues(!$scope.currentGamePayout ? 1 : $scope.currentGamePayout);

        $scope.ctx.lineWidth = 1;
        $scope.ctx.strokeStyle = ($scope.themeWhite ? "Black" : "#b0b3c1");
        $scope.ctx.font = "10px Verdana";
        $scope.ctx.fillStyle = ($scope.themeWhite ? 'black' : "#b0b3c1");
        $scope.ctx.textAlign = "center";

        //Draw Y Axis Values
        var heightIncrement = $scope.plotHeight / ($scope.YAxisPlotValue);
        for (var payout = $scope.payoutSeparation, i = 0; payout < $scope.YAxisPlotValue; payout += $scope.payoutSeparation, i++) {
            var y = $scope.plotHeight - (payout * heightIncrement);
            $scope.ctx.fillText((payout + 1) + 'x', 10, y);

            $scope.ctx.beginPath();
            $scope.ctx.moveTo($scope.xStart, y);
            $scope.ctx.lineTo($scope.xStart + 5, y);
            $scope.ctx.stroke();

            if (i > 100) { console.log("For 3 too long"); break; }
        }

        //Calculate X Axis
        $scope.milisecondsSeparation = stepValues($scope.XAxisPlotValue);
        $scope.XAxisValuesSeparation = $scope.plotWidth / ($scope.XAxisPlotValue / $scope.milisecondsSeparation);

        //Draw X Axis Values
        for (var miliseconds = 0, counter = 0, i = 0; miliseconds < $scope.XAxisPlotValue; miliseconds += $scope.milisecondsSeparation, counter++, i++) {
            var seconds = miliseconds / 1000;
            var textWidth = $scope.ctx.measureText(seconds).width;
            var x = (counter * $scope.XAxisValuesSeparation) + $scope.xStart;
            $scope.ctx.fillText(seconds, x - textWidth / 2, $scope.plotHeight + 11);

            if (i > 100) { console.log("For 4 too long"); break; }
        }

        //Draw background Axis
        $scope.ctx.lineWidth = 1;
        $scope.ctx.beginPath();
        $scope.ctx.moveTo($scope.xStart, 0);
        $scope.ctx.lineTo($scope.xStart, $scope.canvasHeight - $scope.yStart);
        $scope.ctx.lineTo($scope.canvasWidth, $scope.canvasHeight - $scope.yStart);
        $scope.ctx.stroke();
    };

    //Return the font size in pixels of one percent of the width canvas by x times
    $scope.fontSizePx = function (times) {
        var fontSize = $scope.fontSizeNum(times);
        return fontSize.toFixed(2) + 'px';
    }
    $scope.drawGameData = function () {
        //One percent of canvas width
        $scope.onePercent = $scope.canvasWidth / 100;
        //Multiply it x times


        $scope.ctx.textAlign = "center";
        $scope.ctx.textBaseline = 'middle';
        if ($scope.Engine.gameState === 'IN_PROGRESS') {
            if (StateLib.currentlyPlaying($scope.Engine))
                $scope.ctx.fillStyle = '#7cba00';
            else
                $scope.ctx.fillStyle = ($scope.themeWhite ? "black" : "#b0b3c1");

            $scope.ctx.font = $scope.fontSizePx(20) + " Verdana";
            $scope.ctx.fillText(parseFloat($scope.currentGamePayout).toFixed(2) + 'x', $scope.canvasWidth / 2, $scope.canvasHeight / 2);
        }

        //If the engine enters in the room @ ENDED it doesn't have the crash value, so we don't display it
        if ($scope.Engine.gameState === 'ENDED') {

            $scope.ctx.font = $scope.fontSizePx(15) + " Verdana";
            $scope.ctx.fillStyle = "red";
            $scope.ctx.fillText('Busted', $scope.canvasWidth / 2, $scope.canvasHeight / 2 - $scope.fontSizeNum(15) / 2);
            $scope.ctx.fillText('@ ' + Clib.formatDecimals($scope.Engine.tableHistory[0].game_crash / 100, 2) + 'x', $scope.canvasWidth / 2, $scope.canvasHeight / 2 + $scope.fontSizeNum(15) / 2);
        }

        if ($scope.Engine.gameState === 'STARTING') {
            $scope.ctx.font = $scope.fontSizePx(5) + " Verdana";
            $scope.ctx.fillStyle = "grey";

            var timeLeft = (($scope.Engine.startTime - Date.now()) / 1000).toFixed(1);

            $scope.ctx.fillText('Next round in ' + timeLeft + 's', $scope.canvasWidth / 2, $scope.canvasHeight / 2);
        }


        //if($scope.lag) {
        //    $scope.ctx.fillStyle = "black";
        //    $scope.ctx.font="20px Verdana";
        //    $scope.ctx.fillText('Network Lag', 250, 250);
        //}
    };

    $scope.checkForLag = function () {
        $scope.Engine.lag = true;
    };
    $scope.place_bet = function () {
        $scope.doBet($scope.placeBet_Amount, $scope.placeBet_CashOut * 100);
    };
}]);
