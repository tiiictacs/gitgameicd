﻿var MetaMaskHelper = {

    Web3Instant: function fn() {
        return new Web3(Web3.givenProvider)
    }(),

    //========Mainnet=========
    //ChainId: 56,
    //Chain: '0x38',  //'0x61' Testnet chain //'0x38' Mainet chain
    //ChainName: 'BNB',
    //CurrencyName: 'BNB',
    //RpcUrls: 'https://bsc-dataseed.binance.org',//https://bsc-dataseed1.binance.org:443 Mainnet //https://data-seed-prebsc-1-s1.binance.org:8545 Testnet // https://bsc-dataseed.binance.org/ new Mainnet

    //TokenSymbol: 'ICD',
    //TokenAddress: '0x0667427e71d38bB32f4E1226c41AA42ac50953C7',//0x0667427e71d38bB32f4E1226c41AA42ac50953C7 mainnet //0x217bd5d5cebb548e32ed3abe7a5b9efaa107c876 TestNet

    //========Testnet=========
    ChainId: 97,
    Chain: '0x61',
    ChainName: 'BNB',
    CurrencyName: 'BNB',
    RpcUrls: 'https://data-seed-prebsc-1-s1.binance.org:8545',

    TokenSymbol: 'ICD',
    TokenAddress: '0x217bd5d5cebb548e32ed3abe7a5b9efaa107c876',

    initialize: function () {
        if (!window.Web3Instant) {
            window.Web3Instant = new Web3(Web3.givenProvider);
        }

        if (!(ethereum && ethereum.isMetaMask)) {
            //show popup with link to metamask extension install
            window.location.replace('https://metamask.io/');
        }

        if (!(ethereum && ethereum.isTrust)) {
            //window.location.replace('https://metamask.io/');
        }
    },

    CheckNetWork: async function fn() {
        try {
            let currentChain = await MetaMaskHelper.Web3Instant.eth.getChainId();

            if (currentChain === MetaMaskHelper.ChainId) {
                return true;
            }

            be.error('DAPP Notification', 'Wrong network, please chain your network to BSC. Current Chain: ' + currentChain)
            return false
        } catch (e) {
            be.error('DAPP Notification', e.message)
            return false
        }
    },

    AnyConnectedAccounts: async function fn() {
        try {
            var accounts = await MetaMaskHelper.Web3Instant.eth.getAccounts();

            return accounts && accounts.length > 0;
        } catch (e) {
            be.error('DAPP Notification', e.message);

            return false;
        }
    },

    SwitchChain: async function fn() {
        if (ethereum.isTrust)
            return true

        try {
            await ethereum.request({
                method: 'wallet_switchEthereumChain',
                params: [{ chainId: MetaMaskHelper.Chain }],
            });

            return true;
        } catch (switchError) {
            // This error code indicates that the chain has not been added to MetaMask.
            if (switchError.code === 4902) {
                try {
                    await ethereum.request({
                        method: 'wallet_addEthereumChain',
                        params: [{
                            chainId: MetaMaskHelper.Chain,
                            chainName: MetaMaskHelper.ChainName,
                            rpcUrls: [MetaMaskHelper.RpcUrls],
                            nativeCurrency: {
                                name: MetaMaskHelper.CurrencyName,
                                symbol: MetaMaskHelper.CurrencyName,
                                decimals: 18
                            },
                        }],
                    });
                } catch (addError) {
                    // handle "add" error
                    be.error('DAPP Notification', addError.message)
                    return false
                }
            }

            be.error('DAPP Notification', switchError.message)
            return false
        }
    },

    RegisterEthereumEvents: function fn(accountChangeHandler) {

        ethereum.on('accountsChanged', accountChangeHandler);
    },

    AddICDAsset: async function fn() {
        const tokenDecimals = 18;
        const tokenImage = 'https://bscscan.com/token/images/icdefi_32.png';

        try {
            // wasAdded is a boolean. Like any RPC method, an error may be thrown.
            const wasAdded = await ethereum.request({
                method: 'wallet_watchAsset',
                params: {
                    type: 'ERC20', // Initially only supports ERC20, but eventually more!
                    options: {
                        address: MetaMaskHelper.TokenAddress, // The address that the token is at.
                        symbol: MetaMaskHelper.TokenSymbol, // A ticker symbol or shorthand, up to 5 chars.
                        decimals: tokenDecimals, // The number of decimals in the token
                        image: tokenImage, // A string url of the token logo
                    },
                },
            });

            //if (wasAdded) {
            //    be.success('ICD Notification', 'Added asset successful!')
            //} else {
            //    be.error('MetaMask Notification', 'Failed to add asset')
            //}
        } catch (error) {
            /*be.error('MetaMask Notification', error.message)*/
        }
    },

    HasInstalledMetaMask: function fn() {
        try {
            return window.ethereum && window.ethereum.isMetaMask
        } catch (e) {
            return false;
        }
    },

    HasInstalledTrust: function fn() {
        try {
            return window.ethereum && window.ethereum.isTrust
        } catch (e) {
            return false;
        }
    },

    MetaMask: function () {
        this.currentAccount = {}

        this.init = async function fn() {

            registerEvents()

            if (!window.ethereum) {
                ShowConnectButton()
                $('.metamask_add-asset').hide()
                return
            }

            await MetaMaskHelper.CheckNetWork();

            if (window.ethereum.isTrust)
                $('.metamask_add-asset').hide()


            MetaMaskHelper.RegisterEthereumEvents(handleAccountsChanged)

            await MetaMaskHelper.SwitchChain();

            var hasAccounts = await MetaMaskHelper.AnyConnectedAccounts()
            if (hasAccounts) {
                await handleRequestAccounts()
            }

            if ($('.metamask_address').val()) {
                ShowSendButton()
            }
            else {
                ShowConnectButton()
            }
        }

        function registerEvents() {

            $('.numberFormat').on("keypress", function (e) {
                var keyCode = e.which ? e.which : e.keyCode;
                var ret = ((keyCode >= 48 && keyCode <= 57) || keyCode == 46);
                if (ret)
                    return true;
                else
                    return false;
            });

            $(".numberFormat").focusout(function () {
                var numberValue = parseFloat($(this).val().replace(/,/g, ''));
                if (!numberValue) {
                    $(this).val(be.formatCurrency(0));
                }
                else {
                    $(this).val(be.formatCurrency(numberValue));
                }
            });

            $('.metamask_add-asset').on('click', () => {
                MetaMaskHelper.AddICDAsset()
            });

            $('.metamask_amount').on('focusout', async function () {
                be.startLoading()

                let value = $('.metamask_amount').val();

                var response = await GetAsync('/MetaMask/GetAmountICDPerBNB/' + value)
                    .catch(error => {
                        be.error('Invalid amount BNB')
                        be.stopLoading()
                    })

                if (response) {

                    $('.metamask_icd-amount').val(be.formatCurrency(response.Data))
                }

                be.stopLoading()
            })

            $('#wallet-connect-metamask').on('click', async function (e) {
                e.preventDefault()
                await ConnectMetaMask();

                $('#connect-wallet-modal').modal('hide');
            });

            $('#wallet-connect-trust').on('click', async function (e) {
                e.preventDefault()
                await ConnectTrustWallet();

                $('#connect-wallet-modal').modal('hide');
            });
        }

        async function confirmProgressTransaction() {

            //switch net
            var isSwitchSucess = await MetaMaskHelper.SwitchChain()

            if (!isSwitchSucess) {
                return;
            }

            //process buy icd
            be.startLoading();

            InitializeTransactionMetaMask().then(async res => {

                if (!res.Success) {
                    be.stopLoading()

                    if (!res.Message) {
                        be.error('ICD Notification', 'Can not process transaction.')
                        return
                    }

                    be.error(res.Message)
                    return
                }

                var amountConverted = Web3.utils.toWei(res.Data.Value.toString());
                var hexAmount = Web3.utils.numberToHex(amountConverted, 'ether');

                let data = {
                    from: res.Data.From,
                    to: res.Data.To,
                    value: hexAmount,
                    gasPrice: res.Data.GasPrice,
                    gas: res.Data.Gas,
                    data: res.Data.TransactionHex
                }

                let transactionHash = await sendTransaction(data)
                    .then(txh => txh)
                    .catch(error => {
                        if (window.ethereum.isMetaMask) {
                            UpdateErrorMetaMask(data.data, error.code);

                            if (error.code === 4001)
                                be.error('MetaMask Notification', 'Transaction was Rejected')
                            else {
                                be.error('MetaMask Notification', 'Something went wrong! Please contact administrator for support. Code: ' + error.code)
                            }
                        }

                        if (window.ethereum.isTrust) {
                            UpdateErrorMetaMask(data.data, 4001);
                            be.error('MetaMask Notification', 'Transaction was Rejected')
                        }

                        be.stopLoading();
                    })

                if (!transactionHash) {
                    return;
                }

                be.stopLoading();
                be.startLoading('<b>We are processing your transaction.</b><b> Kindly wait for a moment ultil the process completed...</b>');

                VerifyMetaMaskRequest(transactionHash).then(res => {
                    be.stopLoading();

                    if (!res.Success) {

                        be.error('ICD Notification', res.Message)
                        return
                    }
                    be.success('ICD Notification', res.Message)

                    $('.metamask_amount').val(0)
                    $('.metamask_icd-amount').val(0)
                });
            })
                .catch(error => {
                    be.error('ICD Notification', 'Something went wrong! please, contact administrator.')
                    be.stopLoading();
                })
        }

        function ShowConnectButton() {
            $('.metamask_button').off('click')

            $('.metamask_button').attr('data-type', 'connect')
                .html('Connect Wallet');

            $('.metamask_button[data-type="connect"]').on('click', async function () {
                $('#connect-wallet-modal').modal('show');
            });
        }

        function ShowSendButton() {
            $('.metamask_button').off('click')

            $('.metamask_button').attr('data-type', 'send').html('Buy')

            $('.metamask_button[data-type="send"]').on('click', async function () {

                var isValidNet = await MetaMaskHelper.CheckNetWork();

                if (isValidNet) {
                    await confirmProgressTransaction()
                }
            });
        }

        async function ConnectTrustWallet() {

            if (MetaMaskHelper.HasInstalledTrust())
                await handleRequestAccounts();
        }

        async function ConnectMetaMask() {
            if (!MetaMaskHelper.HasInstalledMetaMask()) {

                if (be.isDevice()) {
                    window.location.replace('https://metamask.app.link/dapp/icdefi.org/home/presale');
                    return;
                }

                be.confirm('ICD notification', 'You have not installed MetaMask, please install MetaMask', function () {
                    window.location.replace('https://metamask.io/');
                });

                return;
            }

            await handleRequestAccounts();
        }


        //request to metamask if not connected, will show a prompt from metamask
        async function handleRequestAccounts() {
            try {
                let accounts = await MetaMaskHelper.Web3Instant.eth.requestAccounts()

                if (accounts && accounts.length > 0) {
                    $('.metamask_address').val(accounts[0]);

                    $('.metamask_form').removeClass('d-none')

                    return accounts
                }
            } catch (e) {
                ShowConnectButton()
            }
        }

        async function sendTransaction(data) {
            return await ethereum.request({
                method: 'eth_sendTransaction',
                params: [
                    data,
                ]
            });
        }

        function handleAccountsChanged(accounts) {
            if (accounts.length === 0) {
                // MetaMask is locked or the user has not connected any accounts
                ShowConnectButton()
                $('.metamask_form').hide();
            } else if (accounts[0]) {
                // Do any other work!
                $('.metamask_address').val(accounts[0])
                ShowSendButton()
                $('.metamask_form').show();
            }
        }

        function InitializeTransactionMetaMask() {

            let data = {
                BNBAmount: $('.metamask_amount').val(),
                Address: $('.metamask_address').val()
            }

            data.IsDevice = MetaMaskHelper.isDevice

            if (ethereum.isMetaMask) {
                data.WalletRequest = "MetaMask"
            }

            if (ethereum.isTrust) {
                data.WalletRequest = "Trust"
            }

            return PostAsync('/MetaMask/InitializeTransactionMetaMask', data);
        }

        function VerifyMetaMaskRequest(transactionHash) {
            return PostAsync('/MetaMask/VerifyMetaMaskRequest', transactionHash)
        }

        function UpdateErrorMetaMask(transactionHex, errorCode) {
            let data = {
                transactionHex: transactionHex,
                errorCode: errorCode
            }
            return PostAsync('/MetaMask/UpdateErrorMetaMask', data);
        }


        async function PostAsync(url = '', data = {}) {
            var token = $('input[name="__RequestVerificationToken"]').val();

            return await fetch(url, {
                method: 'POST',
                dataType: 'json',
                headers: {
                    "RequestVerificationToken": token,
                    'Content-Type': 'application/json; charset=utf-8'
                    // 'Content-Type': 'application/x-www-form-urlencoded',
                },
                body: JSON.stringify(data)
            }).then(response => response.json())
        }

        async function GetAsync(url = '') {
            return await fetch(url, {
                method: 'GET',
                dataType: 'json',
                headers: {
                    'Content-Type': 'application/json; charset=utf-8'
                    // 'Content-Type': 'application/x-www-form-urlencoded',
                },
            }).then(response => response.json())
        }
    }
}
