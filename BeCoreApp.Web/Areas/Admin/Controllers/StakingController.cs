﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Security.Claims;
using System.Threading.Tasks;
using BeCoreApp.Application.Interfaces;
using BeCoreApp.Application.ViewModels.BlockChain;
using BeCoreApp.Application.ViewModels.BotTelegram;
using BeCoreApp.Application.ViewModels.Common;
using BeCoreApp.Application.ViewModels.Staking;
using BeCoreApp.Application.ViewModels.System;
using BeCoreApp.Data.Entities;
using BeCoreApp.Data.Enums;
using BeCoreApp.Extensions;
using BeCoreApp.Services;
using BeCoreApp.Utilities.Constants;
using BeCoreApp.Utilities.Dtos;
using BeCoreApp.Utilities.Extensions;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Nethereum.RPC.Eth.DTOs;
using Nethereum.Util;
using Newtonsoft.Json;

namespace BeCoreApp.Areas.Admin.Controllers
{
    public class StakingController : BaseController
    {
        private readonly IUserService _userService;
        private readonly UserManager<AppUser> _userManager;
        private readonly IBlockChainService _blockChainService;
        private readonly IWalletInvestTransactionService _walletInvestTransactionService;
        private readonly IWalletLockTransactionService _walletLockTransactionService;
        private readonly IWalletBNBAffiliateTransactionService _walletBNBAffiliateTransactionService;
        private readonly IWalletBNBBEP20TransactionService _walletBNBBEP20TransactionService;
        private readonly ITransactionService _transactionService;
        private readonly ILogger<ExchangeController> _logger;
        private readonly AddressUtil _addressUtil = new AddressUtil();
        private readonly IEmailSender _emailSender;
        private readonly IChartRoundService _chartRoundService;
        private readonly IStakingService _stakingService;
        private readonly IStakingAffiliateService _stakingAffiliateService;
        private readonly IStakingRewardService _stakingRewardService;
        public StakingController(
            IStakingRewardService stakingRewardService,
            IStakingAffiliateService stakingAffiliateService,
            IStakingService stakingService,
            IWalletBNBBEP20TransactionService walletBNBBEP20TransactionService,
            IWalletBNBAffiliateTransactionService walletBNBAffiliateTransactionService,
            IChartRoundService chartRoundService,
            IWalletInvestTransactionService walletInvestTransactionService,
            IWalletLockTransactionService walletLockTransactionService,
            ILogger<ExchangeController> logger,
            ITransactionService transactionService,
            UserManager<AppUser> userManager,
            IUserService userService,
            IEmailSender emailSender,
            IBlockChainService blockChainService)
        {
            _stakingAffiliateService = stakingAffiliateService;
            _stakingRewardService = stakingRewardService;
            _stakingService = stakingService;
            _walletBNBAffiliateTransactionService = walletBNBAffiliateTransactionService;
            _walletBNBBEP20TransactionService = walletBNBBEP20TransactionService;
            _chartRoundService = chartRoundService;
            _walletInvestTransactionService = walletInvestTransactionService;
            _walletLockTransactionService = walletLockTransactionService;
            _logger = logger;
            _transactionService = transactionService;
            _userManager = userManager;
            _blockChainService = blockChainService;
            _userService = userService;
            _emailSender = emailSender;
        }

        [HttpGet]
        public IActionResult Index()
        {
            var enumStakingTimeLines = ((StakingTimeLine[])Enum.GetValues(typeof(StakingTimeLine)))
                .Select(c => new EnumModel()
                {
                    Value = (int)c,
                    Name = c.GetDescription()
                }).ToList();

            ViewBag.TimeLineType = new SelectList(enumStakingTimeLines, "Value", "Name");

            var enumStakingPackages = ((StakingPackage[])Enum.GetValues(typeof(StakingPackage)))
                .Select(c => new EnumModel()
                {
                    Value = (int)c,
                    Name = c.GetDescription()
                }).ToList();

            ViewBag.PackageType = new SelectList(enumStakingPackages, "Value", "Name");

            return View();
        }

        [HttpGet]
        public IActionResult GetPackageAllPaging(string keyword, int page, int pageSize)
        {
            string appUserId = string.Empty;

            var roleName = User.GetSpecificClaim("RoleName");
            if (roleName.ToLower() == "customer")
                appUserId = User.GetSpecificClaim("UserId");

            var model = _stakingService.GetAllPaging(keyword, appUserId, page, pageSize);

            return new OkObjectResult(model);
        }

        [HttpGet]
        public IActionResult GetCommissionAllPaging(string keyword, int page, int pageSize)
        {
            string appUserId = string.Empty;

            var roleName = User.GetSpecificClaim("RoleName");
            if (roleName.ToLower() == "customer")
                appUserId = User.GetSpecificClaim("UserId");

            var model = _stakingRewardService.GetAllPaging(keyword, appUserId, page, pageSize);

            return new OkObjectResult(model);
        }

        [HttpGet]
        public IActionResult GetAffiliateAllPaging(string keyword, int page, int pageSize)
        {
            string appUserId = string.Empty;

            var roleName = User.GetSpecificClaim("RoleName");
            if (roleName.ToLower() == "customer")
                appUserId = User.GetSpecificClaim("UserId");

            var model = _stakingAffiliateService.GetAllPaging(keyword, appUserId, page, pageSize);

            return new OkObjectResult(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> GetCommission(int id)
        {
            try
            {
                var userId = User.GetSpecificClaim("UserId");

                var appUser = await _userManager.FindByIdAsync(userId);
                if (appUser == null)
                    return new OkObjectResult(new GenericResult(false, "Account does not exist"));

                var staking = _stakingService.GetById(id);
                if (staking == null)
                    return new OkObjectResult(new GenericResult(false, "Package does not exist"));

                if (staking.Type == StakingType.Finish)
                    return new OkObjectResult(new GenericResult(false, "Package is finish"));

                if (staking.StakingTimes == staking.ReceiveTimes)
                    return new OkObjectResult(new GenericResult(false, "Package is finish"));

                if (((int)staking.TimeLine * 30) == staking.ReceiveTimes)
                    return new OkObjectResult(new GenericResult(false, "Package is finish"));

                if (staking.DateCreated.Date == DateTime.Now.Date)
                    return new OkObjectResult(new GenericResult(false, "Get interest starting tomorrow"));

                if (staking.ReceiveLatest.Date == DateTime.Now.Date)
                    return new OkObjectResult(new GenericResult(false, "You got today's interest"));

                var totalInterestRate = staking.InterestRate + appUser.StakingInterest;

                var timesAmount = Math.Round(staking.StakingAmount / staking.StakingTimes, 2);

                var commission = Math.Round(timesAmount * (totalInterestRate / 100), 2);

                appUser.ICDCommissionBalance += commission;
                var updateCommission = await _userManager.UpdateAsync(appUser);

                if (updateCommission.Succeeded)
                {
                    staking.ReceiveAmount += commission;
                    staking.ReceiveTimes += 1;
                    staking.ReceiveLatest = DateTime.Now;

                    if (staking.StakingTimes == staking.ReceiveTimes)
                        staking.Type = StakingType.Finish;

                    _stakingService.Update(staking);
                    _stakingService.Save();

                    var stakingReward = _stakingRewardService.Add(new StakingRewardViewModel
                    {
                        AppUserId = appUser.Id,
                        PackageInterestRate = staking.InterestRate,
                        SuddenInterestRate = appUser.StakingInterest,
                        RealInterestRate = totalInterestRate,
                        DateCreated = DateTime.Now,
                        Amount = commission,
                        StakingId = staking.Id
                    });
                    _stakingRewardService.Save();

                    if (appUser.IsSystem == false && appUser.ReferralId.HasValue)
                    {
                        var referralF1 = await _userManager.FindByIdAsync(appUser.ReferralId.ToString());
                        if (referralF1 != null && referralF1.IsSystem == false && referralF1.StakingBalance >= 1000)
                        {
                            var referralF1Commission = Math.Round(commission * 0.2m, 2);

                            referralF1.ICDAffiliateBalance += referralF1Commission;
                            var updateReferralF1Affiliate = await _userManager.UpdateAsync(referralF1);
                            if (updateReferralF1Affiliate.Succeeded)
                            {
                                _stakingAffiliateService.Add(new StakingAffiliateViewModel
                                {
                                    AppUserId = referralF1.Id,
                                    DateCreated = DateTime.Now,
                                    Amount = referralF1Commission,
                                    StakingRewardId = stakingReward.Id
                                });

                                _stakingAffiliateService.Save();
                            }


                            if (referralF1.IsSystem == false && referralF1.ReferralId.HasValue)
                            {
                                var referralF2 = await _userManager.FindByIdAsync(referralF1.ReferralId.ToString());
                                if (referralF2 != null && referralF2.IsSystem == false && referralF2.StakingBalance >= 1000)
                                {
                                    var referralF2Commission = Math.Round(commission * 0.1m, 2);

                                    referralF2.ICDAffiliateBalance += referralF2Commission;
                                    var updateReferralF2Affiliate = await _userManager.UpdateAsync(referralF2);
                                    if (updateReferralF2Affiliate.Succeeded)
                                    {
                                        _stakingAffiliateService.Add(new StakingAffiliateViewModel
                                        {
                                            AppUserId = referralF2.Id,
                                            DateCreated = DateTime.Now,
                                            Amount = referralF2Commission,
                                            StakingRewardId = stakingReward.Id
                                        });

                                        _stakingAffiliateService.Save();
                                    }

                                    if (referralF2.IsSystem == false && referralF2.ReferralId.HasValue)
                                    {
                                        var referralF3 = await _userManager.FindByIdAsync(referralF2.ReferralId.ToString());
                                        if (referralF3 != null && referralF3.IsSystem == false && referralF3.StakingBalance >= 1000)
                                        {
                                            var referralF3Commission = Math.Round(commission * 0.05m, 2);

                                            referralF3.ICDAffiliateBalance += referralF3Commission;
                                            var updateReferralF3Affiliate = await _userManager.UpdateAsync(referralF3);
                                            if (updateReferralF3Affiliate.Succeeded)
                                            {
                                                _stakingAffiliateService.Add(new StakingAffiliateViewModel
                                                {
                                                    AppUserId = referralF3.Id,
                                                    DateCreated = DateTime.Now,
                                                    Amount = referralF3Commission,
                                                    StakingRewardId = stakingReward.Id
                                                });

                                                _stakingAffiliateService.Save();
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                else
                {
                    return new OkObjectResult(new GenericResult(false, updateCommission.Errors.FirstOrDefault().Description));
                }

                return new OkObjectResult(new GenericResult(true, "Get interest is success"));
            }
            catch (Exception ex)
            {
                _logger.LogError("StakingController_GetCommission: {0}", ex.Message);
                return new OkObjectResult(new GenericResult(false, ex.Message));
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> BuyStaking(string modelJson)
        {
            try
            {
                var model = JsonConvert.DeserializeObject<BuyStakingViewModel>(modelJson);

                decimal priceICD = 0.12M;

                var amountICD = (int)model.Package;

                var userId = User.GetSpecificClaim("UserId");

                var appUser = await _userManager.FindByIdAsync(userId);
                if (appUser == null)
                    return new OkObjectResult(new GenericResult(false, "Account does not exist"));

                var appUserTotalStaking = _stakingService.GetTotalPackage(appUser.Id, StakingType.Process);
                if ((appUserTotalStaking + amountICD) > 100000)
                    return new OkObjectResult(new GenericResult(false, "Maximum staking 100,000 ICD"));

                if (model.Type == 1 || model.Type == 2)
                {
                    decimal paymentUSD = priceICD * amountICD;

                    decimal priceBNBBep20 = _blockChainService.GetCurrentPrice("BNB", "USD");
                    if (priceBNBBep20 == 0)
                        return new OkObjectResult(new GenericResult(false, "There is a problem loading the currency value!"));

                    var paymentBNB = Math.Round(paymentUSD / priceBNBBep20, 4);

                    if (model.Type == 1)
                    {
                        if (paymentBNB > appUser.MainBalance)
                            return new OkObjectResult(new GenericResult(false, "Your balance is not enough to make a transaction"));

                        appUser.MainBalance -= paymentBNB;
                        appUser.StakingBalance += amountICD;

                        var resultMainBalanceUpdate = await _userManager.UpdateAsync(appUser);

                        if (resultMainBalanceUpdate.Succeeded)
                        {
                            _walletBNBBEP20TransactionService.Add(new WalletBNBBEP20TransactionViewModel
                            {
                                AddressFrom = "Wallet BNB BEP20",
                                AddressTo = "SYSTEM",
                                Amount = paymentBNB,
                                AmountReceive = paymentBNB,
                                Fee = 0,
                                FeeAmount = 0,
                                DateCreated = DateTime.Now,
                                AppUserId = appUser.Id,
                                TransactionHash = "SYSTEM",
                                Type = WalletBNBBEP20TransactionType.PayStaking
                            });
                            _walletBNBBEP20TransactionService.Save();
                        }
                        else
                            return new OkObjectResult(new GenericResult(false, resultMainBalanceUpdate.Errors.FirstOrDefault().Description));
                    }
                    else
                    {
                        if (paymentBNB > appUser.BNBAffiliateBalance)
                            return new OkObjectResult(new GenericResult(false, "Your balance is not enough to make a transaction"));

                        appUser.BNBAffiliateBalance -= paymentBNB;
                        appUser.StakingBalance += amountICD;

                        var resultBNBAffiliateBalanceUpdate = await _userManager.UpdateAsync(appUser);

                        if (resultBNBAffiliateBalanceUpdate.Succeeded)
                        {
                            _walletBNBAffiliateTransactionService.Add(new WalletBNBAffiliateTransactionViewModel
                            {
                                AddressFrom = "Wallet BNB Affiliate",
                                AddressTo = "SYSTEM",
                                Amount = paymentBNB,
                                AmountReceive = paymentBNB,
                                Fee = 0,
                                FeeAmount = 0,
                                DateCreated = DateTime.Now,
                                AppUserId = appUser.Id,
                                TransactionHash = "SYSTEM",
                                Type = WalletBNBAffiliateTransactionType.PayStaking
                            });
                            _walletBNBAffiliateTransactionService.Save();
                        }
                        else
                            return new OkObjectResult(new GenericResult(false, resultBNBAffiliateBalanceUpdate.Errors.FirstOrDefault().Description));
                    }
                }
                else
                {
                    if (amountICD > appUser.InvestBalance)
                        return new OkObjectResult(new GenericResult(false, "Your balance is not enough to make a transaction"));

                    appUser.StakingBalance += amountICD;
                    appUser.InvestBalance -= amountICD;

                    var resultInvestBalanceUpdate = await _userManager.UpdateAsync(appUser);

                    if (resultInvestBalanceUpdate.Succeeded)
                    {
                        _walletInvestTransactionService.Add(new WalletInvestTransactionViewModel
                        {
                            AddressFrom = "Wallet Invest",
                            AddressTo = "SYSTEM",
                            Amount = amountICD,
                            AmountReceive = amountICD,
                            Fee = 0,
                            FeeAmount = 0,
                            DateCreated = DateTime.Now,
                            AppUserId = appUser.Id,
                            TransactionHash = "SYSTEM",
                            Type = WalletInvestTransactionType.PayStaking
                        });
                        _walletInvestTransactionService.Save();
                    }
                    else
                        return new OkObjectResult(new GenericResult(false, resultInvestBalanceUpdate.Errors.FirstOrDefault().Description));
                }

                int interestRate = 0;
                if (model.TimeLine == StakingTimeLine.TwelveMonth)
                    interestRate = 65;
                else if (model.TimeLine == StakingTimeLine.NineMonth)
                    interestRate = 45;
                else if (model.TimeLine == StakingTimeLine.SixMonth)
                    interestRate = 25;
                else
                    interestRate = 10;

                _stakingService.Add(new StakingViewModel
                {
                    TimeLine = model.TimeLine,
                    Package = model.Package,
                    AppUserId = appUser.Id,
                    InterestRate = interestRate,
                    StakingTimes = (int)model.TimeLine * 30,
                    StakingAmount = amountICD,
                    ReceiveTimes = 0,
                    ReceiveAmount = 0,
                    Type = StakingType.Process,
                    ReceiveLatest = DateTime.Now,
                    DateCreated = DateTime.Now
                });
                _stakingService.Save();


                if (appUser.ReferralId.HasValue && appUser.IsSystem == false)
                {
                    var referralF1 = await _userManager.FindByIdAsync(appUser.ReferralId.ToString());
                    if (referralF1 != null && referralF1.IsSystem == false
                        && referralF1.StakingBalance >= 1000 && referralF1.StakingInterest < 100)
                    {
                        int qualifiedNumber = 0;

                        decimal maxPackage = _stakingService.GetMaxPackage(referralF1.Id);

                        var userDownlines = _userManager.Users.Where(x => x.ReferralId == referralF1.Id);
                        foreach (var downline in userDownlines)
                        {
                            if (downline.StakingBalance >= maxPackage)
                                qualifiedNumber += 1;
                        }

                        int interest = 0;
                        if (qualifiedNumber >= 2 && qualifiedNumber < 5)
                            interest = 25;
                        else if (qualifiedNumber >= 5 && qualifiedNumber < 10)
                            interest = 50;
                        else if (qualifiedNumber >= 10 && qualifiedNumber < 20)
                            interest = 75;
                        else if (qualifiedNumber >= 20)
                            interest = 100;

                        if (referralF1.StakingInterest < interest)
                        {
                            referralF1.StakingInterest = interest;
                            await _userManager.UpdateAsync(referralF1);
                        }
                    }
                }

                return new OkObjectResult(new GenericResult(true, "Staking is successful."));
            }
            catch (Exception ex)
            {
                _logger.LogError("StakingController_BuyStaking: {0}", ex.Message);
                return new OkObjectResult(new GenericResult(false, ex.Message));
            }
        }

        [HttpGet]
        public IActionResult CaculationStakingByType(string modelJson)
        {
            try
            {
                var model = JsonConvert.DeserializeObject<BuyStakingViewModel>(modelJson);
                var amountICD = (int)model.Package;
                if (model.Type == 3)
                {
                    model.AmountPayment = amountICD;
                    return new OkObjectResult(model);
                }
                else
                {
                    decimal priceICD = 0.12M;
                    decimal paymentUSD = priceICD * amountICD;

                    decimal priceBNBBep20 = _blockChainService.GetCurrentPrice("BNB", "USD");
                    if (priceBNBBep20 == 0)
                        return new OkObjectResult(new GenericResult(false, "There is a problem loading the currency value!"));

                    var paymentBNB = Math.Round(paymentUSD / priceBNBBep20, 4);

                    model.AmountPayment = paymentBNB;

                    return new OkObjectResult(model);
                }
            }
            catch (Exception ex)
            {
                _logger.LogError("GameController_CaculationStakingByType: {0}", ex.Message);
                return new OkObjectResult(new GenericResult(false, ex.Message));
            }
        }

        [HttpGet]
        public async Task<IActionResult> GetWalletBlanceByType(int type)
        {
            var userId = User.GetSpecificClaim("UserId");
            var appUser = await _userService.GetById(userId);
            decimal walletBalance = 0;
            if (type == 1)
            {
                walletBalance = appUser.MainBalance;
            }
            else if (type == 2)
            {
                walletBalance = appUser.BNBAffiliateBalance;
            }
            else if (type == 3)
            {
                walletBalance = appUser.InvestBalance;
            }

            return new OkObjectResult(walletBalance);
        }

        [HttpGet]
        public async Task<IActionResult> GetWalletBlance()
        {
            var userId = User.GetSpecificClaim("UserId");

            var appUser = await _userService.GetById(userId);

            var model = new WalletViewModel()
            {
                MainBalance = appUser.MainBalance,
            };

            return new OkObjectResult(model);
        }


        private async Task SendComisionMail(TransactionViewModel vm)
        {
            try
            {
                var appUser = await _userManager.FindByIdAsync(vm.AppUserId.ToString());
                if (appUser != null)
                {
                    var parameters = new ComisionMessageParam
                    {
                        Amount = vm.Amount,
                        RecievedAt = DateTime.Now,
                        AddressTo = vm.AddressTo,
                        Unit = vm.Unit.ToString(),
                        Rate = vm.Unit == TransactionUnit.BNB ? vm.PriceBNB : 0,
                        AddressFrom = vm.AddressFrom
                    };

                    var message = _emailSender.BuildReportCommisionMessage(parameters);

                    await _emailSender.TrySendEmailAsync(appUser.Email, $"Commission received {DateTime.UtcNow.ToICDFormatTime()}(UTC)", message);
                }
            }
            catch (Exception e)
            {
                _logger.LogError(e, "Failed to Send Commision Mail");
            }
        }
    }
}