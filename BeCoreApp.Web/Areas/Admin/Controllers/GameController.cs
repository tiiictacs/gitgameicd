﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BeCoreApp.Application.Interfaces;
using BeCoreApp.Application.ViewModels.Game;
using BeCoreApp.Application.ViewModels.System;
using BeCoreApp.Data.Entities;
using BeCoreApp.Data.Enums;
using BeCoreApp.Extensions;
using BeCoreApp.Utilities.Dtos;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Nethereum.Util;
using Newtonsoft.Json;

namespace BeCoreApp.Areas.Admin.Controllers
{
    public class GameController : BaseController
    {
        private readonly IWalletInvestTransactionService _walletInvestTransactionService;
        private readonly IWalletBNBAffiliateTransactionService _walletBNBAffiliateTransactionService;
        private readonly IWalletBNBBEP20TransactionService _walletBNBBEP20TransactionService;
        private readonly IUserService _userService;
        private readonly UserManager<AppUser> _userManager;
        private readonly IBlockChainService _blockChainService;
        private readonly ITransactionService _transactionService;
        private readonly ITRONService _tronService;
        private readonly ILogger<WalletController> _logger;
        private readonly AddressUtil _addressUtil = new AddressUtil();
        private readonly ILuckyRoundHistoryService _luckyRoundHistoryService;
        private readonly ILuckyRoundService _luckyRoundService;
        private readonly IGameTicketService _gameTicketService;
        public GameController(
            IGameTicketService gameTicketService,
            IWalletInvestTransactionService walletInvestTransactionService,
            IWalletBNBAffiliateTransactionService walletBNBAffiliateTransactionService,
            IWalletBNBBEP20TransactionService walletBNBBEP20TransactionService,
            ILuckyRoundHistoryService luckyRoundHistoryService,
            ILuckyRoundService luckyRoundService,
            ILogger<WalletController> logger,
            ITRONService tronService,
            ITransactionService transactionService,
            UserManager<AppUser> userManager,
            IUserService userService,
            IBlockChainService blockChainService)
        {
            _gameTicketService = gameTicketService;
            _walletInvestTransactionService = walletInvestTransactionService;
            _walletBNBAffiliateTransactionService = walletBNBAffiliateTransactionService;
            _walletBNBBEP20TransactionService = walletBNBBEP20TransactionService;
            _luckyRoundHistoryService = luckyRoundHistoryService;
            _luckyRoundService = luckyRoundService;
            _logger = logger;
            _tronService = tronService;
            _transactionService = transactionService;
            _userManager = userManager;
            _blockChainService = blockChainService;
            _userService = userService;
        }

        [HttpGet]
        public IActionResult Index()
        {
            return View();
        }

        #region Ticket
        [HttpGet]
        public IActionResult GetAllTicketPaging(string keyword, int page, int pageSize)
        {
            string appUserId = string.Empty;

            var roleName = User.GetSpecificClaim("RoleName");
            if (roleName.ToLower() == "customer")
                appUserId = User.GetSpecificClaim("UserId");

            var model = _gameTicketService.GetAllPaging(keyword, appUserId, page, pageSize);
            return new OkObjectResult(model);
        }
        #endregion

        #region LuckyRound
        [HttpGet]
        public IActionResult LuckyRound()
        {
            return View();
        }

        public List<SegmentViewModel> SetSegments(List<SegmentViewModel> Segments, int value, string message)
        {
            if (Segments == null)
                Segments = new List<SegmentViewModel>();

            var segment = Segments.FirstOrDefault(x => x.Value == value);
            if (segment == null)
                Segments.Add(new SegmentViewModel { Value = value, Message = message, Times = 1 });
            else
                segment.Times += 1;

            return Segments;
        }
        public class SegmentViewModel
        {
            public int Value { get; set; }
            public string Message { get; set; }
            public int Times { get; set; }
        }

        [HttpPost]
        public async Task<IActionResult> LuckyRoundResult(int totalSpin)
        {
            try
            {
                var Segments = new List<SegmentViewModel>();

                var userId = User.GetSpecificClaim("UserId");

                var appUser = await _userManager.FindByIdAsync(userId);
                if (appUser == null)
                {
                    return new OkObjectResult(new GenericResult(false, "Account does not exist"));
                }

                if (appUser.TicketBalance < 1)
                {
                    return new OkObjectResult(new GenericResult(false, "Your ticket balance is not enough to make a spin!"));
                }

                int[] ticketWinNumber = new int[] { 8,108,208,308, 408, 508, 608, 708, 808, 908, 18, 118,
                218, 318, 418, 518, 618, 718, 818, 918, 28, 128, 228, 328, 428, 528, 628, 728, 828,
                928, 38, 138, 238, 338, 438, 538, 638, 738, 838, 938, 48, 148, 248, 348, 448, 548, 648, 748, 848, 948 };

                int[] icd50WinNumber = new int[] { 1 , 101 , 301 , 401 , 601 , 901 , 503 , 803 , 5 , 205 , 305 , 605 , 905 ,
                107, 407, 9, 709, 211, 311, 511, 911, 13, 113, 413, 613, 813, 314, 17, 217, 917, 119, 519, 719, 421, 621, 921,
                23, 123, 223, 323, 823, 525, 925, 27, 127, 427, 229, 629, 31, 531, 731, 931, 233, 333, 633, 135, 435, 37, 937,
                239, 41, 141, 541, 841, 243, 343, 443, 643, 943, 47, 247, 747, 947, 149, 549, 351, 451, 53, 653, 853, 155, 255,
                955, 357, 557, 59, 459, 659, 759, 861, 961, 63, 163, 263, 363, 563, 467, 667, 767, 967, 69, 269, 569, 869, 171,
                773, 973, 75, 475, 675, 875, 277, 377, 577, 179, 879, 979, 81, 581, 183, 283, 483, 783, 883, 85, 385, 685, 985,
                187, 287, 587, 887, 89, 291, 591, 791, 193, 493, 693, 993, 95, 595, 895, 297, 397, 797, 99, 199, 499, 699, 999};

                if (totalSpin == 0)
                {
                    totalSpin = appUser.TicketBalance;
                }

                for (int i = 0; i < totalSpin; i++)
                {
                    if (appUser.TicketBalance < 1)
                    {
                        break;
                    }

                    appUser.TicketBalance -= 1;
                    var updateUserTicket = await _userManager.UpdateAsync(appUser);

                    if (!updateUserTicket.Succeeded)
                    {
                        return new OkObjectResult(new GenericResult(false, updateUserTicket.Errors.FirstOrDefault().Description));
                    }

                    _gameTicketService.Add(new GameTicketViewModel
                    {
                        AddressFrom = "Wallet Ticket",
                        AddressTo = "SYSTEM",
                        Amount = 1,
                        DateCreated = DateTime.Now,
                        AppUserId = appUser.Id,
                        Type = GameTicketType.PayLuckyRound
                    });
                    _gameTicketService.Save();

                    var luckyRoundProcess = _luckyRoundService.GetProcess();

                    _luckyRoundHistoryService.Add(new LuckyRoundHistoryViewModel
                    {
                        AddressFrom = "Wallet Ticket",
                        AddressTo = "SYSTEM",
                        AppUserId = appUser.Id,
                        LuckyRoundId = luckyRoundProcess.Id,
                        Amount = 1,
                        Type = LuckyRoundHistoryType.PayLuckyRound,
                        Unit = LuckyRoundHistoryUnit.Ticket,
                        DateCreated = DateTime.Now,
                        DateUpdated = DateTime.Now
                    });
                    _luckyRoundHistoryService.Save();

                    var countLuckyRoundHistory = _luckyRoundHistoryService
                        .CountByType(LuckyRoundHistoryType.PayLuckyRound, luckyRoundProcess.Id);

                    if (countLuckyRoundHistory == 207 || countLuckyRoundHistory == 416)
                    {
                        _luckyRoundHistoryService.Add(new LuckyRoundHistoryViewModel
                        {
                            AddressFrom = "SYSTEM",
                            AddressTo = "Wallet Invest",
                            AppUserId = appUser.Id,
                            LuckyRoundId = luckyRoundProcess.Id,
                            Amount = 500,
                            Type = LuckyRoundHistoryType.Win500ICD,
                            Unit = LuckyRoundHistoryUnit.ICD,
                            DateCreated = DateTime.Now,
                            DateUpdated = DateTime.Now
                        });
                        _luckyRoundHistoryService.Save();

                        appUser.InvestBalance += 500;
                        var updateUserInvest = await _userManager.UpdateAsync(appUser);

                        if (!updateUserInvest.Succeeded)
                            return new OkObjectResult(new GenericResult(false, updateUserInvest.Errors.FirstOrDefault().Description));

                        _walletInvestTransactionService.Add(
                            new WalletInvestTransactionViewModel
                            {
                                AddressFrom = "SYSTEM",
                                AddressTo = "Wallet Invest",
                                Amount = 500,
                                AmountReceive = 500,
                                AppUserId = appUser.Id,
                                Fee = 0,
                                FeeAmount = 0,
                                TransactionHash = "SYSTEM",
                                DateCreated = DateTime.Now,
                                Type = WalletInvestTransactionType.WinLuckyRound
                            });
                        _walletInvestTransactionService.Save();

                        Segments = SetSegments(Segments, 3, "500 ICD");
                    }
                    else if (countLuckyRoundHistory == 369 || countLuckyRoundHistory == 776)
                    {
                        _luckyRoundHistoryService.Add(new LuckyRoundHistoryViewModel
                        {
                            AddressFrom = "SYSTEM",
                            AddressTo = "Wallet Invest",
                            AppUserId = appUser.Id,
                            LuckyRoundId = luckyRoundProcess.Id,
                            Amount = 1000,
                            Type = LuckyRoundHistoryType.Win1000ICD,
                            Unit = LuckyRoundHistoryUnit.ICD,
                            DateCreated = DateTime.Now,
                            DateUpdated = DateTime.Now
                        });
                        _luckyRoundHistoryService.Save();


                        appUser.InvestBalance += 1000;
                        var updateUserInvest = await _userManager.UpdateAsync(appUser);

                        if (!updateUserInvest.Succeeded)
                            return new OkObjectResult(new GenericResult(false, updateUserInvest.Errors.FirstOrDefault().Description));

                        _walletInvestTransactionService.Add(
                            new WalletInvestTransactionViewModel
                            {
                                AddressFrom = "SYSTEM",
                                AddressTo = "Wallet Invest",
                                Amount = 1000,
                                AmountReceive = 1000,
                                AppUserId = appUser.Id,
                                Fee = 0,
                                FeeAmount = 0,
                                TransactionHash = "SYSTEM",
                                DateCreated = DateTime.Now,
                                Type = WalletInvestTransactionType.WinLuckyRound
                            });
                        _walletInvestTransactionService.Save();

                        Segments = SetSegments(Segments, 4, "1000 ICD");
                    }
                    else if (countLuckyRoundHistory == 485 || countLuckyRoundHistory == 834)
                    {
                        _luckyRoundHistoryService.Add(new LuckyRoundHistoryViewModel
                        {
                            AddressFrom = "SYSTEM",
                            AddressTo = "Wallet Invest",
                            AppUserId = appUser.Id,
                            LuckyRoundId = luckyRoundProcess.Id,
                            Amount = 3000,
                            Type = LuckyRoundHistoryType.Win3000ICD,
                            Unit = LuckyRoundHistoryUnit.ICD,
                            DateCreated = DateTime.Now,
                            DateUpdated = DateTime.Now
                        });
                        _luckyRoundHistoryService.Save();


                        appUser.InvestBalance += 3000;
                        var updateUserInvest = await _userManager.UpdateAsync(appUser);

                        if (!updateUserInvest.Succeeded)
                            return new OkObjectResult(new GenericResult(false, updateUserInvest.Errors.FirstOrDefault().Description));

                        _walletInvestTransactionService.Add(
                            new WalletInvestTransactionViewModel
                            {
                                AddressFrom = "SYSTEM",
                                AddressTo = "Wallet Invest",
                                Amount = 3000,
                                AmountReceive = 3000,
                                AppUserId = appUser.Id,
                                Fee = 0,
                                FeeAmount = 0,
                                TransactionHash = "SYSTEM",
                                DateCreated = DateTime.Now,
                                Type = WalletInvestTransactionType.WinLuckyRound
                            });
                        _walletInvestTransactionService.Save();

                        Segments = SetSegments(Segments, 5, "3000 ICD");
                    }
                    else if (countLuckyRoundHistory == 969)
                    {
                        _luckyRoundHistoryService.Add(new LuckyRoundHistoryViewModel
                        {
                            AddressFrom = "SYSTEM",
                            AddressTo = "Wallet Invest",
                            AppUserId = appUser.Id,
                            LuckyRoundId = luckyRoundProcess.Id,
                            Amount = 5000,
                            Type = LuckyRoundHistoryType.Win5000ICD,
                            Unit = LuckyRoundHistoryUnit.ICD,
                            DateCreated = DateTime.Now,
                            DateUpdated = DateTime.Now
                        });
                        _luckyRoundHistoryService.Save();


                        appUser.InvestBalance += 5000;
                        var updateUserInvest = await _userManager.UpdateAsync(appUser);

                        if (!updateUserInvest.Succeeded)
                            return new OkObjectResult(new GenericResult(false, updateUserInvest.Errors.FirstOrDefault().Description));

                        _walletInvestTransactionService.Add(
                            new WalletInvestTransactionViewModel
                            {
                                AddressFrom = "SYSTEM",
                                AddressTo = "Wallet Invest",
                                Amount = 5000,
                                AmountReceive = 5000,
                                AppUserId = appUser.Id,
                                Fee = 0,
                                FeeAmount = 0,
                                TransactionHash = "SYSTEM",
                                DateCreated = DateTime.Now,
                                Type = WalletInvestTransactionType.WinLuckyRound
                            });
                        _walletInvestTransactionService.Save();

                        Segments = SetSegments(Segments, 6, "5000 ICD");
                    }
                    else if (countLuckyRoundHistory == 989)
                    {
                        _luckyRoundHistoryService.Add(new LuckyRoundHistoryViewModel
                        {
                            AddressFrom = "SYSTEM",
                            AddressTo = "Wallet BNB BEP20",
                            AppUserId = appUser.Id,
                            LuckyRoundId = luckyRoundProcess.Id,
                            Amount = 0.5M,
                            Type = LuckyRoundHistoryType.Win05BNB,
                            Unit = LuckyRoundHistoryUnit.BNB,
                            DateCreated = DateTime.Now,
                            DateUpdated = DateTime.Now
                        });
                        _luckyRoundHistoryService.Save();


                        appUser.MainBalance += 0.5M;
                        var updateUserBNBBEP20 = await _userManager.UpdateAsync(appUser);

                        if (!updateUserBNBBEP20.Succeeded)
                            return new OkObjectResult(new GenericResult(false, updateUserBNBBEP20.Errors.FirstOrDefault().Description));

                        _walletBNBBEP20TransactionService.Add(
                            new WalletBNBBEP20TransactionViewModel
                            {
                                AddressFrom = "SYSTEM",
                                AddressTo = "Wallet BNB BEP20",
                                Amount = 0.5M,
                                AmountReceive = 0.5M,
                                AppUserId = appUser.Id,
                                Fee = 0,
                                FeeAmount = 0,
                                TransactionHash = "SYSTEM",
                                DateCreated = DateTime.Now,
                                Type = WalletBNBBEP20TransactionType.WinLuckyRound
                            });
                        _walletBNBBEP20TransactionService.Save();

                        Segments = SetSegments(Segments, 9, "0.5 BNB");
                    }
                    else if (icd50WinNumber.Contains(countLuckyRoundHistory))
                    {
                        _luckyRoundHistoryService.Add(new LuckyRoundHistoryViewModel
                        {
                            AddressFrom = "SYSTEM",
                            AddressTo = "Wallet Invest",
                            AppUserId = appUser.Id,
                            LuckyRoundId = luckyRoundProcess.Id,
                            Amount = 50,
                            Type = LuckyRoundHistoryType.Win50ICD,
                            Unit = LuckyRoundHistoryUnit.ICD,
                            DateCreated = DateTime.Now,
                            DateUpdated = DateTime.Now
                        });
                        _luckyRoundHistoryService.Save();


                        appUser.InvestBalance += 50;
                        var updateUserInvest = await _userManager.UpdateAsync(appUser);

                        if (!updateUserInvest.Succeeded)
                            return new OkObjectResult(new GenericResult(false, updateUserInvest.Errors.FirstOrDefault().Description));

                        _walletInvestTransactionService.Add(
                            new WalletInvestTransactionViewModel
                            {
                                AddressFrom = "SYSTEM",
                                AddressTo = "Wallet Invest",
                                Amount = 50,
                                AmountReceive = 50,
                                AppUserId = appUser.Id,
                                Fee = 0,
                                FeeAmount = 0,
                                TransactionHash = "SYSTEM",
                                DateCreated = DateTime.Now,
                                Type = WalletInvestTransactionType.WinLuckyRound
                            });
                        _walletInvestTransactionService.Save();

                        Segments = SetSegments(Segments, 2, "50 ICD");
                    }
                    else if (ticketWinNumber.Contains(countLuckyRoundHistory))
                    {
                        _luckyRoundHistoryService.Add(new LuckyRoundHistoryViewModel
                        {
                            AddressFrom = "SYSTEM",
                            AddressTo = "Wallet Ticket",
                            AppUserId = appUser.Id,
                            LuckyRoundId = luckyRoundProcess.Id,
                            Amount = 1,
                            Type = LuckyRoundHistoryType.WinTicket,
                            Unit = LuckyRoundHistoryUnit.Ticket,
                            DateCreated = DateTime.Now,
                            DateUpdated = DateTime.Now
                        });
                        _luckyRoundHistoryService.Save();


                        appUser.TicketBalance += 1;
                        var updateUserTicketUpdate = await _userManager.UpdateAsync(appUser);

                        if (!updateUserTicketUpdate.Succeeded)
                            return new OkObjectResult(new GenericResult(false, updateUserTicketUpdate.Errors.FirstOrDefault().Description));

                        _gameTicketService.Add(
                        new GameTicketViewModel
                        {
                            AddressFrom = "SYSTEM",
                            AddressTo = "Wallet Ticket",
                            Amount = 1,
                            DateCreated = DateTime.Now,
                            AppUserId = appUser.Id,
                            Type = GameTicketType.WinLuckyRound
                        });
                        _gameTicketService.Save();

                        Segments = SetSegments(Segments, 8, "TICKET");
                    }
                    else
                    {
                        if (countLuckyRoundHistory == 1000)
                        {
                            luckyRoundProcess.Status = LuckyRoundStatus.Finish;
                            luckyRoundProcess.DateUpdated = DateTime.Now;
                            _luckyRoundService.Update(luckyRoundProcess);
                            _luckyRoundService.Save();
                        }

                        _luckyRoundHistoryService.Add(new LuckyRoundHistoryViewModel
                        {
                            AddressFrom = "N/A",
                            AddressTo = "N/A",
                            AppUserId = appUser.Id,
                            LuckyRoundId = luckyRoundProcess.Id,
                            Amount = 0,
                            Type = LuckyRoundHistoryType.GoodLuck,
                            Unit = LuckyRoundHistoryUnit.None,
                            DateCreated = DateTime.Now,
                            DateUpdated = DateTime.Now
                        });
                        _luckyRoundHistoryService.Save();

                        Segments = SetSegments(Segments, 1, "Good Luck");
                    }
                }

                return new OkObjectResult(new GenericResult(true, Segments));
            }
            catch (Exception ex)
            {
                return new OkObjectResult(new GenericResult(false, ex.Message));
            }
        }

        [HttpGet]
        public async Task<IActionResult> GetTicket()
        {
            string appUserId = User.GetSpecificClaim("UserId");
            var appUser = await _userManager.FindByIdAsync(appUserId);
            return new OkObjectResult(appUser.TicketBalance);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> BuyTicket(string modelJson)
        {
            try
            {
                var model = JsonConvert.DeserializeObject<BuyTicketViewModel>(modelJson);
                var userId = User.GetSpecificClaim("UserId");

                var appUser = await _userManager.FindByIdAsync(userId);
                if (appUser == null)
                    return new OkObjectResult(new GenericResult(false, "Account does not exist"));

                if (model.TicketOrder < 5)
                    return new OkObjectResult(new GenericResult(false, "Minimum buy 5 TICKET"));


                if (model.Type == 1 || model.Type == 2)
                {
                    decimal priceICD = 0.1M;
                    decimal paymentUSD = priceICD * (model.TicketOrder * 25);

                    decimal priceBNBBep20 = _blockChainService.GetCurrentPrice("BNB", "USD");
                    if (priceBNBBep20 == 0)
                        return new OkObjectResult(new GenericResult(false, "There is a problem loading the currency value!"));

                    var paymentBNB = Math.Round(paymentUSD / priceBNBBep20, 4);

                    if (model.Type == 1)
                    {
                        if (paymentBNB > appUser.MainBalance)
                            return new OkObjectResult(new GenericResult(false, "Your balance is not enough to make a transaction"));

                        appUser.TicketBalance += model.TicketOrder;
                        appUser.MainBalance -= paymentBNB;

                        var resultMainBalanceUpdate = await _userManager.UpdateAsync(appUser);

                        if (resultMainBalanceUpdate.Succeeded)
                        {
                            #region Add Invest Transaction
                            _walletBNBBEP20TransactionService.Add(new WalletBNBBEP20TransactionViewModel
                            {
                                AddressFrom = "Wallet BNB BEP20",
                                AddressTo = "SYSTEM",
                                Amount = paymentBNB,
                                AmountReceive = paymentBNB,
                                Fee = 0,
                                FeeAmount = 0,
                                DateCreated = DateTime.Now,
                                AppUserId = appUser.Id,
                                TransactionHash = "SYSTEM",
                                Type = WalletBNBBEP20TransactionType.PayTicket
                            });
                            _walletBNBBEP20TransactionService.Save();
                            #endregion

                            #region Add Game Ticket
                            _gameTicketService.Add(new GameTicketViewModel
                            {
                                AddressFrom = "Wallet BNB BEP20",
                                AddressTo = "Wallet Ticket",
                                Amount = model.TicketOrder,
                                DateCreated = DateTime.Now,
                                AppUserId = appUser.Id,
                                Type = GameTicketType.SwapFromWalletBNBBEP20
                            });
                            _gameTicketService.Save();
                            #endregion
                        }
                        else
                        {
                            return new OkObjectResult(new GenericResult(false,
                                resultMainBalanceUpdate.Errors.FirstOrDefault().Description));
                        }
                    }
                    else
                    {
                        if (paymentBNB > appUser.BNBAffiliateBalance)
                            return new OkObjectResult(new GenericResult(false, "Your balance is not enough to make a transaction"));

                        appUser.TicketBalance += model.TicketOrder;
                        appUser.BNBAffiliateBalance -= paymentBNB;

                        var resultBNBAffiliateBalanceUpdate = await _userManager.UpdateAsync(appUser);

                        if (resultBNBAffiliateBalanceUpdate.Succeeded)
                        {
                            #region Add Invest Transaction
                            _walletBNBAffiliateTransactionService.Add(new WalletBNBAffiliateTransactionViewModel
                            {
                                AddressFrom = "Wallet BNB Affiliate",
                                AddressTo = "SYSTEM",
                                Amount = paymentBNB,
                                AmountReceive = paymentBNB,
                                Fee = 0,
                                FeeAmount = 0,
                                DateCreated = DateTime.Now,
                                AppUserId = appUser.Id,
                                TransactionHash = "SYSTEM",
                                Type = WalletBNBAffiliateTransactionType.PayTicket
                            });
                            _walletBNBAffiliateTransactionService.Save();
                            #endregion

                            #region Add Game Ticket
                            _gameTicketService.Add(new GameTicketViewModel
                            {
                                AddressFrom = "Wallet BNB Affiliate",
                                AddressTo = "Wallet Ticket",
                                Amount = model.TicketOrder,
                                DateCreated = DateTime.Now,
                                AppUserId = appUser.Id,
                                Type = GameTicketType.SwapFromWalletBNBAffiliate
                            });
                            _gameTicketService.Save();
                            #endregion
                        }
                        else
                        {
                            return new OkObjectResult(new GenericResult(false,
                                resultBNBAffiliateBalanceUpdate.Errors.FirstOrDefault().Description));
                        }
                    }
                }
                else if (model.Type == 3)
                {
                    var paymentICD = model.TicketOrder * 25;

                    if (paymentICD > appUser.InvestBalance)
                        return new OkObjectResult(new GenericResult(false, "Your balance is not enough to make a transaction"));

                    appUser.TicketBalance += model.TicketOrder;
                    appUser.InvestBalance -= paymentICD;

                    var resultInvestBalanceUpdate = await _userManager.UpdateAsync(appUser);

                    if (resultInvestBalanceUpdate.Succeeded)
                    {
                        #region Add Invest Transaction
                        _walletInvestTransactionService.Add(new WalletInvestTransactionViewModel
                        {
                            AddressFrom = "Wallet Invest",
                            AddressTo = "SYSTEM",
                            Amount = paymentICD,
                            AmountReceive = paymentICD,
                            Fee = 0,
                            FeeAmount = 0,
                            DateCreated = DateTime.Now,
                            AppUserId = appUser.Id,
                            TransactionHash = "SYSTEM",
                            Type = WalletInvestTransactionType.PayTicket
                        });
                        _walletInvestTransactionService.Save();
                        #endregion

                        #region Add Game Ticket
                        _gameTicketService.Add(new GameTicketViewModel
                        {
                            AddressFrom = "Wallet Invest",
                            AddressTo = "Wallet Ticket",
                            Amount = model.TicketOrder,
                            DateCreated = DateTime.Now,
                            AppUserId = appUser.Id,
                            Type = GameTicketType.SwapFromWalletInvest
                        });
                        _gameTicketService.Save();
                        #endregion
                    }
                    else
                    {
                        return new OkObjectResult(new GenericResult(false,
                            resultInvestBalanceUpdate.Errors.FirstOrDefault().Description));
                    }
                }
                else
                {
                    return new OkObjectResult(new GenericResult(false, "Wallet type is required"));
                }

                return new OkObjectResult(new GenericResult(true, "Buy Ticket is successful."));
            }
            catch (Exception ex)
            {
                _logger.LogError("GameController_BuyTicket: {0}", ex.Message);
                return new OkObjectResult(new GenericResult(false, ex.Message));
            }
        }
        [HttpGet]
        public IActionResult CaculationTicketByType(string modelJson)
        {
            try
            {
                var model = JsonConvert.DeserializeObject<BuyTicketViewModel>(modelJson);
                if (model.Type == 3)
                {
                    model.AmountPayment = model.TicketOrder * 25;
                    return new OkObjectResult(model);
                }
                else
                {

                    decimal priceICD = 0.1M;
                    decimal paymentUSD = priceICD * (model.TicketOrder * 25);

                    decimal priceBNBBep20 = _blockChainService.GetCurrentPrice("BNB", "USD");
                    if (priceBNBBep20 == 0)
                        return new OkObjectResult(new GenericResult(false, "There is a problem loading the currency value!"));

                    var paymentBNB = Math.Round(paymentUSD / priceBNBBep20, 4);

                    model.AmountPayment = paymentBNB;

                    return new OkObjectResult(model);
                }

            }
            catch (Exception ex)
            {
                _logger.LogError("ExchangeController_CaculationICDByBNB: {0}", ex.Message);
                return new OkObjectResult(new GenericResult(false, ex.Message));
            }
        }

        [HttpGet]
        public async Task<IActionResult> GetWalletBlanceByType(int type)
        {
            var userId = User.GetSpecificClaim("UserId");
            var appUser = await _userService.GetById(userId);
            decimal walletBalance = 0;
            if (type == 1)
            {
                walletBalance = appUser.MainBalance;
            }
            else if (type == 2)
            {
                walletBalance = appUser.BNBAffiliateBalance;
            }
            else if (type == 3)
            {
                walletBalance = appUser.InvestBalance;
            }

            return new OkObjectResult(walletBalance);
        }

        [HttpGet]
        public IActionResult GetAllLuckyRoundHistoryPaging(string keyword, int page, int pageSize)
        {
            string appUserId = string.Empty;

            var roleName = User.GetSpecificClaim("RoleName");
            if (roleName.ToLower() == "customer")
            {
                appUserId = User.GetSpecificClaim("UserId");
            }

            var model = _luckyRoundHistoryService.GetAllPaging(keyword, appUserId, page, pageSize);
            return new OkObjectResult(model);
        }


        #endregion

   
        #region CrashGame
        [HttpGet]
        public IActionResult CrashGame()
        {
            return View();
        }

        [HttpGet]
        public IActionResult GameAdmin()
        {
            return View();
        }

        [HttpGet]
        public IActionResult Configs()
        {
            return View();
        }

        #endregion
    }
}