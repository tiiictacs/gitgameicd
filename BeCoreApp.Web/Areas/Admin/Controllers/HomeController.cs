﻿using BeCoreApp.Application.Interfaces;
using BeCoreApp.Data.Entities;
using BeCoreApp.Extensions;
using BeCoreApp.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace BeCoreApp.Areas.Admin.Controllers
{
    public class HomeController : BaseController
    {
        private readonly IChartRoundService _chartRoundService;
        private readonly INotifyService _notifyService;
        private readonly UserManager<AppUser> _userManager;
        public HomeController(
            IChartRoundService chartRoundService,
            UserManager<AppUser> userManager,
            INotifyService notifyService
            )
        {
            _chartRoundService = chartRoundService;
            _userManager = userManager;
            _notifyService = notifyService;
        }

        public IActionResult Index()
        {

            var userid = CurrentUserId;

            var u = User.Identity.Name;

            var roleName = User.GetSpecificClaim("RoleName");
            if (roleName.ToLower() == "admin")
                return Redirect("/admin/report/index");

            var model = new HomeViewModel();
            model.Notify = _notifyService.GetDashboard();
            model.ChartRounds = _chartRoundService.GetAll();
            return View(model);
        }
    }
}