﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Security.Claims;
using System.Threading.Tasks;
using BeCoreApp.Application.Interfaces;
using BeCoreApp.Application.ViewModels.BlockChain;
using BeCoreApp.Application.ViewModels.BotTelegram;
using BeCoreApp.Application.ViewModels.System;
using BeCoreApp.Application.ViewModels.Valuesshare;
using BeCoreApp.Data.Entities;
using BeCoreApp.Data.Enums;
using BeCoreApp.Extensions;
using BeCoreApp.Services;
using BeCoreApp.Utilities.Constants;
using BeCoreApp.Utilities.Dtos;
using BeCoreApp.Utilities.Extensions;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Nethereum.RPC.Eth.DTOs;
using Nethereum.Util;
using Newtonsoft.Json;

namespace BeCoreApp.Areas.Admin.Controllers
{
    public class ExchangeController : BaseController
    {
        private readonly IUserService _userService;
        private readonly UserManager<AppUser> _userManager;
        private readonly IBlockChainService _blockChainService;
        private readonly IWalletInvestTransactionService _walletInvestTransactionService;
        private readonly IWalletLockTransactionService _walletLockTransactionService;
        private readonly IWalletBNBAffiliateTransactionService _walletBNBAffiliateTransactionService;
        private readonly IWalletBNBBEP20TransactionService _walletBNBBEP20TransactionService;
        private readonly ITransactionService _transactionService;
        private readonly ILogger<ExchangeController> _logger;
        private readonly AddressUtil _addressUtil = new AddressUtil();
        private readonly IEmailSender _emailSender;
        private readonly IChartRoundService _chartRoundService;
        public ExchangeController(
            IWalletBNBBEP20TransactionService walletBNBBEP20TransactionService,
            IWalletBNBAffiliateTransactionService walletBNBAffiliateTransactionService,
            IChartRoundService chartRoundService,
            IWalletInvestTransactionService walletInvestTransactionService,
            IWalletLockTransactionService walletLockTransactionService,
            ILogger<ExchangeController> logger,
            ITransactionService transactionService,
            UserManager<AppUser> userManager,
            IUserService userService,
            IEmailSender emailSender,
            IBlockChainService blockChainService)
        {
            _walletBNBAffiliateTransactionService = walletBNBAffiliateTransactionService;
            _walletBNBBEP20TransactionService = walletBNBBEP20TransactionService;
            _chartRoundService = chartRoundService;
            _walletInvestTransactionService = walletInvestTransactionService;
            _walletLockTransactionService = walletLockTransactionService;
            _logger = logger;
            _transactionService = transactionService;
            _userManager = userManager;
            _blockChainService = blockChainService;
            _userService = userService;
            _emailSender = emailSender;
        }

        [HttpGet]
        public IActionResult Index()
        {
            try
            {
                //var preSale = _chartRoundService.GetByType(ChartRoundType.PreSale);
                //preSale.DistributedToken += 500;
                //_chartRoundService.Update(preSale);
                //_chartRoundService.Save();
            }
            catch (Exception ex)
            {
            }

            return View();
        }

        public class BuyICDViewModel
        {
            public int Type { get; set; }
            public decimal Amount { get; set; }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> ByICD(string modelJson)
        {
            try
            {
                var model = JsonConvert.DeserializeObject<BuyICDViewModel>(modelJson);

                var userId = User.GetSpecificClaim("UserId");

                var appUser = await _userManager.FindByIdAsync(userId);
                if (appUser == null)
                    return new OkObjectResult(new GenericResult(false, "Account does not exist"));

                if (model.Amount < 0.1m)
                    return new OkObjectResult(new GenericResult(false, "Minimum buy 0.1 BNB"));

                if (model.Amount > 6)
                    return new OkObjectResult(new GenericResult(false, "Maximum buy 6 BNB"));

                decimal payPrivateSale = _transactionService.GetUserAmountByType(appUser.Id, TransactionType.PayPreSale);
                if ((payPrivateSale + model.Amount) > 6)
                    return new OkObjectResult(new GenericResult(false, "Maximum buy 6 BNB"));

                if (model.Type == 1)
                {
                    if (model.Amount > appUser.MainBalance)
                        return new OkObjectResult(new GenericResult(false, "Your balance is not enough to make a transaction"));
                }
                else if (model.Type == 2)
                {
                    if (model.Amount > appUser.BNBAffiliateBalance)
                        return new OkObjectResult(new GenericResult(false, "Your balance is not enough to make a transaction"));
                }
                else
                {
                    return new OkObjectResult(new GenericResult(false, "Wallet type is required"));
                }

                decimal priceICD = 0.2M;

                decimal priceBNBBep20 = _blockChainService.GetCurrentPrice("BNB", "USD");
                if (priceBNBBep20 == 0)
                    return new OkObjectResult(new GenericResult(false, "There is a problem loading the currency value!"));

                var amountUSD = Math.Round(model.Amount * priceBNBBep20, 2);

                decimal amountICD = Math.Round(amountUSD / priceICD, 2);

                //decimal amountICDPercent = amountICD / 2;

                //decimal amountICDInvest = amountICDPercent;

                //decimal amountICDLock = amountICDPercent + (amountICDPercent * 0.1m);

                //decimal icdRecevice = amountICDInvest + amountICDLock;

                appUser.InvestBalance += amountICD;
                //appUser.LockBalance += amountICDLock;

                if (model.Type == 1)
                    appUser.MainBalance -= model.Amount;
                else
                    appUser.BNBAffiliateBalance -= model.Amount;

                var resultAppUserUpdate = await _userManager.UpdateAsync(appUser);

                if (resultAppUserUpdate.Succeeded)
                {
                    if (model.Type == 1)
                    {
                        _walletBNBBEP20TransactionService.Add(
                            new WalletBNBBEP20TransactionViewModel
                            {
                                AddressFrom = "Wallet BNB BEP20",
                                AddressTo = "SYSTEM",
                                Amount = model.Amount,
                                AmountReceive = model.Amount,
                                Fee = 0,
                                FeeAmount = 0,
                                DateCreated = DateTime.Now,
                                AppUserId = appUser.Id,
                                TransactionHash = "SYSTEM",
                                Type = WalletBNBBEP20TransactionType.PayPreSale
                            });
                        _walletBNBBEP20TransactionService.Save();
                    }
                    else
                    {
                        _walletBNBAffiliateTransactionService.Add(
                            new WalletBNBAffiliateTransactionViewModel
                            {
                                AddressFrom = "Wallet BNB Affiliate",
                                AddressTo = "SYSTEM",
                                Amount = model.Amount,
                                AmountReceive = model.Amount,
                                Fee = 0,
                                FeeAmount = 0,
                                DateCreated = DateTime.Now,
                                AppUserId = appUser.Id,
                                TransactionHash = "SYSTEM",
                                Type = WalletBNBAffiliateTransactionType.PayPreSale
                            });
                        _walletBNBAffiliateTransactionService.Save();
                    }

                    //#region Add Lock Transaction
                    //_walletLockTransactionService.Add(new WalletLockTransactionViewModel
                    //{
                    //    AddressFrom = "SYSTEM",
                    //    AddressTo = "Wallet Lock",
                    //    Amount = amountICDLock,
                    //    AmountReceive = amountICDLock,
                    //    Fee = 0,
                    //    FeeAmount = 0,
                    //    DateCreated = DateTime.Now,
                    //    AppUserId = appUser.Id,
                    //    TransactionHash = "SYSTEM",
                    //    Type = WalletLockTransactionType.DepositPrivateSale
                    //});
                    //_walletLockTransactionService.Save();
                    //#endregion

                    #region Add Invest Transaction
                    _walletInvestTransactionService.Add(new WalletInvestTransactionViewModel
                    {
                        AddressFrom = "SYSTEM",
                        AddressTo = "Wallet Invest",
                        Amount = amountICD,
                        AmountReceive = amountICD,
                        Fee = 0,
                        FeeAmount = 0,
                        DateCreated = DateTime.Now,
                        AppUserId = appUser.Id,
                        TransactionHash = "SYSTEM",
                        Type = WalletInvestTransactionType.DepositPreSale
                    });
                    _walletInvestTransactionService.Save();
                    #endregion

                    #region Add Transaction
                    _transactionService.Add(new TransactionViewModel
                    {
                        TransactionHash = "SYSTEM",
                        Amount = amountICD,
                        AddressFrom = "SYSTEM",
                        AddressTo = "Wallet Invest",
                        DateCreated = DateTime.Now,
                        AppUserId = appUser.Id,
                        Unit = TransactionUnit.ICD,
                        Type = TransactionType.BuyICD,
                    });
                    _transactionService.Save();

                    _transactionService.Add(new TransactionViewModel
                    {
                        TransactionHash = "SYSTEM",
                        Amount = model.Amount,
                        AddressFrom = model.Type == 1 ? "Wallet BNB BEP20" : "Wallet BNB Affiliate",
                        AddressTo = "SYSTEM",
                        DateCreated = DateTime.Now,
                        AppUserId = appUser.Id,
                        Unit = TransactionUnit.BNB,
                        Type = TransactionType.PayPreSale,
                    });
                    _transactionService.Save();
                    #endregion

                    //if (appUser.ReferralId != null)
                    //{
                    //    var userLevel1 = await _userManager.FindByIdAsync(appUser.ReferralId.Value.ToString());
                    //    if (userLevel1 != null && userLevel1.IsSystem == false)
                    //    {
                    //        decimal affiliateLevel1Amount = model.Amount * 0.05M;

                    //        userLevel1.BNBAffiliateBalance += affiliateLevel1Amount;
                    //        var resultUserLevel1Update = await _userManager.UpdateAsync(userLevel1);

                    //        if (resultUserLevel1Update.Succeeded)
                    //        {
                    //            var affiliateLevel1Transaction = new TransactionViewModel
                    //            {
                    //                PriceBNB = priceBNBBep20,
                    //                TransactionHash = "SYSTEM",
                    //                Amount = affiliateLevel1Amount,
                    //                AddressFrom = "SYSTEM",
                    //                AddressTo = "Wallet BNB Affiliate",
                    //                DateCreated = DateTime.Now,
                    //                AppUserId = userLevel1.Id,
                    //                Unit = TransactionUnit.BNB,
                    //                Type = TransactionType.AffiliateBuyICD,
                    //            };

                    //            _transactionService.Add(affiliateLevel1Transaction);
                    //            _transactionService.Save();

                    //            await SendComisionMail(affiliateLevel1Transaction);
                    //        }

                    //        if (userLevel1.ReferralId != null)
                    //        {
                    //            var userLevel2 = await _userManager.FindByIdAsync(userLevel1.ReferralId.Value.ToString());
                    //            if (userLevel2 != null && userLevel2.IsSystem == false)
                    //            {
                    //                decimal affiliateLevel2Amount = model.Amount * 0.03M;

                    //                userLevel2.BNBAffiliateBalance += affiliateLevel2Amount;
                    //                var resultUserLevel2Update = await _userManager.UpdateAsync(userLevel2);

                    //                if (resultUserLevel2Update.Succeeded)
                    //                {
                    //                    var affiliateLevel2Transaction = new TransactionViewModel
                    //                    {
                    //                        PriceBNB = priceBNBBep20,
                    //                        TransactionHash = "SYSTEM",
                    //                        Amount = affiliateLevel2Amount,
                    //                        AddressFrom = "SYSTEM",
                    //                        AddressTo = "Wallet BNB Affiliate",
                    //                        DateCreated = DateTime.Now,
                    //                        AppUserId = userLevel2.Id,
                    //                        Unit = TransactionUnit.BNB,
                    //                        Type = TransactionType.AffiliateBuyICD,
                    //                    };

                    //                    _transactionService.Add(affiliateLevel2Transaction);
                    //                    _transactionService.Save();

                    //                    await SendComisionMail(affiliateLevel2Transaction);
                    //                }

                    //                if (userLevel2.ReferralId != null)
                    //                {
                    //                    var userLevel3 = await _userManager.FindByIdAsync(userLevel2.ReferralId.Value.ToString());
                    //                    if (userLevel3 != null && userLevel3.IsSystem == false)
                    //                    {
                    //                        decimal affiliateLevel3Amount = model.Amount * 0.02M;

                    //                        userLevel3.BNBAffiliateBalance += affiliateLevel3Amount;
                    //                        var resultUserLevel3Update = await _userManager.UpdateAsync(userLevel3);

                    //                        if (resultUserLevel3Update.Succeeded)
                    //                        {
                    //                            var affiliateLevel3Transaction = new TransactionViewModel
                    //                            {
                    //                                PriceBNB = priceBNBBep20,
                    //                                TransactionHash = "SYSTEM",
                    //                                Amount = affiliateLevel3Amount,
                    //                                AddressFrom = "SYSTEM",
                    //                                AddressTo = "Wallet BNB Affiliate",
                    //                                DateCreated = DateTime.Now,
                    //                                AppUserId = userLevel3.Id,
                    //                                Unit = TransactionUnit.BNB,
                    //                                Type = TransactionType.AffiliateBuyICD,
                    //                            };

                    //                            _transactionService.Add(affiliateLevel3Transaction);
                    //                            _transactionService.Save();

                    //                            await SendComisionMail(affiliateLevel3Transaction);
                    //                        }
                    //                    }
                    //                }
                    //            }
                    //        }
                    //    }
                    //}

                    return new OkObjectResult(new GenericResult(true, "Buy ICD is successful."));
                }
                else
                {
                    return new OkObjectResult(new GenericResult(false,
                        resultAppUserUpdate.Errors.FirstOrDefault().Description));
                }
            }
            catch (Exception ex)
            {
                _logger.LogError("WalletController_BuyICD: {0}", ex.Message);
                return new OkObjectResult(new GenericResult(false, ex.Message));
            }
        }


        [HttpGet]
        public IActionResult CaculationICDByBNB(string modelJson)
        {
            try
            {
                var model = JsonConvert.DeserializeObject<ExchangeICDViewModel>(modelJson);

                decimal priceICD = 0.2M;

                decimal priceBNBBep20 = _blockChainService.GetCurrentPrice("BNB", "USD");
                if (priceBNBBep20 == 0)
                    return new OkObjectResult(new GenericResult(false, "There is a problem loading the currency value!"));

                var amountUSD = Math.Round(model.OrderBNB * priceBNBBep20, 2);

                decimal amountICD = Math.Round(amountUSD / priceICD, 2);

                //decimal amountICDPercent = amountICD / 2;

                //decimal amountICDInvest = amountICDPercent;

                //decimal amountICDLock = amountICDPercent + (amountICDPercent * 0.1m);

                //decimal icdRecevice = amountICDInvest + amountICDLock;

                model.AmountICD = Math.Round(amountICD, 2);

                return new OkObjectResult(model);
            }
            catch (Exception ex)
            {
                _logger.LogError("ExchangeController_CaculationICDByBNB: {0}", ex.Message);
                return new OkObjectResult(new GenericResult(false, ex.Message));
            }
        }

        [HttpGet]
        public async Task<IActionResult> GetWalletBlanceByType(int type)
        {
            var userId = User.GetSpecificClaim("UserId");
            var appUser = await _userService.GetById(userId);
            decimal bnbBalance = 0;
            if (type == 1)
            {
                bnbBalance = appUser.MainBalance;
            }
            else if (type == 2)
            {
                bnbBalance = appUser.BNBAffiliateBalance;
            }

            return new OkObjectResult(bnbBalance);
        }

        [HttpGet]
        public async Task<IActionResult> GetWalletBlance()
        {
            var userId = User.GetSpecificClaim("UserId");
            var appUser = await _userService.GetById(userId);
            var model = new WalletViewModel()
            {
                MainBalance = appUser.MainBalance,
            };

            return new OkObjectResult(model);
        }


        private async Task SendComisionMail(TransactionViewModel vm)
        {
            try
            {
                var appUser = await _userManager.FindByIdAsync(vm.AppUserId.ToString());
                if (appUser != null)
                {
                    var parameters = new ComisionMessageParam
                    {
                        Amount = vm.Amount,
                        RecievedAt = DateTime.Now,
                        AddressTo = vm.AddressTo,
                        Unit = vm.Unit.ToString(),
                        Rate = vm.Unit == TransactionUnit.BNB ? vm.PriceBNB : 0,
                        AddressFrom = vm.AddressFrom
                    };

                    var message = _emailSender.BuildReportCommisionMessage(parameters);

                    await _emailSender.TrySendEmailAsync(appUser.Email, $"Commission received {DateTime.UtcNow.ToICDFormatTime()}(UTC)", message);
                }
            }
            catch (Exception e)
            {
                _logger.LogError(e, "Failed to Send Commision Mail");
            }
        }
    }
}